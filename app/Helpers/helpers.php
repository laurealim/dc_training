<?php

use App\Model\Apartment;
function pr($array = array())
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

function dynamicYearList()
{
    $yearArrList = array();
    for ($i = date("Y") - 1; $i < date("Y") + 10; $i++) {
        $yearArrList[$i] = $i;
    }

    return $yearArrList;
}

function dynamicMonthList()
{
    $monthList = [
        1 => 'January',
        2 => 'February',
        3 => 'March',
        4 => 'April',
        5 => 'May',
        6 => 'June',
        7 => 'July',
        8 => 'August',
        9 => 'September',
        10 => 'October',
        11 => 'November',
        12 => 'December',
    ];
    $monthArrList = array();
    for ($i = 1; $i < 13; $i++) {
        $monthArrList[$i] = $monthList[$i];
    }

    return $monthArrList;
}

function ledgerType()
{
    return [1 => 'Initial', 2 => 'Regular', 3 => 'Adjustment', 4 => 'Advance',];
}

function status()
{
    return [0 => 'Deleted', 1 => 'Active', 2 => 'Inactive',];
}

function userType()
{
    return [1 => 'Super User', 2 => 'Owner', 3 => 'Tenant',];
}

function ledgerFor()
{
    return [1 => 'Billing', 2 => 'Payment',];
}

function state()
{
    return [1 => 'Pending', 2 => 'Approved', 3 => 'Rejected',];
}

function is_assigned()
{
    return [0 => 'Unassigned', 1 => 'Assigned', 2 => 'Confirmed',];
}

function training_status()
{
    return [0 => 'Postponed', 1 => 'Pending', 2 => 'Complete',];
}

function getAptListByClientId($client_id){
    $apartmentModel = new Apartment();
//    $ledgerModel = new Ledger();
    $dataListObj = array();
    $aptList = $apartmentModel->where('apartments.client_id', '=', $client_id)
        ->where('apartments.status', '=', config('constants.status.Active'))
        ->where('apartments.is_assigned', '=', config('constants.is_assigned.Confirmed'))
        ->leftJoin('apartment_rents', 'apartments.id', 'apartment_rents.apt_id')
        ->select('apartment_rents.rent_amount', 'apartment_rents.service_charge', 'apartment_rents.adv_month', 'apartment_rents.adv_rent', 'apartments.*')
        ->get();

    $optionData = '<option>--- Select State ---</option>';
    foreach ($aptList as $key => $vals) {
        $vals->is_initial = 1;
        $optionData .= '<option value="' . $vals->id . '">' . $vals->apt_number . '</option>';
        $ledgerDataArr = is_initial_bill($vals->id, $vals->client_id, config('constants.ledgerFor.Billing'));
        if ($ledgerDataArr) {
            $vals->is_initial = 0;
        }
        $dataListObj[$vals->id] = $vals;
    }

    return ['options' => $optionData, 'dataValue' => $dataListObj];
}

function getAptDetailsByAptAndClientId($client_id, $apt_id){
    $apartmentModel = new Apartment();
    $aptData = $apartmentModel->where('apartments.id', '=', $apt_id)
        ->where('apartments.client_id', '=', $client_id)
        ->leftJoin('apartment_rents', 'apartments.id', 'apartment_rents.apt_id')
        ->select('apartment_rents.rent_amount', 'apartment_rents.service_charge', 'apartment_rents.adv_month', 'apartment_rents.adv_rent')
        ->get();

    return $aptData;
}


?>