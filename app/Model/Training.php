<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Training extends Model
{
    protected $fillable = [
        'name', 'created_by'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', "created_by");
    }

    public function departments()
    {
        return $this->hasOne('App\Model\Department', 'id', "created_by");
    }

    public function saveData($data)
    {
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $this->$key = $value;
            }
        }
        $this->created_by = auth()->user()->id;
//        if (!empty($data->trade_code)) {
//            $this->trade_code = strtotime(now());
//        }
        $this->dept_id = Auth::user()->dept_id;
        $this->start_date = date('Y-m-d', strtotime($data->start_date));
        $this->end_date = date('Y-m-d', strtotime($data->end_date));
        $this->exam_date = date('Y-m-d', strtotime($data->exam_date));
        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        foreach ($data->request as $key => $value) {
            if ($key != "_token") {
                $ticket->$key = $value;
            }
        }
        $ticket->start_date = date('Y-m-d', strtotime($ticket->start_date));
        $ticket->end_date = date('Y-m-d', strtotime($ticket->end_date));
        $ticket->exam_date = date('Y-m-d', strtotime($ticket->exam_date));
        $ticket->save();
        return 1;
    }

    public function getIdTradeCodeAttribute()
    {
        return $this->id . '_' . $this->trade_code;
    }

    public function registrations()
    {
        return $this->hasMany('App\Model\Registration')->orderBy('training_id', 'DESC');
    }
}
