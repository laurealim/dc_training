<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RentUpdateHistory extends Model
{
    protected $fillable = [
        'apt_rent_id','rent_amount','applied_from', 'status',
    ];

    public function saveData($data)
    {
        //dd($data->property_name);
        $this->apt_rent_id = $data->apt_rent_id;
        $this->rent_amount = $data->rent_amount;
        $this->applied_from = $data->applied_from;
        $this->status= $data->status;
        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);

        $ticket->apt_rent_id = $data->apt_rent_id;
        $ticket->rent_amount = $data->rent_amount;
        $ticket->applied_from = $data->applied_from;
        $ticket->status= $data->status;
        $ticket->save();
        return 1;
    }
}
