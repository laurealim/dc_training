<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Trade extends Model
{
    protected $fillable = [
        'trade_name','created_by','is_deleted','trade_code',
    ];

    public function saveData($data)
    {
//        dd($data);
        $this->created_by = auth()->user()->id;
        $this->trade_name = $data->trade_name;
        $this->trade_code = $data->trade_code;
        $this->status = config('constants.status.Active');
        $this->save();
        return 1;
    }

    public function updateData($data)
    {
        $ticket = $this->find($data['id']);
        $ticket->trade_name = $data->trade_name;
        $ticket->trade_code = $data->trade_code;
        $ticket->status = $data->status;
        $ticket->save();
        return 1;
    }

    public function user()
    {
        return $this->hasMany('App\User')->orderBy('name', 'ASC');;
    }
}
