<?php

namespace App\Http\Controllers;

use App\Model\Department;
use App\Model\District;
use App\Model\Division;
use App\Model\Registration;
use App\Model\Training;
use App\Model\Union;
use App\Model\Upazila;
use App\User;
use Auth;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

//use Maatwebsite\Excel\Excel;

class RegistrationController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();
//        var_dump($trainingList);

        $registrationList = $registrationModel->select("users.first_name", 'registrations.*')->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('registrations.name_en', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->select('registrations.*', 'users.first_name')
//            ->tosql();
            ->paginate($displayValue);
//        dd($registrationList);

        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        $unionList = $unionModel->pluck('bn_name', 'id')->all();
        $unionList[0] = 'পৌরসভা';

        $addressArr = [$divisionData,$districtList,$upazilaList,$unionList];

        $uri = "";
        if (request()->ajax()) {
            $uri = request()->getUri();
            return view('registration.ajax_list', compact('registrationList', 'trainingList', 'uri', 'addressArr'));
        } else {
            return view('registration.adminList', compact('registrationList', 'trainingList', 'uri', 'addressArr'));
        }
    }

    public function adminListId($id)
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();
//        dd($trainingList);

        $registrationList = $registrationModel->select("users.first_name", 'registrations.*')->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('registrations.training_id', '=', $id)
            ->where('registrations.name_en', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->select('registrations.*', 'users.first_name')
//            ->toSql();
            ->paginate($displayValue);

//        dd($registrationList);

        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        $unionList = $unionModel->pluck('bn_name', 'id')->all();
        $unionList[0] = 'পৌরসভা';

        $addressArr = [$divisionData,$districtList,$upazilaList,$unionList];

        $uri = "";
        if (request()->ajax()) {
            $uri = request()->getUri();
            return view('registration.ajax_list', compact('registrationList', 'trainingList', 'uri', 'addressArr'));
        } else {
            return view('registration.adminList', compact('registrationList', 'trainingList', 'uri', 'addressArr'));
        }
    }

    public function adminForm()
    {
        $trainingModel = new Training();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();

        $eduLevel_list = config('constants.edu_level.arr');
        $eduBoard_list = config('constants.edu_board.arr');
        $relrg_list = config('constants.religious.arr');
        $gender_list = config('constants.gender.arr');
        $marital_list = config('constants.marital_status.arr');

        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();

        return view('registration.adminForm', compact('trainingList', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'divisionData', 'districtList', 'upazilaList'));
    }

    public function adminStore(Request $request)
    {
        $registrationModel = new Registration();

        $formData = $request->all();
        $request->request->add(['nid' => $this->bn2en($formData['nid'])]);
        $request->request->add(['phone' => $this->bn2en($formData['phone'])]);
        if (isset($formData['same_add'])) {
            $request->request->add(['dist_per' => $formData['dist_pre']]);
            $request->request->add(['up_per' => $formData['up_pre']]);
            $request->request->add(['post_per' => $formData['post_pre']]);
            $request->request->add(['vill_per' => $formData['vill_pre']]);

            unset($request['same_add']);
        }

//        $formData['dist_per'] = $formData['dist_pre'];
//        $formData['up_per'] = $formData['up_pre'];
//        $formData['post_per'] = $formData['post_pre'];
//        $formData['vill_per'] = $formData['vill_pre'];
//        dd($formData);




        $data = $this->validate($request, [
            'training_id' => 'required',
            'name_bn' => 'required',
            'name_en' => 'required',
            'f_name_bn' => 'required',
            'm_name_bn' => 'required',
            'vill_pre' => 'required',
            'post_pre' => 'required',
            'up_pre' => 'required',
            'dist_pre' => 'required',
            'vill_per' => 'required',
            'post_per' => 'required',
            'up_per' => 'required',
            'dist_per' => 'required',
            'edu_lvl' => 'required',
            'phone' => 'required',
            'nid' => 'required',
            'dob' => 'required',
            'relg' => 'required',
            'nation' => 'required',
            'gender' => 'required',
            'married' => 'required',
			//'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048';
        ],
            [
                'training_id.required' => 'Please Select a Training',
                'name_bn.required' => 'Bengoli Name is required',
                'name_en.required' => 'English Name is required',
                'f_name_bn.required' => 'Bengoli Name is required',
                'm_name_bn.required' => 'Bengoli Name is required',
                'vill_pre.required' => 'Present Village is required',
                'post_pre.required' => 'Present Post Office is required',
                'up_pre.required' => 'Present Upozilla is required',
                'dist_pre.required' => 'Present District is required',
                'vill_per.required' => 'Permanent Village is required',
                'post_per.required' => 'Permanent Post Office is required',
                'up_per.required' => 'Permanent Upozilla is required',
                'dist_per.required' => 'Permanent District is required',
                'edu_lvl.required' => 'Education Level is required',
                'phone.required' => 'Phone is required',
                'nid.required' => 'Valid ID is required',
                'dob.required' => 'Date of Birth is required',
                'relg.required' => 'Please Select a Religious',
                'nation.required' => 'Please Select Your Nationality',
                'gender.required' => 'Please Select your gender',
                'married.required' => 'Please Select your Marital Status',
				//'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
            ]);

        $lastInsertedId = $registrationModel->saveData($request);

        /*  File Manipulation   */
        $filename = '';
        try {
            if ($request->hasFile('image')) {
                $client_id = $lastInsertedId;
                $path = "trainee";

                // cache the file
                $file = $request->file('image');

                // generate a new filename. getClientOriginalExtension() for the file extension
                $filename = $client_id . '.' . $file->getClientOriginalExtension();
                $filePath = $path . '/' . $client_id;

                if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                    Storage::delete($filePath . '/' . $filename);
                }

                // save to storage_1/app/public/clients as the new $filename
                $path = $file->storeAs($filePath, $filename);

                Registration::where('id', $lastInsertedId)->update(['image' => $filename]);
            }
        } catch (\Exception $exception) {
            var_dump($exception);
        }

        return redirect('admin/registration/list')->with('success', 'New Registration added successfully');
    }

    public function show($id)
    {
        //
    }

    public function adminEdit($id, Request $request)
    {
        $registrationModel = new Registration();
        $trainingModel = new Training();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        try {

            $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
                ->where('is_deleted', '=', config('constants.is_deleted.Active'))
                ->pluck('name', 'id')->all();

            $eduLevel_list = config('constants.edu_level.arr');
            $eduBoard_list = config('constants.edu_board.arr');
            $relrg_list = config('constants.religious.arr');
            $gender_list = config('constants.gender.arr');
            $marital_list = config('constants.marital_status.arr');

            $registrationData = $registrationModel->where('registrations.id', $id)
                ->leftJoin('users', 'registrations.created_by', 'users.id')
                ->select('registrations.*', 'users.first_name')
                ->firstOrFail();

            $districtListPre = $districtModel->pluck('bn_name', 'id')->all();
            $districtListPer = $districtModel->pluck('bn_name', 'id')->all();
            $upazilaListPre = $upazilaModel->where('district_id', $registrationData->dist_pre)->pluck('bn_name', 'id')->all();
            $upazilaListPer = $upazilaModel->where('district_id', $registrationData->dist_per)->pluck('bn_name', 'id')->all();
            $unionListPre = $unionModel->where('upazilla_id', $registrationData->up_pre)->pluck('bn_name', 'id')->all();
            $unionListPer = $unionModel->where('upazilla_id', $registrationData->up_per)->pluck('bn_name', 'id')->all();

            $listArr = [
                $districtListPre, $districtListPer, $upazilaListPre, $upazilaListPer, $unionListPre, $unionListPer
            ];

            return view('registration.adminEdit', compact('trainingList', 'id', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'registrationData', 'listArr'));
//            return view('training.adminEdit', compact('deptList', 'id', 'trainingData'));
        } catch (\Exception $ex) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
//            return redirect('client/apt/list')->with('errors', 'No Data Found...');
//            return redirect()->back()->withErrors(['msg', 'The Message']);    When has multiple errors and go back to the previous url...
        }
    }

    public function adminUpdate(Request $request, $id)
    {
        $registrationModel = new Registration();

        $formData = $request->all();
        if (isset($formData['same_add'])) {
            $request->request->add(['dist_per' => $formData['dist_pre']]);
            $request->request->add(['up_per' => $formData['up_pre']]);
            $request->request->add(['post_per' => $formData['post_pre']]);
            $request->request->add(['vill_per' => $formData['vill_pre']]);

            unset($request['same_add']);
        }
        $request->request->add(['nid' => $this->bn2en($formData['nid'])]);
        $request->request->add(['phone' => $this->bn2en($formData['phone'])]);

        $data = $this->validate($request, [
            'training_id' => 'required',
            'name_bn' => 'required',
            'name_en' => 'required',
            'f_name_bn' => 'required',
            'm_name_bn' => 'required',
            'vill_pre' => 'required',
            'post_pre' => 'required',
            'up_pre' => 'required',
            'dist_pre' => 'required',
            'vill_per' => 'required',
            'post_per' => 'required',
            'up_per' => 'required',
            'dist_per' => 'required',
            'edu_lvl' => 'required',
            'phone' => 'required',
            'nid' => 'required',
            'dob' => 'required',
            'relg' => 'required',
            'nation' => 'required',
            'gender' => 'required',
            'married' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048'
        ],
            [
                'training_id.required' => 'Please Select a Training',
                'name_bn.required' => 'Bengoli Name is required',
                'name_en.required' => 'English Name is required',
                'f_name_bn.required' => 'Bengoli Name is required',
                'm_name_bn.required' => 'Bengoli Name is required',
                'vill_pre.required' => 'Present Village is required',
                'post_pre.required' => 'Present Post Office is required',
                'up_pre.required' => 'Present Upozilla is required',
                'dist_pre.required' => 'Present District is required',
                'vill_per.required' => 'Permanent Village is required',
                'post_per.required' => 'Permanent Post Office is required',
                'up_per.required' => 'Permanent Upozilla is required',
                'dist_per.required' => 'Permanent District is required',
                'edu_lvl.required' => 'Education Level is required',
                'phone.required' => 'Phone is required',
                'nid.required' => 'Valid ID is required',
                'dob.required' => 'Date of Birth is required',
                'relg.required' => 'Please Select a Religious',
                'nation.required' => 'Please Select Your Nationality',
                'gender.required' => 'Please Select your gender',
                'married.required' => 'Please Select your Marital Status',
//                'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
            ]);

        $registrationModel->updateData($request);

        /*  File Manipulation   */
        $filename = '';
//        die("adasdad");
        try {
            if ($request->hasFile('image')) {

                $path = "trainee";

                // cache the file
                $file = $request->file('image');

                // generate a new filename. getClientOriginalExtension() for the file extension
                $filename = $id . '.' . $file->getClientOriginalExtension();
                $filePath = $path . '/' . $id;

                if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                    Storage::delete($filePath . '/' . $filename);
                }

                // save to storage_1/app/public/properties as the new $filename
                $path = $file->storeAs($filePath, $filename);
//            $request->request->add(['p_images' => $filename]);

                Registration::where('id', $id)->update(['image' => $filename]);
            }
        } catch (\Exception $exception) {
            var_dump($exception);
        }


        return redirect('admin/registration/list')->with('success', 'New Registration added successfully');
    }

    public function adminDestroy(Request $request, $id)
    {
        $registrationModel = new Registration();

        DB::beginTransaction();
        $updateData = [
            'is_deleted' => config('constants.is_deleted.Deleted'),
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function adminRegStatusChange(Request $request, $id, $status)
    {
        $registrationModel = new Registration();

        DB::beginTransaction();
        $updateData = [
            'status' => $status,
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Trainee Status has changed Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Could not change trainee status Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function adminExportRegister()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();
//        dd($trainingList);

        $registrationList = $registrationModel->select("users.first_name", 'registrations.*')->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->select('registrations.*', 'users.first_name')
//            ->tosql();
            ->get();
//            ->toArray();

        $this->__exportRegList($registrationList, $trainingList);
    }


    /*  Trainee Registration Head portal portion    */
    public function clientList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();
//        dd($trainingList);

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'registrations.*')->where('registrations.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('registrations.name_en', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');

        if (Auth::user()->created_by == config('constants.userType.SuperUser')) {
            $queryString->where('trainings.dept_id', '=', Auth::user()->dept_id);
        } else {
            $queryString->where('trainings.dept_id', '=', Auth::user()->dept_id);
            $queryString->where('registrations.created_by', '=', Auth::user()->id);
        }
        $registrationList = $queryString->paginate($displayValue);
//            ->select('registrations.*', 'users.first_name')
//            ->tosql();
//        dd($registrationList);

        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        $unionList = $unionModel->pluck('bn_name', 'id')->all();
        $unionList[0] = 'পৌরসভা';

        $addressArr = [$divisionData,$districtList,$upazilaList,$unionList];

        if (request()->ajax()) {
            return view('user.registration.ajax_list', compact('registrationList', 'trainingList', 'addressArr'));
        } else {
            return view('user.registration.clientList', compact('registrationList', 'trainingList', 'addressArr'));
        }
    }

    public function clientListId($id)
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();
//        dd($trainingList);

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'registrations.*')->where('registrations.is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('registrations.training_id', '=', $id)
            ->where('registrations.name_en', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');


        if (Auth::user()->created_by == config('constants.userType.SuperUser')) {
//            $queryString->where('registrations.dept_id', '=', Auth::user()->dept_id);
            $queryString->where('trainings.dept_id', '=', Auth::user()->dept_id);
        } else {
//            $queryString->where('registrations.dept_id', '=', Auth::user()->dept_id);
            $queryString->where('trainings.dept_id', '=', Auth::user()->dept_id);
            $queryString->where('registrations.created_by', '=', Auth::user()->id);
        }
//        $queryString->toSql();
        $registrationList = $queryString->paginate($displayValue);

//        dd($registrationList->toSql());
        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        $unionList = $unionModel->pluck('bn_name', 'id')->all();
        $unionList[0] = 'পৌরসভা';
        $addressArr = [$divisionData,$districtList,$upazilaList,$unionList];

        if (request()->ajax()) {
            return view('user.registration.ajax_list', compact('registrationList', 'trainingList', 'addressArr'));
        } else {
            return view('user.registration.clientList', compact('registrationList', 'trainingList', 'addressArr'));
        }
    }

    public function clientForm()
    {
        $trainingModel = new Training();
        $queryString = $trainingModel::query();
        $queryString->where('status', '=', config('constants.training_status.Pending'))->where('is_deleted', '=', config('constants.is_deleted.Active'));

        if (Auth::user()->created_by == config('constants.userType.SuperUser')) {
            $queryString->where('dept_id', '=', Auth::user()->dept_id);
        } else {
            $queryString->where('created_by', '=', Auth::user()->id);
        }
        $trainingList = $queryString->pluck('name', 'id')->all();

        $eduLevel_list = config('constants.edu_level.arr');
        $eduBoard_list = config('constants.edu_board.arr');
        $relrg_list = config('constants.religious.arr');
        $gender_list = config('constants.gender.arr');
        $marital_list = config('constants.marital_status.arr');

        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        return view('user.registration.clientForm', compact('trainingList', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'divisionData', 'districtList', 'upazilaList'));
    }

    public function clientStore(Request $request)
    {
        $registrationModel = new Registration();

        $formData = $request->all();
        if (isset($formData['same_add'])) {
            $request->request->add(['dist_per' => $formData['dist_pre']]);
            $request->request->add(['up_per' => $formData['up_pre']]);
            $request->request->add(['post_per' => $formData['post_pre']]);
            $request->request->add(['vill_per' => $formData['vill_pre']]);

            unset($request['same_add']);
        }

        $request->request->add(['nid' => $this->bn2en($formData['nid'])]);
        $request->request->add(['phone' => $this->bn2en($formData['phone'])]);

        $data = $this->validate($request, [
            'training_id' => 'required',
            'name_bn' => 'required',
            'name_en' => 'required',
            'f_name_bn' => 'required',
            'm_name_bn' => 'required',
            'vill_pre' => 'required',
            'post_pre' => 'required',
            'up_pre' => 'required',
            'dist_pre' => 'required',
            'vill_per' => 'required',
            'post_per' => 'required',
            'up_per' => 'required',
            'dist_per' => 'required',
            'edu_lvl' => 'required',
            'phone' => 'required',
            'nid' => 'required',
            'dob' => 'required',
            'relg' => 'required',
            'nation' => 'required',
            'gender' => 'required',
            'married' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048'
        ],
            [
                'training_id.required' => 'Please Select a Training',
                'name_bn.required' => 'Bengoli Name is required',
                'name_en.required' => 'English Name is required',
                'f_name_bn.required' => 'Bengoli Name is required',
                'm_name_bn.required' => 'Bengoli Name is required',
                'vill_pre.required' => 'Present Village is required',
                'post_pre.required' => 'Present Post Office is required',
                'up_pre.required' => 'Present Upozilla is required',
                'dist_pre.required' => 'Present District is required',
                'vill_per.required' => 'Permanent Village is required',
                'post_per.required' => 'Permanent Post Office is required',
                'up_per.required' => 'Permanent Upozilla is required',
                'dist_per.required' => 'Permanent District is required',
                'edu_lvl.required' => 'Education Level is required',
                'phone.required' => 'Phone is required',
                'nid.required' => 'Valid ID is required',
                'dob.required' => 'Date of Birth is required',
                'relg.required' => 'Please Select a Religious',
                'nation.required' => 'Please Select Your Nationality',
                'gender.required' => 'Please Select your gender',
                'married.required' => 'Please Select your Marital Status',
//                'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
            ]);

        $lastInsertedId = $registrationModel->saveData($request);

        /*  File Manipulation   */
        $filename = '';
        try {
            if ($request->hasFile('image')) {
                $client_id = $lastInsertedId;
                $path = "trainee";

                // cache the file
                $file = $request->file('image');

                // generate a new filename. getClientOriginalExtension() for the file extension
                $filename = $client_id . '.' . $file->getClientOriginalExtension();
                $filePath = $path . '/' . $client_id;

                if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                    Storage::delete($filePath . '/' . $filename);
                }

                // save to storage_1/app/public/clients as the new $filename
                $path = $file->storeAs($filePath, $filename);

                Registration::where('id', $lastInsertedId)->update(['image' => $filename]);
            }
        } catch (\Exception $exception) {
            var_dump($exception);
        }


        return redirect('client/registration/list')->with('success', 'New Registration added successfully');
    }

    public function clientshow($id)
    {
        //
    }

    public function clientEdit($id)
    {
        $registrationModel = new Registration();
        $trainingModel = new Training();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        try {

            $queryString = $trainingModel::query();
            $queryString->where('status', '=', config('constants.training_status.Pending'))->where('is_deleted', '=', config('constants.is_deleted.Active'));

            if (Auth::user()->created_by == config('constants.userType.SuperUser')) {
                $queryString->where('dept_id', '=', Auth::user()->dept_id);
            } else {
                $queryString->where('created_by', '=', Auth::user()->id);
            }
            $trainingList = $queryString->pluck('name', 'id')->all();

            $eduLevel_list = config('constants.edu_level.arr');
            $eduBoard_list = config('constants.edu_board.arr');
            $relrg_list = config('constants.religious.arr');
            $gender_list = config('constants.gender.arr');
            $marital_list = config('constants.marital_status.arr');

            $registrationData = $registrationModel->where('registrations.id', $id)
                ->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
                ->leftJoin('users', 'registrations.created_by', 'users.id')
                ->select('registrations.*', 'users.first_name')
                ->firstOrFail();

            $districtListPre = $districtModel->pluck('bn_name', 'id')->all();
            $districtListPer = $districtModel->pluck('bn_name', 'id')->all();
            $upazilaListPre = $upazilaModel->where('district_id', $registrationData->dist_pre)->pluck('bn_name', 'id')->all();
            $upazilaListPer = $upazilaModel->where('district_id', $registrationData->dist_per)->pluck('bn_name', 'id')->all();
            $unionListPre = $unionModel->where('upazilla_id', $registrationData->up_pre)->pluck('bn_name', 'id')->all();
            $unionListPer = $unionModel->where('upazilla_id', $registrationData->up_per)->pluck('bn_name', 'id')->all();

            $listArr = [
                $districtListPre, $districtListPer, $upazilaListPre, $upazilaListPer, $unionListPre, $unionListPer
            ];
            return view('user.registration.clientEdit', compact('trainingList', 'id', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'registrationData', 'listArr'));
//            return view('training.adminEdit', compact('deptList', 'id', 'trainingData'));
        } catch (\Exception $ex) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
//            return redirect('client/apt/list')->with('errors', 'No Data Found...');
//            return redirect()->back()->withErrors(['msg', 'The Message']);    When has multiple errors and go back to the previous url...
        }
    }

    public function clientUpdate(Request $request, $id)
    {
        $registrationModel = new Registration();

        $formData = $request->all();
        if (isset($formData['same_add'])) {
            $request->request->add(['dist_per' => $formData['dist_pre']]);
            $request->request->add(['up_per' => $formData['up_pre']]);
            $request->request->add(['post_per' => $formData['post_pre']]);
            $request->request->add(['vill_per' => $formData['vill_pre']]);

            unset($request['same_add']);
        }

        $request->request->add(['nid' => $this->bn2en($formData['nid'])]);
        $request->request->add(['phone' => $this->bn2en($formData['phone'])]);

        $data = $this->validate($request, [
            'training_id' => 'required',
            'name_bn' => 'required',
            'name_en' => 'required',
            'f_name_bn' => 'required',
            'm_name_bn' => 'required',
            'vill_pre' => 'required',
            'post_pre' => 'required',
            'up_pre' => 'required',
            'dist_pre' => 'required',
            'vill_per' => 'required',
            'post_per' => 'required',
            'up_per' => 'required',
            'dist_per' => 'required',
            'edu_lvl' => 'required',
            'phone' => 'required',
            'nid' => 'required',
            'dob' => 'required',
            'relg' => 'required',
            'nation' => 'required',
            'gender' => 'required',
            'married' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048'
        ],
            [
                'training_id.required' => 'Please Select a Training',
                'name_bn.required' => 'Bengoli Name is required',
                'name_en.required' => 'English Name is required',
                'f_name_bn.required' => 'Bengoli Name is required',
                'm_name_bn.required' => 'Bengoli Name is required',
                'vill_pre.required' => 'Present Village is required',
                'post_pre.required' => 'Present Post Office is required',
                'up_pre.required' => 'Present Upozilla is required',
                'dist_pre.required' => 'Present District is required',
                'vill_per.required' => 'Permanent Village is required',
                'post_per.required' => 'Permanent Post Office is required',
                'up_per.required' => 'Permanent Upozilla is required',
                'dist_per.required' => 'Permanent District is required',
                'edu_lvl.required' => 'Education Level is required',
                'phone.required' => 'Phone is required',
                'nid.required' => 'Valid ID is required',
                'dob.required' => 'Date of Birth is required',
                'relg.required' => 'Please Select a Religious',
                'nation.required' => 'Please Select Your Nationality',
                'gender.required' => 'Please Select your gender',
                'married.required' => 'Please Select your Marital Status',
//                'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
            ]);

        $registrationModel->updateData($request);

        /*  File Manipulation   */
        $filename = '';
        try {
            if ($request->hasFile('image')) {
                $path = "trainee";

                // cache the file
                $file = $request->file('image');

                // generate a new filename. getClientOriginalExtension() for the file extension
                $filename = $id . '.' . $file->getClientOriginalExtension();
                $filePath = $path . '/' . $id;

                if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                    Storage::delete($filePath . '/' . $filename);
                }

                // save to storage_1/app/public/properties as the new $filename
                $path = $file->storeAs($filePath, $filename);
//            $request->request->add(['p_images' => $filename]);

                Registration::where('id', $id)->update(['image' => $filename]);
            }
        } catch (\Exception $exception) {
            var_dump($exception);
        }


        return redirect('client/registration/list')->with('success', 'New Registration added successfully');
    }

    public function clientDestroy(Request $request, $id)
    {
        $registrationModel = new Registration();

        DB::beginTransaction();
        $updateData = [
            'is_deleted' => config('constants.is_deleted.Deleted'),
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function clientRegStatusChange(Request $request, $id, $status)
    {
        $registrationModel = new Registration();

        DB::beginTransaction();
        $updateData = [
            'status' => $status,
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Trainee Status has changed Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Could not change trainee status Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function clientExportRegister()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();
//        dd($trainingList);

        $registrationList = $registrationModel->select("users.first_name", 'registrations.*')
            ->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('registrations.dept_id', '=', Auth::user()->dept_id)
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->select('registrations.*', 'users.first_name')
//            ->tosql();
            ->get();
//            ->toArray();

        $this->__exportRegList($registrationList, $trainingList);
    }


    /*  Trainee Registration User portal portion    */
    public function userList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();
//        dd($trainingList);

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'registrations.*')->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('registrations.name_en', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'registrations.created_by', 'users.id');

        if (Auth::user()->created_by == config('constants.userType.SuperUser')) {
            $queryString->where('registrations.dept_id', '=', Auth::user()->dept_id);
        } else {
            $queryString->where('registrations.dept_id', '=', Auth::user()->dept_id);
            $queryString->where('registrations.created_by', '=', Auth::user()->id);
        }
        $registrationList = $queryString->paginate($displayValue);
//            ->select('registrations.*', 'users.first_name')
//            ->tosql();
//        dd($registrationList);

        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        $unionList = $unionModel->pluck('bn_name', 'id')->all();
        $unionList[0] = 'পৌরসভা';
//        dd($unionList);

        $addressArr = [$divisionData,$districtList,$upazilaList,$unionList];

        if (request()->ajax()) {
            return view('user.registration.user_ajax_list', compact('registrationList', 'trainingList', 'addressArr'));
        } else {
            return view('user.registration.userList', compact('registrationList', 'trainingList', 'addressArr'));
        }
    }

    public function userListId($id)
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();
//        dd($trainingList);

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'registrations.*')->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('registrations.training_id', '=', $id)
            ->where('registrations.name_en', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'registrations.created_by', 'users.id');


        if (Auth::user()->created_by == config('constants.userType.SuperUser')) {
            $queryString->where('registrations.dept_id', '=', Auth::user()->dept_id);
        } else {
            $queryString->where('registrations.dept_id', '=', Auth::user()->dept_id);
            $queryString->where('registrations.created_by', '=', Auth::user()->id);
        }
//        $queryString->toSql();
        $registrationList = $queryString->paginate($displayValue);

//        dd($registrationList->toSql());

        if (request()->ajax()) {
            return view('user.registration.user_ajax_list', compact('registrationList', 'trainingList'));
        } else {
            return view('user.registration.userList', compact('registrationList', 'trainingList'));
        }
    }

    public function userForm()
    {
        $trainingModel = new Training();
        $queryString = $trainingModel::query();
        $queryString->where('status', '=', config('constants.training_status.Pending'))->where('is_deleted', '=', config('constants.is_deleted.Active'));

        $queryString->where('dept_id', '=', Auth::user()->dept_id);
//        if(Auth::user()->created_by == config('constants.userType.SuperUser')){
//        }else{
//            $queryString->where('created_by', '=', Auth::user()->id);
//        }
        $trainingList = $queryString->pluck('name', 'id')->all();

        $eduLevel_list = config('constants.edu_level.arr');
        $eduBoard_list = config('constants.edu_board.arr');
        $relrg_list = config('constants.religious.arr');
        $gender_list = config('constants.gender.arr');
        $marital_list = config('constants.marital_status.arr');

        $divisionModel = new Division();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $divisionData = $divisionModel->pluck('bn_name', 'id')->all();
        $districtList = $districtModel->pluck('bn_name', 'id')->all();
        $upazilaList = $upazilaModel->pluck('bn_name', 'id')->all();
        return view('user.registration.userForm', compact('trainingList', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'divisionData', 'districtList', 'upazilaList'));
    }

    public function userStore(Request $request)
    {
        $registrationModel = new Registration();

        $formData = $request->all();
        if (isset($formData['same_add'])) {
            $request->request->add(['dist_per' => $formData['dist_pre']]);
            $request->request->add(['up_per' => $formData['up_pre']]);
            $request->request->add(['post_per' => $formData['post_pre']]);
            $request->request->add(['vill_per' => $formData['vill_pre']]);

            unset($request['same_add']);
        }

        $request->request->add(['nid' => $this->bn2en($formData['nid'])]);
        $request->request->add(['phone' => $this->bn2en($formData['phone'])]);

        $data = $this->validate($request, [
            'training_id' => 'required',
            'name_bn' => 'required',
            'name_en' => 'required',
            'f_name_bn' => 'required',
            'm_name_bn' => 'required',
            'vill_pre' => 'required',
            'post_pre' => 'required',
            'up_pre' => 'required',
            'dist_pre' => 'required',
            'vill_per' => 'required',
            'post_per' => 'required',
            'up_per' => 'required',
            'dist_per' => 'required',
            'edu_lvl' => 'required',
            'phone' => 'required',
            'nid' => 'required',
            'dob' => 'required',
            'relg' => 'required',
            'nation' => 'required',
            'gender' => 'required',
            'married' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048'
        ],
            [
                'training_id.required' => 'Please Select a Training',
                'name_bn.required' => 'Bengoli Name is required',
                'name_en.required' => 'English Name is required',
                'f_name_bn.required' => 'Bengoli Name is required',
                'm_name_bn.required' => 'Bengoli Name is required',
                'vill_pre.required' => 'Present Village is required',
                'post_pre.required' => 'Present Post Office is required',
                'up_pre.required' => 'Present Upozilla is required',
                'dist_pre.required' => 'Present District is required',
                'vill_per.required' => 'Permanent Village is required',
                'post_per.required' => 'Permanent Post Office is required',
                'up_per.required' => 'Permanent Upozilla is required',
                'dist_per.required' => 'Permanent District is required',
                'edu_lvl.required' => 'Education Level is required',
                'phone.required' => 'Phone is required',
                'nid.required' => 'Valid ID is required',
                'dob.required' => 'Date of Birth is required',
                'relg.required' => 'Please Select a Religious',
                'nation.required' => 'Please Select Your Nationality',
                'gender.required' => 'Please Select your gender',
                'married.required' => 'Please Select your Marital Status',
//                'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
            ]);

        $lastInsertedId = $registrationModel->saveData($request);

        /*  File Manipulation   */
        $filename = '';
        try {
            if ($request->hasFile('image')) {
                $client_id = $lastInsertedId;
                $path = "trainee";

                // cache the file
                $file = $request->file('image');

                // generate a new filename. getClientOriginalExtension() for the file extension
                $filename = $client_id . '.' . $file->getClientOriginalExtension();
                $filePath = $path . '/' . $client_id;

                if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                    Storage::delete($filePath . '/' . $filename);
                }

                // save to storage_1/app/public/clients as the new $filename
                $path = $file->storeAs($filePath, $filename);

                Registration::where('id', $lastInsertedId)->update(['image' => $filename]);
            }
        } catch (\Exception $e) {
            var_dump($e);
        }

        return redirect('user/registration/list')->with('success', 'New Registration added successfully');
    }

    public function userhow($id)
    {
        //
    }

    public function userEdit($id)
    {
        $registrationModel = new Registration();
        $trainingModel = new Training();
        $districtModel = new District();
        $upazilaModel = new Upazila();
        $unionModel = new Union();
        try {

            $queryString = $trainingModel::query();
            $queryString->where('status', '=', config('constants.training_status.Pending'))->where('is_deleted', '=', config('constants.is_deleted.Active'));

            $queryString->where('dept_id', '=', Auth::user()->dept_id);
//            if(Auth::user()->created_by == config('constants.userType.SuperUser')){
//            }else{
//                $queryString->where('created_by', '=', Auth::user()->id);
//            }
            $trainingList = $queryString->pluck('name', 'id')->all();

            $eduLevel_list = config('constants.edu_level.arr');
            $eduBoard_list = config('constants.edu_board.arr');
            $relrg_list = config('constants.religious.arr');
            $gender_list = config('constants.gender.arr');
            $marital_list = config('constants.marital_status.arr');

            $registrationData = $registrationModel->where('registrations.id', $id)
                ->where('registrations.is_deleted', '!=', config('constants.is_deleted.Deleted'))
                ->where('registrations.dept_id', '=', Auth::user()->dept_id)
                ->where('registrations.created_by', '=', Auth::user()->id)
                ->leftJoin('users', 'registrations.created_by', 'users.id')
                ->select('registrations.*', 'users.first_name')
                ->firstOrFail();

            $districtListPre = $districtModel->pluck('bn_name', 'id')->all();
            $districtListPer = $districtModel->pluck('bn_name', 'id')->all();
            $upazilaListPre = $upazilaModel->where('district_id', $registrationData->dist_pre)->pluck('bn_name', 'id')->all();
            $upazilaListPer = $upazilaModel->where('district_id', $registrationData->dist_per)->pluck('bn_name', 'id')->all();
            $unionListPre = $unionModel->where('upazilla_id', $registrationData->up_pre)->pluck('bn_name', 'id')->all();
            $unionListPer = $unionModel->where('upazilla_id', $registrationData->up_per)->pluck('bn_name', 'id')->all();

            $listArr = [
                $districtListPre, $districtListPer, $upazilaListPre, $upazilaListPer, $unionListPre, $unionListPer
            ];
            return view('user.registration.userEdit', compact('trainingList', 'id', 'eduLevel_list', 'eduBoard_list', 'relrg_list', 'gender_list', 'marital_list', 'registrationData', 'listArr'));
//            return view('training.adminEdit', compact('deptList', 'id', 'trainingData'));
        } catch (\Exception $ex) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
//            return redirect('client/apt/list')->with('errors', 'No Data Found...');
//            return redirect()->back()->withErrors(['msg', 'The Message']);    When has multiple errors and go back to the previous url...
        }
    }

    public function userUpdate(Request $request, $id)
    {
        $registrationModel = new Registration();

        $formData = $request->all();
        if (isset($formData['same_add'])) {
            $request->request->add(['dist_per' => $formData['dist_pre']]);
            $request->request->add(['up_per' => $formData['up_pre']]);
            $request->request->add(['post_per' => $formData['post_pre']]);
            $request->request->add(['vill_per' => $formData['vill_pre']]);

            unset($request['same_add']);
        }

        $request->request->add(['nid' => $this->bn2en($formData['nid'])]);
        $request->request->add(['phone' => $this->bn2en($formData['phone'])]);

        $data = $this->validate($request, [
            'training_id' => 'required',
            'name_bn' => 'required',
            'name_en' => 'required',
            'f_name_bn' => 'required',
            'm_name_bn' => 'required',
            'vill_pre' => 'required',
            'post_pre' => 'required',
            'up_pre' => 'required',
            'dist_pre' => 'required',
            'vill_per' => 'required',
            'post_per' => 'required',
            'up_per' => 'required',
            'dist_per' => 'required',
            'edu_lvl' => 'required',
            'phone' => 'required',
            'nid' => 'required',
            'dob' => 'required',
            'relg' => 'required',
            'nation' => 'required',
            'gender' => 'required',
            'married' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:2048'
        ],
            [
                'training_id.required' => 'Please Select a Training',
                'name_bn.required' => 'Bengoli Name is required',
                'name_en.required' => 'English Name is required',
                'f_name_bn.required' => 'Bengoli Name is required',
                'm_name_bn.required' => 'Bengoli Name is required',
                'vill_pre.required' => 'Present Village is required',
                'post_pre.required' => 'Present Post Office is required',
                'up_pre.required' => 'Present Upozilla is required',
                'dist_pre.required' => 'Present District is required',
                'vill_per.required' => 'Permanent Village is required',
                'post_per.required' => 'Permanent Post Office is required',
                'up_per.required' => 'Permanent Upozilla is required',
                'dist_per.required' => 'Permanent District is required',
                'edu_lvl.required' => 'Education Level is required',
                'phone.required' => 'Phone is required',
                'nid.required' => 'Valid ID is required',
                'dob.required' => 'Date of Birth is required',
                'relg.required' => 'Please Select a Religious',
                'nation.required' => 'Please Select Your Nationality',
                'gender.required' => 'Please Select your gender',
                'married.required' => 'Please Select your Marital Status',
//                'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
            ]);

        $registrationModel->updateData($request);

        /*  File Manipulation   */
        $filename = '';
        try {
            if ($request->hasFile('image')) {

                $path = "trainee";

                // cache the file
                $file = $request->file('image');

                // generate a new filename. getClientOriginalExtension() for the file extension
                $filename = $id . '.' . $file->getClientOriginalExtension();
                $filePath = $path . '/' . $id;

                if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                    Storage::delete($filePath . '/' . $filename);
                }

                // save to storage_1/app/public/properties as the new $filename
                $path = $file->storeAs($filePath, $filename);
//            $request->request->add(['p_images' => $filename]);

                Registration::where('id', $id)->update(['image' => $filename]);
            }
        } catch (\Exception $exception) {
            var_dump($exception);
        }

        return redirect('user/registration/list')->with('success', 'New Registration added successfully');
    }

    public function userDestroy(Request $request, $id)
    {
        $registrationModel = new Registration();

        DB::beginTransaction();
        $updateData = [
            'is_deleted' => config('constants.is_deleted.Deleted'),
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function userRegStatusChange(Request $request, $id, $status)
    {
        $registrationModel = new Registration();

        DB::beginTransaction();
        $updateData = [
            'status' => $status,
        ];

        if (isset($request->id)) {
            $registrationModel->where('id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Trainee Status has changed Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Could not change trainee status Successfully..');
            return response()->json(['status' => 'error']);
        }
    }


    /*  Common Function */

    private function __exportRegList($registrationList, $trainingList)
    {
        $registration_array[] = array('Sr. No.', 'প্রশিক্ষণার্থীর নাম', 'প্রশিক্ষণের নাম', 'মোবাইল নাম্বার', 'ই-মেইল', 'জন্ম নিবন্ধন / ভোটার আইডি নাম্বার / NID', 'বয়স', 'বর্তমান ঠিকানা', 'শিক্ষাগত যোগ্যতা', 'Status');

        foreach ($registrationList as $srNo => $registration) {
//            dd($registration->name_bn);
            $registration_array[] = array(
                'Sr. No.' => $srNo + 1,
                'প্রশিক্ষণার্থীর নাম' => $registration->name_bn,
                'প্রশিক্ষণের নাম' => $trainingList[$registration->training_id],
                'মোবাইল নাম্বার' => $registration->phone,
                'ই-মেইল' => $registration->email,
                'জন্ম নিবন্ধন / ভোটার আইডি নাম্বার' => $registration->nid,
                'বয়স' => $registration->age,
                'বর্তমান ঠিকানা' => $registration->vill_pre . ', ' . $registration->post_pre . ', ' . $registration->up_pre . ', ' . $registration->dist_pre,
                'শিক্ষাগত যোগ্যতা' => config('constants.edu_level.arr.' . $registration->edu_lvl),
                'Status' => ($registration->status == config('constants.registration_status.Waiting')) ? config('constants.registration_status.1') : (($registration->status == config('constants.registration_status.Approved')) ? config('constants.registration_status.2') : (($registration->status == config('constants.registration_status.Confirmed')) ? config('constants.registration_status.3') : config('constants.registration_status.0'))),
            );
        }

        Excel::create("Registration List", function ($excel) use ($registration_array) {
            $excel->setTitle('Registration List');
            $excel->sheet('Registration List', function ($sheet) use ($registration_array) {
                $sheet->fromArray($registration_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }

    public static $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
    public static $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");

    public static function bn2en($number)
    {
        return str_replace(self::$bn, self::$en, $number);
    }
}
