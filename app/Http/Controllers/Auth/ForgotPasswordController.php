<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    public function __construct()
    {
        $this->middleware('guest:web');
    }

    protected function validateEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email'
        ]);

        $request->request->add(['user_type' => 2]);
    }
}
