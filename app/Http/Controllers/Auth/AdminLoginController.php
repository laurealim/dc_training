<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AdminLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo = '/admin';


    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm()
    {
        return view('admin.login');
    }

//    public function login(Request $request)
//    {
//        // Validate the form data
//        $this->validate($request, [
//            'email' => 'required|email',
//            'password' => 'required|min:6'
//        ]);
//
//        // Attempt to log the user in
//        if ($this->guard()->attempt(['email' => $request->email, 'password' => $request->password, 'user_type' => 1], $request->remember)) {
//            // if successful, then redirect to their intended location
//            return redirect()->intended(route('admin.dashboard'));
//        }
//        // if unsuccessful, then redirect back to the login with the form data
//        return redirect()->back()->withInput($request->only('email', 'remember'));
//    }

    public function logout()
    {
        $this->guard()->logout();

        return redirect('/admin/login');
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

    protected function credentials(Request $request)
    {
        $user_type = config('constants.userType.SuperUser');
        $user_status = config('constants.status.Active');
        return [
            "email" => $request->get($this->username()),
            'password' => $request->password,
            'user_type' => $user_type,
            'status' =>  $user_status,
        ];

    }

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
            'login_error' => 'Errors in Login',
        ]);
    }
}
