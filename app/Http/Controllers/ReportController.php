<?php

namespace App\Http\Controllers;

use App\Model\Department;
use App\Model\Registration;
use App\Model\Trade;
use App\Model\Training;
use Illuminate\Http\Request;
use Excel;
use Auth;

class ReportController extends Controller
{
    /*  Admin Section   */
    public function adminTrainingWiseRegistration()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $tradeModel = new Trade();

        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'trainings.trade_name', 'registrations.*')
//            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
//            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $trade_name = "";
        $reg_status = "";
        $registrationStatusArr = [
            1 => 'Waiting',
            2 => 'Approved',
            3 => 'Confirmed',
        ];
        $titleName = "Registration Report";
        if (request()->ajax()) {
            if (isset($_GET['training_id']) && !empty($_GET['training_id'])) {
                $training_id = $_GET['training_id'];
                $queryString->where('registrations.training_id', '=', $_GET['training_id']);
            }
            if (isset($_GET['reg_status']) && !empty($_GET['reg_status'])) {
                $reg_status = $_GET['reg_status'];
                $queryString->where('registrations.status', '=', $_GET['reg_status']);
            }
            if (isset($_GET['trade_name']) && !empty($_GET['trade_name'])) {
                $trade_name = $_GET['trade_name'];
                $queryString->where('trainings.trade_name', '=', $_GET['trade_name']);
            }
            $registrationList = $queryString->get();
            return view('report.ajax_training_wise_registration', compact('registrationList', 'departmentList', 'trainingList', 'training_id', 'reg_status', 'trade_name', 'registrationStatusArr', 'titleName', 'tradeList'));
        } else {
            $registrationList = $queryString->get();
            return view('report.adminTrainingWiseRegistration', compact('registrationList', 'departmentList', 'trainingList', 'training_id', 'reg_status', 'trade_name', 'registrationStatusArr', 'titleName', 'tradeList'));
        }
    }

    public function adminExportTrainingTrainingWiseRegistration(Request $request)
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $tradeModel = new Trade();

        $trainingList = $trainingModel->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'trainings.trade_name', 'registrations.*')
//            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
//            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $training_status = "";
        $reg_status = "";

//        dd($_POST);

        if (isset($_POST['training_id']) && !empty($_POST['training_id'])) {
            $training_id = $_POST['training_id'];
            $queryString->where('registrations.training_id', '=', $_POST['training_id']);
        }
        if (isset($_POST['reg_status']) && !empty($_POST['reg_status'])) {
            $reg_status = $_POST['reg_status'];
            $queryString->where('registrations.status', '=', $_POST['reg_status']);
        }
        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
            $trade_name = $_POST['trade_name'];
            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
        }
        $registrationList = $queryString->get();
        $this->__exportRegList($registrationList, $trainingList, $departmentList);
    }

    public function adminTrainingReport()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $tradeModel = new Trade();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();
        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $trainingModel::query();
        $queryString = $trainingModel::query();
        $queryString->select("users.first_name")->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
//            ->where('trainings.name', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->select('trainings.*', 'users.first_name');
//            ->tosql();
//            ->get();
        if (request()->ajax()) {
//            dd($_GET);
            if (isset($_GET['dept_id']) && !empty($_GET['dept_id'])) {
                $queryString->where('trainings.dept_id', '=', $_GET['dept_id']);
            }
            if (isset($_GET['start_date']) && !empty($_GET['start_date'])) {
                $strDate = date('Y-m-d', strtotime($_GET['start_date']));
                $strDate = $strDate . " 00:00:00";
                $queryString->where('trainings.start_date', '>=', $strDate);

                $endDate = date("Y-m-d", strtotime("now")) . " 00:00:00";
                if (isset($_GET['end_date']) && !empty($_GET['end_date'])) {
                    $endDate = date('Y-m-d', strtotime($_GET['end_date']));
                    $endDate = $endDate . " 00:00:00";
                    $queryString->where('trainings.end_date', '<=', $endDate);
                }
//                $queryString->whereBetween('created_at',[$strDate,$endDate]);
            }
            if (isset($_GET['status']) && !empty($_GET['status'])) {
                $queryString->where('trainings.status', '=', $_GET['status']);
            }
            if (isset($_GET['trade_name']) && !empty($_GET['trade_name'])) {
                $trade_name = $_GET['trade_name'];
                $queryString->where('trainings.trade_name', '=', $_GET['trade_name']);
            }

            $trainingList = $queryString->get();
            return view('report.ajax_training_report', compact('trainingList', 'departmentList', 'tradeList'));
        } else {
            $trainingList = $queryString->get();
            return view('report.adminTrainingReport', compact('trainingList', 'departmentList', 'tradeList'));
        }
    }

    public function adminTrainingReportExport()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $queryString = $trainingModel::query();
        $queryString = $trainingModel::query();
        $queryString->select("users.first_name")->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
//            ->where('trainings.name', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->select('trainings.*', 'users.first_name');
//            ->tosql();
//            ->get();
//        if (request()->ajax()) {
//            dd($_POST);
        if (isset($_POST['dept_id']) && !empty($_POST['dept_id'])) {
            $queryString->where('trainings.dept_id', '=', $_POST['dept_id']);
        }
        if (isset($_POST['start_date']) && !empty($_POST['start_date'])) {
            $strDate = date('Y-m-d', strtotime($_POST['start_date']));
            $strDate = $strDate . " 00:00:00";
            $queryString->where('trainings.start_date', '>=', $strDate);

            $endDate = date("Y-m-d", strtotime("now")) . " 00:00:00";
            if (isset($_POST['end_date']) && !empty($_POST['end_date'])) {
                $endDate = date('Y-m-d', strtotime($_POST['end_date']));
                $endDate = $endDate . " 00:00:00";
                $queryString->where('trainings.end_date', '<=', $endDate);
            }
        }
        if (isset($_POST['status']) && !empty($_POST['status'])) {
            $queryString->where('trainings.status', '=', $_POST['status']);
        }

        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
            $trade_name = $_POST['trade_name'];
            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
        }
//        }

        $trainingList = $queryString->get();
        $this->__exportTrainingList($trainingList, $departmentList);

    }

    public function adminParticipantReport()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $tradeModel = new Trade();

        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'trainings.trade_name', 'registrations.*')
            ->where('registrations.status', '!=', config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
//            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $trade_name = "";
        $reg_status = "";
        $registrationStatusArr = [
            2 => 'Approved',
            3 => 'Confirmed',
        ];
        $titleName = "Participant Report";
        if (request()->ajax()) {
            if (isset($_GET['training_id']) && !empty($_GET['training_id'])) {
                $training_id = $_GET['training_id'];
                $queryString->where('registrations.training_id', '=', $_GET['training_id']);
            }
            if (isset($_GET['reg_status']) && !empty($_GET['reg_status'])) {
                $trade_name = $_GET['reg_status'];
                $queryString->where('registrations.status', '=', $_GET['reg_status']);
            }
            if (isset($_GET['trade_name']) && !empty($_GET['trade_name'])) {
                $reg_status = $_GET['trade_name'];
                $queryString->where('trainings.trade_name', '=', $_GET['trade_name']);
            }
            $registrationList = $queryString->get();
            return view('report.ajax_participant_report', compact('registrationList', 'departmentList', 'trainingList', 'training_id', 'trade_name', 'reg_status', 'registrationStatusArr', 'titleName', 'tradeList'));
        } else {
            $registrationList = $queryString->get();
            return view('report.adminParticipantReport', compact('registrationList', 'departmentList', 'trainingList', 'training_id', 'trade_name', 'reg_status', 'registrationStatusArr', 'titleName','tradeList'));
        }
    }

    public function adminParticipantReportExport(Request $request)
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();

        $trainingList = $trainingModel->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'registrations.*')
            ->where('registrations.status', '!=', config('constants.registration_status.Waiting'))
//            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
//            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $training_status = "";
        $reg_status = "";

        if (isset($_POST['training_id']) && !empty($_POST['training_id'])) {
            $training_id = $_POST['training_id'];
            $queryString->where('registrations.training_id', '=', $_POST['training_id']);
        }
        if (isset($_POST['reg_status']) && !empty($_POST['reg_status'])) {
            $training_status = $_POST['reg_status'];
            $queryString->where('registrations.status', '=', $_POST['reg_status']);
        }
        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
            $reg_status = $_POST['trade_name'];
            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
        }
        $registrationList = $queryString->get();
        $this->__exportRegList($registrationList, $trainingList, $departmentList);
    }


    /*  Client Section  */
    public function clientTrainingWiseRegistration()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $tradeModel = new Trade();

        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

//        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
//        ->pluck('name', 'id')->all();
        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'trainings.trade_name', 'registrations.*')
            ->where('registrations.dept_id', '=', Auth::user()->dept_id)
//            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
//            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $trade_name = "";
        $reg_status = "";
        $registrationStatusArr = [
            1 => 'Waiting',
            2 => 'Approved',
            3 => 'Confirmed',
        ];
        if (request()->ajax()) {
            if (isset($_GET['training_id']) && !empty($_GET['training_id'])) {
                $training_id = $_GET['training_id'];
                $queryString->where('registrations.training_id', '=', $_GET['training_id']);
            }
            if (isset($_GET['reg_status']) && !empty($_GET['reg_status'])) {
                $training_status = $_GET['reg_status'];
                $queryString->where('registrations.status', '=', $_GET['reg_status']);
            }
            if (isset($_GET['trade_name']) && !empty($_GET['trade_name'])) {
                $reg_status = $_GET['trade_name'];
                $queryString->where('trainings.trade_name', '=', $_GET['trade_name']);
            }
            $registrationList = $queryString->get();
            return view('report.ajax_training_wise_registration', compact('registrationList', 'trainingList', 'training_id', 'trade_name', 'reg_status', 'registrationStatusArr','tradeList'));
        } else {
            $registrationList = $queryString->get();
            return view('report.clientTrainingWiseRegistration', compact('registrationList', 'trainingList', 'training_id', 'trade_name', 'reg_status', 'registrationStatusArr','tradeList'));
        }
    }

    public function clientExportTrainingTrainingWiseRegistration(Request $request)
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $tradeModel = new Trade();

        $trainingList = $trainingModel->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))->where('id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'trainings.trade_name', 'registrations.*')
            ->where('registrations.dept_id', '=', Auth::user()->dept_id)
//            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
//            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $training_status = "";
        $reg_status = "";

        if (isset($_POST['training_id']) && !empty($_POST['training_id'])) {
            $training_id = $_POST['training_id'];
            $queryString->where('registrations.training_id', '=', $_POST['training_id']);
        }
        if (isset($_POST['reg_status']) && !empty($_POST['reg_status'])) {
            $training_status = $_POST['reg_status'];
            $queryString->where('registrations.status', '=', $_POST['reg_status']);
        }
        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
            $reg_status = $_POST['trade_name'];
            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
        }
        $registrationList = $queryString->toSql();
        $this->__exportRegList($registrationList, $trainingList, $departmentList);
    }

    public function clientTrainingReport()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $tradeModel = new Trade();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->where('departments.id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $trainingModel::query();
        $queryString->select("users.first_name")->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
//            ->where('trainings.name', 'like', '%' . $searchData . '%')
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->select('trainings.*', 'users.first_name');
//            ->tosql();
//            ->get();
//        dd($tradeList);
        if (request()->ajax()) {
//            dd($_GET);
            if (isset($_GET['dept_id']) && !empty($_GET['dept_id'])) {
                $queryString->where('trainings.dept_id', '=', $_GET['dept_id']);
            }
            if (isset($_GET['start_date']) && !empty($_GET['start_date'])) {
                $strDate = date('Y-m-d', strtotime($_GET['start_date']));
                $strDate = $strDate . " 00:00:00";
                $queryString->where('trainings.start_date', '>=', $strDate);

                $endDate = date("Y-m-d", strtotime("now")) . " 00:00:00";
                if (isset($_GET['end_date']) && !empty($_GET['end_date'])) {
                    $endDate = date('Y-m-d', strtotime($_GET['end_date']));
                    $endDate = $endDate . " 00:00:00";
                    $queryString->where('trainings.end_date', '<=', $endDate);
                }
//                $queryString->whereBetween('created_at',[$strDate,$endDate]);
            }
            if (isset($_GET['status']) && !empty($_GET['status'])) {
                $queryString->where('trainings.status', '=', $_GET['status']);
            }
            if (isset($_GET['trade_name']) && !empty($_GET['trade_name'])) {
                $queryString->where('trainings.trade_name', '=', $_GET['trade_name']);
            }

            $trainingList = $queryString->get();
            return view('report.ajax_training_report', compact('trainingList', 'departmentList','tradeList'));
        } else {
            $trainingList = $queryString->get();
            return view('report.clientTrainingReport', compact('trainingList', 'departmentList', 'tradeList'));
        }
    }

    public function clientTrainingReportExport()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $tradeModel = new Trade();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->where('departments.id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();
        $queryString = $trainingModel::query();
        $queryString->select("users.first_name")->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
//            ->where('trainings.name', 'like', '%' . $searchData . '%')
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->select('trainings.*', 'users.first_name');
//            ->tosql();
//            ->get();
//        if (request()->ajax()) {
//            dd($_POST);
        if (isset($_POST['dept_id']) && !empty($_POST['dept_id'])) {
            $queryString->where('trainings.dept_id', '=', $_POST['dept_id']);
        }
        if (isset($_POST['start_date']) && !empty($_POST['start_date'])) {
            $strDate = date('Y-m-d', strtotime($_POST['start_date']));
            $strDate = $strDate . " 00:00:00";
            $queryString->where('trainings.start_date', '>=', $strDate);

            $endDate = date("Y-m-d", strtotime("now")) . " 00:00:00";
            if (isset($_POST['end_date']) && !empty($_POST['end_date'])) {
                $endDate = date('Y-m-d', strtotime($_POST['end_date']));
                $endDate = $endDate . " 00:00:00";
                $queryString->where('trainings.end_date', '<=', $endDate);
            }
        }
        if (isset($_POST['status']) && !empty($_POST['status'])) {
            $queryString->where('trainings.status', '=', $_POST['status']);
        }
        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
        }
//        }

        $trainingList = $queryString->get();
        $this->__exportTrainingList($trainingList, $departmentList);

    }

    public function clientParticipantReport()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $tradeModel = new Trade();

        $trainingList = $trainingModel->where('status', '=', config('constants.training_status.Pending'))
            ->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

//        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
//        ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'trainings.trade_name', 'registrations.*')
            ->where('registrations.dept_id', '=', Auth::user()->dept_id)
            ->where('registrations.status', '!=', config('constants.registration_status.Waiting'))
//            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
//            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $trade_name = "";
        $reg_status = "";

        $registrationStatusArr = [
            2 => 'Approved',
            3 => 'Confirmed',
        ];
        $titleName = "Participant Report";
        $registrationList = $queryString->get();
        if (request()->ajax()) {
            if (isset($_GET['training_id']) && !empty($_GET['training_id'])) {
                $training_id = $_GET['training_id'];
                $queryString->where('registrations.training_id', '=', $_GET['training_id']);
            }
            if (isset($_GET['reg_status']) && !empty($_GET['reg_status'])) {
                $training_status = $_GET['reg_status'];
                $queryString->where('registrations.status', '=', $_GET['reg_status']);
            }
            if (isset($_GET['trade_name']) && !empty($_GET['trade_name'])) {
                $trade_name = $_GET['trade_name'];
                $queryString->where('trainings.trade_name', '=', $_GET['trade_name']);
            }

            $registrationList = $queryString->get();
            return view('report.ajax_participant_report', compact('registrationList', 'trainingList', 'training_id', 'trade_name', 'reg_status', 'registrationStatusArr', 'titleName', 'tradeList'));
        } else {
            $registrationList = $queryString->get();
            return view('report.clientParticipantReport', compact('registrationList', 'trainingList', 'training_id', 'trade_name', 'reg_status', 'registrationStatusArr', 'titleName', 'tradeList'));
        }
    }

    public function clientParticipantReportExport(Request $request)
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $registrationModel = new Registration();
        $tradeModel = new Trade();

        $trainingList = $trainingModel->where('is_deleted', '=', config('constants.is_deleted.Active'))
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))->where('id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $queryString = $registrationModel::query();
        $queryString->select("users.first_name", 'trainings.trade_name', 'registrations.*')
            ->where('registrations.dept_id', '=', Auth::user()->dept_id)
            ->where('registrations.status', '!=', config('constants.registration_status.Waiting'))
//            ->where('registrations.status','!=',config('constants.registration_status.Waiting'))
            ->leftJoin('users', 'registrations.created_by', 'users.id')
            ->leftJoin('trainings', 'registrations.training_id', 'trainings.id');
//            ->tosql();
//            ->paginate($displayValue);

        $training_id = "";
        $training_status = "";
        $reg_status = "";

        if (isset($_POST['training_id']) && !empty($_POST['training_id'])) {
            $training_id = $_POST['training_id'];
            $queryString->where('registrations.training_id', '=', $_POST['training_id']);
        }
        if (isset($_POST['reg_status']) && !empty($_POST['reg_status'])) {
            $training_status = $_POST['reg_status'];
            $queryString->where('registrations.status', '=', $_POST['reg_status']);
        }
        if (isset($_POST['trade_name']) && !empty($_POST['trade_name'])) {
            $reg_status = $_POST['trade_name'];
            $queryString->where('trainings.trade_name', '=', $_POST['trade_name']);
        }
//        echo $queryString->toSql();
//        die;
        $registrationList = $queryString->get();
        $this->__exportRegList($registrationList, $trainingList, $departmentList);
    }


    /*  Common Function */
    private function __exportTrainingList($trainingList, $departmentList)
    {
        $training_array[] = array('Sr. No.', 'প্রশিক্ষণের নাম', 'প্রশিক্ষণের স্থান', 'প্রশিক্ষণের সময়কাল (দিন)', 'প্রশিক্ষণ প্রদানকারী সংস্থা', 'প্রতিষ্ঠানের কোড নং', 'প্রশিক্ষণ অনুমোদনকারী সংস্থা', 'প্রশিক্ষণ অর্থায়নকারী সংস্থা', 'অর্থায়ন মূল্য', 'ট্রেনিং এর শুরুর দিন', 'পরীক্ষার দিন', 'Status');

        foreach ($trainingList as $srNo => $training) {
//            dd($registration->name_bn);
            $training_array[] = array(
                'Sr. No.' => $srNo + 1,
                'প্রশিক্ষণের নাম' => $training->name,
                'প্রশিক্ষণের স্থান' => $training->training_place,
                'প্রশিক্ষণের সময়কাল (দিন)' => $training->total_day,
                'প্রশিক্ষণ প্রদানকারী সংস্থা' => $departmentList[$training->dept_id],
                'প্রতিষ্ঠানের কোড নং' => $training->code,
                'প্রশিক্ষণ অনুমোদনকারী সংস্থা' => $training->proposal_org,
                'প্রশিক্ষণ অর্থায়নকারী সংস্থা' => $training->financial_org,
                'অর্থায়ন মূল্য' => $training->budget,
                'ট্রেনিং এর শুরুর দিন' => date('Y-m-d', strtotime($training->start_date)),
                'পরীক্ষার দিন' => date('Y-m-d', strtotime($training->exam_date)),
                'Status' => ($training->status == config('constants.training_status.Pending')) ? config('constants.training_status.1') : (($training->status == config('constants.training_status.Complete')) ? config('constants.training_status.2') : config('constants.training_status.0')),
            );
        }

        Excel::create("Training List", function ($excel) use ($training_array) {
            $excel->setTitle('Training List');
            $excel->sheet('Training List', function ($sheet) use ($training_array) {
                $sheet->fromArray($training_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }

    private function __exportRegList($registrationList, $trainingList, $departmentList)
    {
        $registration_array[] = array('Sr. No.', 'অফিসের নাম', 'প্রশিক্ষণার্থীর নাম', 'প্রশিক্ষণের নাম', 'মোবাইল নাম্বার', 'ই-মেইল', 'জন্ম নিবন্ধন / ভোটার আইডি নাম্বার', 'বয়স', 'বর্তমান ঠিকানা', 'শিক্ষাগত যোগ্যতা', 'Status');

        foreach ($registrationList as $srNo => $registration) {
//            dd($registration->name_bn);
            $registration_array[] = array(
                'Sr. No.' => $srNo + 1,
                'অফিসের নাম' => $departmentList[$registration->dept_id],
                'প্রশিক্ষণার্থীর নাম' => $registration->name_bn,
                'প্রশিক্ষণের নাম' => $trainingList[$registration->training_id],
                'মোবাইল নাম্বার' => $registration->phone,
                'ই-মেইল' => $registration->email,
                'জন্ম নিবন্ধন / ভোটার আইডি নাম্বার' => $registration->nid,
                'বয়স' => $registration->age,
                'বর্তমান ঠিকানা' => $registration->vill_pre . ', ' . $registration->post_pre . ', ' . $registration->up_pre . ', ' . $registration->dist_pre,
                'শিক্ষাগত যোগ্যতা' => config('constants.edu_level.arr.' . $registration->edu_lvl),
                'Status' => ($registration->status == config('constants.registration_status.Waiting')) ? config('constants.registration_status.1') : (($registration->status == config('constants.registration_status.Approved')) ? config('constants.registration_status.2') : (($registration->status == config('constants.registration_status.Confirmed')) ? config('constants.registration_status.3') : config('constants.registration_status.0'))),
            );
        }

        Excel::create("Training wise Registration List", function ($excel) use ($registration_array) {
            $excel->setTitle('Training wise Registration List');
            $excel->sheet('Training wise Registration List', function ($sheet) use ($registration_array) {
                $sheet->fromArray($registration_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }
}
