<?php

namespace App\Http\Controllers;

use App\Model\Department;
use App\Model\Registration;
use App\Model\Trade;
use App\Model\Training;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use Excel;
use Illuminate\Validation\ValidationException;

//use Maatwebsite\Excel\Excel;

class TrainingController extends Controller
{

    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $tradeModel = new Trade();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $trainingList = $trainingModel->select("users.first_name")->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('trainings.name', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->select('trainings.*', 'users.first_name')
//            ->tosql();
            ->paginate($displayValue);

        if (request()->ajax()) {
            return view('training.ajax_list', compact('trainingList', 'departmentList', 'tradeList'));
        } else {
            return view('training.adminList', compact('trainingList', 'departmentList', 'tradeList'));
        }
    }

    public function adminForm()
    {
        $departmentModel = new Department();
        $trainingModel = new Training();
        $tradeModel = new Trade();
        $deptList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $trainingTradeList = $trainingModel->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))->get();
        $tradeList = $tradeModel->where('status', '!=', config('constants.status.Deleted'))->pluck('trade_name', 'id')->all();

        $trainingList = $trainingModel->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))->pluck('name', 'id');

//        dd($trainingTradeList);
        return view('training.adminForm', compact('deptList', 'trainingList', 'trainingTradeList','tradeList'));
    }

    public function adminStore(Request $request)
    {
        $trainingModel = new Training();
        unset($request['training_type']);
        unset($request['trainingList']);
//        dd($request);

        $data = $this->validate($request, [
            'name' => 'required',
            'training_place' => 'required',
            'dept_id' => 'required',
            'total_day' => 'required',
            'budget' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
//            'exam_date' => 'required',
            'status' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
        ], [
            'name.required' => 'Name is required',
            'training_place.required' => 'Training Place is required',
            'dept_id.required' => 'Department required',
            'total_day.required' => 'Total Training day is required',
            'budget.required' => 'Budget is required',
            'start_date.required' => 'Please Select Training Start Date',
            'end_date.required' => 'Please Select Training End Date',
//            'exam_date.required' => 'Please Select Training Exam Date',
            'status.required' => 'Please Select the training status',
        ]);

        $trainingModel->saveData($request);
        return redirect('admin/training/list')->with('success', 'New Training added successfully');

    }

    public function show($id)
    {
        //
    }

    public function adminEdit($id)
    {
        $departmentModel = new Department();
        $trainingModel = new Training();
        $tradeModel = new Trade();
        try {

            $deptList = $departmentModel->where('status', '=', config('constants.status.Active'))
                ->pluck('name', 'id')->all();

            $tradeList = $tradeModel->where('status', '!=', config('constants.status.Deleted'))->pluck('trade_name', 'id')->all();

            $trainingData = $trainingModel->where('trainings.id', $id)
                ->leftJoin('users', 'trainings.created_by', 'users.id')
                ->select('trainings.*', 'users.first_name')
                ->firstOrFail();
            return view('training.adminEdit', compact('deptList', 'id', 'trainingData','tradeList'));
        } catch (\Exception $ex) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
//            return redirect('client/apt/list')->with('errors', 'No Data Found...');
//            return redirect()->back()->withErrors(['msg', 'The Message']);    When has multiple errors and go back to the previous url...
        }
    }

    public function adminUpdate(Request $request, $id)
    {
        $trainingModel = new Training();
        $data = $this->validate($request, [
            'name' => 'required',
            'training_place' => 'required',
            'dept_id' => 'required',
            'total_day' => 'required',
            'budget' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
//            'exam_date' => 'required',
            'status' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
        ], [
            'name.required' => 'Name is required',
            'training_place.required' => 'Training Place is required',
            'dept_id.required' => 'Department required',
            'total_day.required' => 'Total Training day is required',
            'budget.required' => 'Budget is required',
            'start_date.required' => 'Please Select Training Start Date',
            'end_date.required' => 'Please Select Training End Date',
//            'exam_date.required' => 'Please Select Training Exam Date',
            'status.required' => 'Please Select the training status',
        ]);

        $trainingModel->updateData($request);
        return redirect('admin/training/list')->with('success', 'Training edited successfully');
    }

    public function adminDestroy(Request $request, $id)
    {
        $trainingModel = new Training();

        DB::beginTransaction();
        $updateData = [
            'is_deleted' => config('constants.is_deleted.Deleted'),
        ];

        if (isset($request->id)) {
            $trainingModel->where('id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function adminUserLists($id, Request $request)
    {

    }

    public function adminExportTraining()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $trainingList = $trainingModel->select("users.first_name")->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->select('trainings.*', 'users.first_name')
//            ->tosql();
            ->get();

        $this->__exportTrainingList($trainingList, $departmentList);
    }


    /*  Department Head Training Section  */
    public function clientList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $trainingModel = new Training();
        $departmentModel = new Department();
        $tradeModel = new Trade();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();
        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();

        $trainingList = $trainingModel->select("users.first_name")->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
//            ->where('trainings.created_by','=',Auth::user()->created_by)
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->where('trainings.name', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->select('trainings.*', 'users.first_name')
//            ->tosql();
            ->paginate($displayValue);

        if (request()->ajax()) {
            return view('user.training.ajax_list', compact('trainingList', 'departmentList', 'tradeList'));
        } else {
            return view('user.training.clientList', compact('trainingList', 'departmentList', 'tradeList'));
        }
    }

    public function clientForm()
    {
        $departmentModel = new Department();
        $trainingModel = new Training();
        $tradeModel = new Trade();
        $deptList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->pluck('name', 'id')->all();

        $tradeList = $tradeModel->where('status', '!=', config('constants.status.Deleted'))->pluck('trade_name', 'id')->all();

        $trainingTradeList = $trainingModel->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->get();
        $trainingList = $trainingModel->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('trainings.dept_id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id');

        return view('user.training.clientForm', compact('deptList', 'trainingList', 'trainingTradeList','tradeList'));
    }

    public function clientStore(Request $request)
    {
        $trainingModel = new Training();
        unset($request['training_type']);
        unset($request['trainingList']);

        $validateField = [
            'name' => 'required',
            'training_place' => 'required',
            'total_day' => 'required',
            'budget' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
//            'exam_date' => 'required',
            'status' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
        ];
        $validateMessage = [
            'name.required' => 'Name is required',
            'training_place.required' => 'Training Place is required',
            'total_day.required' => 'Total Training day is required',
            'budget.required' => 'Budget is required',
            'start_date.required' => 'Please Select Training Start Date',
            'end_date.required' => 'Please Select Training End Date',
//            'exam_date.required' => 'Please Select Training Exam Date',
            'status.required' => 'Please Select the training status',
        ];

        $data = $this->validate($request, $validateField, $validateMessage);
        $formData['dept_id'] = Auth::user()->dept_id;

        $trainingModel->saveData($request);
        return redirect('client/training/list')->with('success', 'New Training added successfully');

    }

    public function clientshow($id)
    {
        //
    }

    public function clientEdit($id)
    {
        $departmentModel = new Department();
        $trainingModel = new Training();
        $tradeModel = new Trade();
        try {

            $deptList = $departmentModel->where('status', '=', config('constants.status.Active'))
                ->pluck('name', 'id')->all();

            $tradeList = $tradeModel->where('status', '!=', config('constants.status.Deleted'))->pluck('trade_name', 'id')->all();

            $trainingData = $trainingModel->where('trainings.id', $id)
                ->leftJoin('users', 'trainings.created_by', 'users.id')
                ->select('trainings.*', 'users.first_name')
                ->firstOrFail();
            return view('user.training.clientEdit', compact('deptList', 'id', 'trainingData','tradeList'));
        } catch (\Exception $ex) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
//            return redirect('client/apt/list')->with('errors', 'No Data Found...');
//            return redirect()->back()->withErrors(['msg', 'The Message']);    When has multiple errors and go back to the previous url...
        }
    }

    public function clientUpdate(Request $request, $id)
    {
        $trainingModel = new Training();

        $validateField = [
            'name' => 'required',
            'training_place' => 'required',
            'total_day' => 'required',
            'budget' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
//            'exam_date' => 'required',
            'status' => 'required',
        ];
        $validateMessage = [
            'name.required' => 'Name is required',
            'training_place.required' => 'Training Place is required',
            'total_day.required' => 'Total Training day is required',
            'budget.required' => 'Budget is required',
            'start_date.required' => 'Please Select Training Start Date',
            'end_date.required' => 'Please Select Training End Date',
//            'exam_date.required' => 'Please Select Training Exam Date',
            'status.required' => 'Please Select the training status',
        ];

        $data = $this->validate($request, $validateField, $validateMessage);

        $trainingModel->updateData($request);
        return redirect('client/training/list')->with('success', 'Training edited successfully');
    }

    public function clientDestroy(Request $request, $id)
    {
        $trainingModel = new Training();

        DB::beginTransaction();
        $updateData = [
            'is_deleted' => config('constants.is_deleted.Deleted'),
        ];

        if (isset($request->id)) {
            $trainingModel->where('id', '=', $id)->update($updateData);
            DB::commit();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            DB::rollback();
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function clientExportTraining()
    {
        $trainingModel = new Training();
        $departmentModel = new Department();
        $departmentList = $departmentModel->where('status', '=', config('constants.status.Active'))
            ->where('id', '=', Auth::user()->dept_id)
            ->pluck('name', 'id')->all();

        $trainingList = $trainingModel->select("users.first_name")->where('is_deleted', '!=', config('constants.is_deleted.Deleted'))
            ->where('trainings.dept_id','=',Auth::user()->dept_id)
            ->leftJoin('users', 'trainings.created_by', 'users.id')
            ->select('trainings.*', 'users.first_name')
//            ->tosql();
            ->get();

        $this->__exportTrainingList($trainingList, $departmentList);
    }


    /*  Common Function */
    private function __exportTrainingList($trainingList, $departmentList)
    {
        $tradeModel = new Trade();
        $tradeList = $tradeModel->where('status', '=', config('constants.status.Active'))
            ->pluck('trade_name', 'id')->all();


        $training_array[] = array('Sr. No.', 'প্রশিক্ষণের নাম', 'প্রশিক্ষণের স্থান', 'প্রশিক্ষণের সময়কাল (দিন)', 'ট্রেডের নাম', 'প্রশিক্ষণ প্রদানকারী সংস্থা', 'প্রতিষ্ঠানের কোড নং', 'প্রশিক্ষণ অনুমোদনকারী সংস্থা', 'প্রশিক্ষণ অর্থায়নকারী সংস্থা', 'অর্থায়ন মূল্য', 'ট্রেনিং এর শুরুর দিন', 'পরীক্ষার দিন', 'Status');

        foreach ($trainingList as $srNo => $training) {
//            dd($registration->name_bn);
            $training_array[] = array(
                'Sr. No.' => $srNo + 1,
                'প্রশিক্ষণের নাম' => $training->name,
                'প্রশিক্ষণের স্থান' => $training->training_place,
                'প্রশিক্ষণের সময়কাল (দিন)' => $training->total_day,
                'ট্রেডের নাম' => $tradeList[$training->trade_name],
                'প্রশিক্ষণ প্রদানকারী সংস্থা' => $departmentList[$training->dept_id],
                'প্রতিষ্ঠানের কোড নং' => $training->code,
                'প্রশিক্ষণ অনুমোদনকারী সংস্থা' => $training->proposal_org,
                'প্রশিক্ষণ অর্থায়নকারী সংস্থা' => $training->financial_org,
                'অর্থায়ন মূল্য' => $training->budget,
                'ট্রেনিং এর শুরুর দিন' => date('Y-m-d', strtotime($training->start_date)),
                'পরীক্ষার দিন' => date('Y-m-d', strtotime($training->exam_date)),
                'Status' => ($training->status == config('constants.training_status.Pending')) ? config('constants.training_status.1') : (($training->status == config('constants.training_status.Complete')) ? config('constants.training_status.2') : config('constants.training_status.0')),
            );
        }

        Excel::create("Training List", function ($excel) use ($training_array) {
            $excel->setTitle('Training List');
            $excel->sheet('Training List', function ($sheet) use ($training_array) {
                $sheet->fromArray($training_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }

}
