<?php

namespace App\Http\Controllers;

use App\Model\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartmentController extends Controller
{

    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $departmentModel = new Department();

        $departments = $departmentModel->where('departments.status', '!=', config('constants.status.Deleted'))
            ->leftJoin('users', 'departments.created_by', 'users.id')
            ->select('users.first_name', 'users.last_name', 'departments.*')
            ->paginate($displayValue);
        if (request()->ajax()) {
            return view('department.ajax_list', compact('departments'));
        } else {
            return view('department.adminList', compact('departments'));
        }
    }

    public function adminForm()
    {
        return view('department.adminForm');
    }

    public function adminStore(Request $request)
    {
        $departmentModel = new Department();

        $data = $this->validate($request, [
            'name' => 'required',
        ], [
            'name.required' => 'Department Name is required',

        ]);

        $departmentModel->saveData($request);
        return redirect('admin/department/list')->with('success', 'New Department added successfully');
    }

    public function show($id)
    {
        //
    }

    public function adminEdit($id)
    {
        $departmentData = Department::where('id', $id)->first();
        return view('department.adminEdit', compact('departmentData', 'id'));
    }

    public function adminUpdate(Request $request, $id)
    {
        $departmentModel = new Department();
        $data = $this->validate($request, [
            'name' => 'required',
        ], [
            'name.required' => 'Department Name is required',

        ]);

        $departmentModel->updateData($request);

        return redirect('admin/department/list')->with('success', 'Department edited successfully');
    }

    public function adminDestroy($id, Request $request)
    {
        $departmentModel = new Department();

        DB::beginTransaction();
        $updateData = [
            'status' => config('constants.status.Deleted'),
        ];

        $status = "success";
        $message = "Department Delete successfully.";

        if (!empty($id)) {
            try {
                $departmentModel->where('id', '=', $id)
                    ->update($updateData);
                DB::commit();
            } catch (\Exception $exPledger) {
                DB::rollback();
                $status = "error";
                $message = "Department Delete failed!!!";
            }
        } else {
            $status = "error";
            $message = "No data has been found to delete!!!";
        }
        return response()->json(['status' => $status, 'message' => $message]);
    }
}
