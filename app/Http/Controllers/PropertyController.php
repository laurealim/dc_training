<?php

namespace App\Http\Controllers;

use App\Model\Property;
use App\Model\Country;
use App\Model\District;
use App\Model\Division;
use App\Model\PropertyDetails;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Mockery\CountValidator\Exception;
use Illuminate\Support\Facades\DB;

class PropertyController extends Controller
{

    /********************************************************************/
    /************************* Admin Sections ***************************/
    /********************************************************************/

    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $users = new User();
        $clientList = $users->where('users.user_type', '=', config('constants.userType.Owner'))
            ->where('users.status', '=', config('constants.status.Active'))
            ->select('users.id', 'users.first_name', 'users.last_name')
            ->pluck('users.first_name', 'users.id')->all();

        $resultData = $this->_propertyList($displayValue, $searchData);
        $properties = $resultData;

//        $users = new User();
//        $clientList = $users->where('users.user_type', '=', config('constants.userType.Owner'))
//            ->where('users.status','=',config('constants.status.Active'))
//            ->select('users.id', 'users.first_name', 'users.last_name')
//            ->pluck('users.first_name','users.id')->all();
//
//        $properties = new Property();
//
//        $properties = $properties->where('properties.property_name', 'like', '%' . $searchData . '%')
//            ->leftJoin('users', 'properties.created_by', 'users.id')
//            ->leftJoin('property_details', 'properties.id', 'property_details.property_id')
//            ->select('properties.*', 'users.first_name', 'property_details.*')
//            ->paginate($displayValue);
//        //->toSql();

        if (request()->ajax()) {
            return view('property.ajax_list', compact('properties', 'clientList'));
        } else {
            return view('property.adminList', compact('properties', 'clientList'));
        }

    }

    public function adminForm()
    {
        $users = new User();
        $clientList = $users->where('users.user_type', '=', config('constants.userType.Owner'))
            ->where('users.status', '=', config('constants.status.Active'))
            ->select('users.id', 'users.first_name', 'users.last_name')
            ->get();
//        dd($clientList[2]->last_name);
        $countryData = Country::all();
        $divisionData = array();
        $districtData = array();
        return view('property.adminForm', compact('countryData', 'divisionData', 'districtData', 'clientList'));
    }

    public function adminStore(Request $request)
    {
        $resultData = $this->_propertySave($request);

        if ($resultData['msgType'] === 'success') {
            return redirect('admin/property/list')->with($resultData['msgType'], $resultData['msg']);
        } else {
            return redirect('admin/property/add')
                ->withErrors($resultData['validator'])
                ->withInput();
        }
    }

    public function show($id)
    {
        //
    }

    public function adminEdit($id)
    {
        $users = new User();
        $clientList = $users->where('users.user_type', '=', config('constants.userType.Owner'))
            ->where('users.status', '=', config('constants.status.Active'))
            ->select('users.id', 'users.first_name', 'users.last_name')
            ->get();

        $resData = $this->_propertyEdit($id);
//        dd($resData);
        $propertyData = $resData['propertyData'];
        $countryData = $resData['countryData'];
        $divisionData = $resData['divisionData'];
        $districtData = $resData['districtData'];

        return view('property.adminEdit', compact('propertyData', 'id', 'countryData', 'divisionData', 'districtData', 'clientList'));

    }

    public function adminUpdate(Request $request, $id)
    {
        $resultData = $this->_propertyUpdate($request,$id);
        if ($resultData['msgType'] === 'success') {
            return redirect('admin/property/list')->with($resultData['msgType'], $resultData['msg']);
        } else {
            return redirect('admin/property/'.$id)->withErrors($resultData['validator'])->withInput();
        }
//        return redirect('admin/property/list')->with('success', 'Property edited successfully');
    }

    public function adminDestroy($id, Request $request)
    {
        $propertyModel = Property::findOrFail($id);
        $propertyDtailModel = PropertyDetails::where('property_id', '=', $id)->firstOrFail();
        if (isset($id)) {
            $propertyDtailModel->delete();
            $propertyModel->delete();

            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }


    /********************************************************************/
    /************************* Client Sections **************************/
    /********************************************************************/

    public function clientList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $users = new User();
        $clientList = $users->where('users.user_type', '=', config('constants.userType.Owner'))
            ->where('users.status', '=', config('constants.status.Active'))
            ->select('users.id', 'users.first_name', 'users.last_name')
            ->pluck('users.first_name', 'users.id')->all();

        $resultData = $this->_propertyList($displayValue, $searchData);
        $properties = $resultData;

        if (request()->ajax()) {
            return view('property.client_ajax_list', compact('properties', 'clientList'));
        } else {
            return view('property.clientList', compact('properties', 'clientList'));
        }
    }

    public function clientForm()
    {
        $countryData = Country::all();
        $divisionData = array();
        $districtData = array();
        return view('property.clientForm', compact('countryData', 'divisionData', 'districtData'));
    }

    public function clientStore(Request $request)
    {
        $request->request->add(['owner' => Auth::user()->id]);

        $resultData = $this->_propertySave($request);

        if ($resultData['msgType'] === 'success') {
            return redirect('client/property/list')->with($resultData['msgType'], $resultData['msg']);
        } else {
            return redirect('client/property/add')
                ->withErrors($resultData['validator'])
                ->withInput();
//            return redirect('client/property/add')->with($resultData['msgType'], $resultData['msg']);
        }
    }

    public function clientEdit($id)
    {
        $resData = $this->_propertyEdit($id);
//        dd($resData);
        $propertyData = $resData['propertyData'];
        $countryData = $resData['countryData'];
        $divisionData = $resData['divisionData'];
        $districtData = $resData['districtData'];

        return view('property.clientEdit', compact('propertyData', 'countryData', 'divisionData', 'districtData', 'id'));
    }

    public function clientUpdate(Request $request, $id)
    {
//        $request->request->add(['owner' => Auth::user()->id]);

        $resultData = $this->_propertyUpdate($request,$id);
        if ($resultData['msgType'] === 'success') {
            return redirect('client/property/list')->with($resultData['msgType'], $resultData['msg']);
        } else {
            return redirect('client/property/'.$id)->withErrors($resultData['validator'])->withInput();
        }
//        return redirect('admin/property/list')->with('success', 'Property edited successfully');
    }

    public function clientDestroy($id, Request $request)
    {
        try {
            DB::beginTransaction();
            $propertyModel = Property::where('owner', '=', Auth::user()->id)
                ->findOrFail($id);

            $propertyDtailModel = PropertyDetails::where('property_id', '=', $id)
                ->firstOrFail();
            if (isset($id)) {
                $propertyDtailModel->delete();
                $propertyModel->delete();
                DB::commit();

                $request->session()->flash('success', 'Data Deleted Successfully..');
                return response()->json(['status' => 'success']);
            } else {
                $request->session()->flash('error', 'Data Can\'t Deleted Successfully..');
                return response()->json(['status' => 'error']);
            }
        }catch (\Exception $ex) {
            DB::rollback();
            $request->session()->flash('error', 'Data Can\'t Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }


    /********************************************************************/
    /************************* Common Sections **************************/
    /********************************************************************/

    private function _propertyList($displayValue, $searchData)
    {
        $properties = Property::query();
        $properties->where('properties.property_name', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'properties.created_by', 'users.id')
            ->leftJoin('property_details', 'properties.id', 'property_details.property_id')
            ->select('properties.*', 'users.first_name', 'property_details.*');

        $properties->when(Auth::user()->user_type != config('constants.userType.SuperUser'), function ($q) {
            return $q->where('properties.owner', '=', Auth::user()->id);
        });

//        $propertilist = $properties->toSql();
//        dd($propertilist);

        $properties = $properties->paginate($displayValue);

        return $properties;
    }

    private function _propertySave($request)
    {
        $propertyModel = new Property();
        $propertyDetailsModel = new PropertyDetails();
        $validator = "";
        try {
            DB::beginTransaction();

            $validationRules = [
                'property_name' => 'required',
                'country_id' => 'required',
                'division_id' => 'required',
                'district_id' => 'required',
                'address' => 'required',
                'builders' => 'required',
                'total_unit' => 'required|integer',
                'total_floor' => 'required|integer',
                'unit_floor' => 'required|integer',
                'photo' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
            ];

            $validationMessages = [
                'country_id.required' => 'Please Select a Country',
                'division_id.required' => 'Please Select a Division',
                'district_id.required' => 'Please Select a District',
                'property_name.required' => 'Property Name is required',
                'address.required' => 'Address is required',
                'builders.required' => 'Builders Name is required',
                'total_unit.required' => 'Total Unit Number is required',
                'total_floor.required' => 'Total Floor Number is required',
                'unit_floor.required' => 'Unit per Floor is required',
                'photo.required' => 'Photo is required',
            ];

            if (auth()->user()->type == config('constants.userType.SuperUser')) {
                array_push($validationRules, ['owner' => 'required']);
                array_push($validationMessages, ['owner.required' => 'Property Owner is required']);
            }

            $resValData = $this->validate($request, $validationRules, $validationMessages);

            $propertyModel->saveData($request);
            $lastInsertedId = $propertyModel->id;

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('photo')) {
                $property_id = $lastInsertedId;
                $path = "properties";

                // cache the file
                $file = $request->file('photo');

                // generate a new filename. getClientOriginalExtension() for the file extension
                $filename = $property_id . '.' . $file->getClientOriginalExtension();
                $filePath = $path . '/' . $property_id;

                if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                    Storage::delete($filePath . '/' . $filename);
                }

                // save to storage/app/public/properties as the new $filename
                $path = $file->storeAs($filePath, $filename);
                $request->request->add(['p_images' => $filename]);
            }

            /*  Save Properties Details Data    */
            $request->request->add(['property_id' => $lastInsertedId]);

            $propertyDetailsModel->saveData($request);

            DB::commit();
            $msgType = 'success';
            $msg = 'New Property added successfully';
        } catch (ValidationException $vex) {
            DB::rollback();
            $msgType = 'error';
            $msg = 'Validation Error';
            $validator = $vex->validator;
        } catch (\Exception $ex) {
            $message = array('message' => 'No File Found to Delete', 'status' => 'error');
            DB::rollback();
            $msgType = 'error';
            $msg = 'New Property can\'t added successfully';
        }
        return array('msg' => $msg, 'msgType' => $msgType, 'validator' => $validator);
    }

    private function _propertyEdit($id)
    {
        try {
            $properties = Property::query();

            $properties->where('properties.id', $id)
                ->leftJoin('users', 'properties.created_by', 'users.id')
                ->leftJoin('property_details', 'properties.id', 'property_details.property_id')
                ->select('properties.*', 'users.first_name', 'property_details.*');

            $properties->when(Auth::user()->user_type != config('constants.userType.SuperUser'), function ($q) {
                return $q->where('properties.owner', '=', Auth::user()->id);
            });

            $propertyData = $properties->first();

            $countryId = $propertyData->country_id;
            $divisionId = $propertyData->division_id;

            $countryData = Country::all();
            $divisionData = Division::where('country_id', $countryId)->get();
            $districtData = District::where('division_id', $divisionId)->get();
            return array('propertyData' => $propertyData, 'countryData' => $countryData, 'divisionData' => $divisionData, 'districtData' => $districtData);
        } catch (Exception $ex) {

        }

    }

    private function _propertyUpdate($request, $id)
    {
        $propertyModel = new Property();
        $propertyDetailsModel = new PropertyDetails();
        $validator = "";

        try {
            DB::beginTransaction();

            $validationRules = [
                'property_name' => 'required',
                'country_id' => 'required',
                'division_id' => 'required',
                'district_id' => 'required',
                'address' => 'required',
                'builders' => 'required',
                'total_unit' => 'required|integer',
                'total_floor' => 'required|integer',
                'unit_floor' => 'required|integer'
            ];

            $validationMessages = [
                'country_id.required' => 'Please Select a Country',
                'division_id.required' => 'Please Select a Division',
                'district_id.required' => 'Please Select a District',
                'property_name.required' => 'Property Name is required',
                'address.required' => 'Address is required',
                'builders.required' => 'Builders Name is required',
                'total_unit.required' => 'Total Unit Number is required',
                'total_floor.required' => 'Total Floor Number is required',
                'unit_floor.required' => 'Unit per Floor is required',
            ];

            if (auth()->user()->type == config('constants.userType.SuperUser')) {
                array_push($validationRules, ['owner' => 'required']);
                array_push($validationMessages, ['owner.required' => 'Property Owner is required']);
            }

            $resValData = $this->validate($request, $validationRules, $validationMessages);

            $propertyModel->updateData($request);


            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('photo')) {

                $path = "properties";

                // cache the file
                $file = $request->file('photo');

                // generate a new filename. getClientOriginalExtension() for the file extension
                $filename = $id . '.' . $file->getClientOriginalExtension();
                $filePath = $path . '/' . $id;

                if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                    Storage::delete($filePath . '/' . $filename);
                }

                // save to storage/app/public/properties as the new $filename
                $path = $file->storeAs($filePath, $filename);
                $request->request->add(['p_images' => $filename]);
            }

            /*  Save Properties Details Data    */
            $request->request->add(['property_id' => $id]);

            $propertyDetailsModel->updateData($request);

            DB::commit();
            $msgType = 'success';
            $msg = 'New Property added successfully';
        } catch (ValidationException $vex) {
            DB::rollback();
            $msgType = 'error';
            $msg = 'Validation Error';
            $validator = $vex->validator;
        } catch (\Exception $ex) {

            dd($ex);
            $message = array('message' => 'No File Found to Delete', 'status' => 'error');
            DB::rollback();
            $msgType = 'error';
            $msg = 'New Property can\'t added successfully';
        }
        return array('msg' => $msg, 'msgType' => $msgType, 'validator' => $validator);
    }
}
