<?php

namespace App\Http\Controllers;

use App\Model\Domain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DomainController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $domains = new Domain();

        $domains = $domains->where('domains.name', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'domains.created_by', 'users.id')
            ->select('domains.*', 'users.first_name')
            ->paginate($displayValue);


        if (request()->ajax()) {
            return view('domain.ajax_list',compact('domains'));
        } else {
            return view('domain.adminList',compact('domains'));
        }
    }

    public function adminForm()
    {
        return view('domain.adminForm');
    }

    public function adminStore(Request $request)
    {
        $domainModel = new Domain();
        $data = $this->validate($request,[
            'name' => 'required',
        ]);

        $domainModel->saveData($request);
        return redirect('admin/domain/list')->with('success','New Domain added successfully');
    }

    public function show(Domain $domain)
    {
    }

    public function adminEdit($id, Domain $domain)
    {
        $domainData = Domain::where('id', $id)->first();
        return view('domain.adminEdit', compact('domainData', 'id'));
    }

    public function adminUpdate(Request $request, Domain $domain, $id)
    {
        $domainModel = new Domain();
        $data = $this->validate($request, [
            'name' => 'required',
        ]);

        $domainModel->updateData($request);

        return redirect('admin/domain/list')->with('success','Domain edited successfully');
    }

    public function adminDestroy(Request $request, Domain $domain)
    {
        $domainInfo = Domain::findOrFail($request->id);
        if(isset($request->id)){
            $domainInfo->delete();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status'=>'success']);
        }
        else{
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status'=>'error']);
        }
    }
}
