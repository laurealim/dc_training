<?php

namespace App\Http\Controllers;

use App\Model\Department;
use App\Model\Property;
use App\Model\PropertyDetails;
use App\User;
use Auth;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class ClientController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $userModel = new User();
        $departmentModel = new Department();
        $departmentList = $departmentModel->pluck('name', 'id');
//        $departmentList = $departmentModel->select('name', 'id')
//            ->where('status', '=', config('constants.status.Active'))
//            ->get();

        $clientLList = $userModel->where('users.first_name', 'like', '%' . $searchData . '%')
            ->where('users.status', '!=', config('constants.status.Deleted'))
            ->where('users.id', '!=', Auth::user()->id)
//            ->orWhere('users.last_name', 'like', '%' . $searchData . '%')
//            ->orWhere('users.email', 'like', '%' . $searchData . '%')
//            ->orWhere('users.phone', 'like', '%' . $searchData . '%')
            ->paginate($displayValue);

//        $name = "asd";
//        $clientLList = $userModel->where('users.created_by', '!=', 0);
//
//        $clientLList->where('users.first_name', 'like', '%' . $searchData . '%')
//            ->orWhere('users.last_name', 'like', '%' . $searchData . '%')
//            ->orWhere('users.email', 'like', '%' . $searchData . '%')
//            ->orWhere('users.phone', 'like', '%' . $searchData . '%')
//            ->paginate($displayValue);

//        ->toSql();
//        dd($clientLList);

        if (request()->ajax()) {
            return view('client.ajax_list', compact('clientLList', 'departmentList'));
        } else {
            return view('client.adminList', compact('clientLList', 'departmentList'));
        }
    }

    public function adminForm()
    {
        $departmentModel = new Department();
        $departmentList = $departmentModel->pluck('name', 'id');
        return view('client.adminForm', compact('departmentList'));
    }

    public function adminStore(Request $request)
    {
        $data = $this->validate($request, [
            'first_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'user_type' => 'required',
            'dept_id' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
        ], [
            'first_name.required' => 'Name is required',
            'email.required' => 'Valid email required',
            'phone.required' => 'Phone required',
            'user_type.required' => 'Please Select a user type',
            'dept_id.required' => 'Please Select a department',

        ]);

        $formData = $request->all();
        $formData['created_by'] = Auth::user()->id;
        $formData['password'] = bcrypt('abc123');
        $formData['dob'] = date('Y-m-d', strtotime($formData['dob']));
        $formData['status'] = config('constants.status.Active');

        $resData = User::Create($formData);
        $lastInsertedId = $resData->id;
//        $lastInsertedId = 6;

        /*  File Manipulation   */
        $filename = '';
        if ($request->hasFile('image')) {
            $client_id = $lastInsertedId;
            $path = "clients";

            // cache the file
            $file = $request->file('image');

            // generate a new filename. getClientOriginalExtension() for the file extension
            $filename = $client_id . '.' . $file->getClientOriginalExtension();
            $filePath = $path . '/' . $client_id;

            if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                Storage::delete($filePath . '/' . $filename);
            }

            // save to storage_1/app/public/clients as the new $filename
            $path = $file->storeAs($filePath, $filename);

            User::where('id', $lastInsertedId)->update(['image' => $filename]);
        }

        return redirect('admin/client/list')->with('success', 'New Client added successfully');
    }

    public function show($id)
    {
        //
    }

    public function adminEdit($id)
    {
        $userModel = new User();
        $clientData = $userModel->where('id', $id)
            ->first();
        $departmentModel = new Department();
        $departmentList = $departmentModel->pluck('name', 'id');

        return view('client.adminEdit', compact('clientData', 'id', "departmentList"));
    }

    public function adminUpdate(Request $request, $id)
    {
        $userModel = new User();

        $data = $this->validate($request, [
            'first_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'user_type' => 'required',
            'dept_id' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
        ], [
            'first_name.required' => 'Name is required',
            'email.required' => 'Valid email required',
            'phone.required' => 'Phone required',
            'user_type.required' => 'Please Select a user type',
            'dept_id.required' => 'Please Select a department',

        ]);

        $formData = $request->all();
        $formData['dob'] = date('Y-m-d', strtotime($formData['dob']));
//        pr($formData);
//        die;
        $userModel->updateData($formData, $id);
        /*  File Manipulation   */
        $filename = '';
        if ($request->hasFile('image')) {

            $path = "clients";

            // cache the file
            $file = $request->file('image');

            // generate a new filename. getClientOriginalExtension() for the file extension
            $filename = $id . '.' . $file->getClientOriginalExtension();
            $filePath = $path . '/' . $id;

            if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                Storage::delete($filePath . '/' . $filename);
            }

            // save to storage_1/app/public/properties as the new $filename
            $path = $file->storeAs($filePath, $filename);
//            $request->request->add(['p_images' => $filename]);

            User::where('id', $id)->update(['image' => $filename]);
        }

        return redirect('admin/client/list')->with('success', 'Client edited successfully');
    }

    public function adminDestroy($id, Request $request)
    {
        $userData = User::findOrFail($id);

        $path = "clients";
        $filePath = $path . '/' . $id;
        $filename = $userData['image'];

        if (isset($id)) {
            DB::beginTransaction();
            try {
                $userData->delete();

                if (!empty($filename)) {
                    if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                        Storage::delete($filePath . '/' . $filename);
                    }
                }
                DB::commit();
                $request->session()->flash('success', 'Data Deleted Successfully..');
                return response()->json(['status' => 'success']);
            } catch (\Exception $ex) {
                DB::rollback();
                $request->session()->flash('errors', 'Data can not Deleted Successfully..');
                return response()->json(['status' => 'error']);
//                return response()->json(['error' => $ex->getMessage()], 500);
            }
        } else {
            $request->session()->flash('errors', 'Data can not Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function adminChangePasswordForm()
    {
        return view('client.adminChangePassword');
    }

    public function adminChangePassword(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $password_confirmation = $request->password_confirmation;
        try {

            $this->resetPassword($email, $password, $password_confirmation);

            return redirect('admin/client/change_password')->with('success', 'Password updated successfully..');
        } catch (\Exception $exception) {
            $request->session()->flash('error', $exception->getMessage());
            return redirect()->back();
        }
    }

    public function adminResetPassword($id, Request $request)
    {

        $userModel = new User();
        $password = 'abc123';
        if (isset($id)) {
            DB::beginTransaction();
            try {
                $user = $userModel->findOrFail($id);
                $user->password = Hash::make($password);
                $user->save();
                DB::commit();
                $request->session()->flash('success', 'Password Reset Successfully..');
                return response()->json(['status' => 'success']);
            } catch (\Exception $exception) {
                DB::rollback();
                $request->session()->flash('errors', 'Can not reset password');
                return response()->json(['status' => 'error']);
            }
        } else {
            $request->session()->flash('errors', 'No Data found');
            return response()->json(['status' => 'error']);
        }
    }


    /*  Department Head Client Section  */
    public function clientList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $userModel = new User();
        $departmentModel = new Department();
        $departmentList = $departmentModel->pluck('name', 'id');
//        $departmentList = $departmentModel->select('name', 'id')
//            ->where('status', '=', config('constants.status.Active'))
//            ->get();

//        dd(Auth::id());
        $clientLList = $userModel->where('users.dept_id', '=', Auth::user()->dept_id)            
			//->where('users.first_name', 'like', '%' . $searchData . '%')
            ->where('users.id', '!=', Auth::user()->id)
            ->where('users.user_type', '!=', config('constants.userType.SuperUser'))
            //->where('users.created_by', '=', Auth::user()->id)
            ->where('users.status', '!=', config('constants.status.Deleted'))
			//->toSql();
			//dd($clientLList);
            ->paginate($displayValue);

        if (request()->ajax()) {
            return view('user.client.ajax_list', compact('clientLList', 'departmentList'));
        } else {
            return view('user.client.clientList', compact('clientLList', 'departmentList'));
        }
    }

    public function clientForm()
    {
        $departmentModel = new Department();
        $departmentList = $departmentModel->pluck('name', 'id');
        return view('user.client.clientForm', compact('departmentList'));
    }

    public function clientStore(Request $request)
    {
        $data = $this->validate($request, [
            'first_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'user_type' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
        ], [
            'first_name.required' => 'Name is required',
            'email.required' => 'Valid email required',
            'phone.required' => 'Phone required',
            'user_type.required' => 'Please Select a user type',

        ]);

        $formData = $request->all();
        $formData['created_by'] = Auth::user()->id;
        $formData['password'] = bcrypt('abc123');
        $formData['dob'] = date('Y-m-d', strtotime($formData['dob']));
        $formData['status'] = config('constants.status.Active');
        $formData['dept_id'] = Auth::user()->dept_id;
        $formData['user_type'] = config('constants.userType.Tenant');

        $resData = User::Create($formData);
        $lastInsertedId = $resData->id;
//        $lastInsertedId = 6;

        /*  File Manipulation   */
        $filename = '';
        if ($request->hasFile('image')) {
            $client_id = $lastInsertedId;
            $path = "clients";

            // cache the file
            $file = $request->file('image');

            // generate a new filename. getClientOriginalExtension() for the file extension
            $filename = $client_id . '.' . $file->getClientOriginalExtension();
            $filePath = $path . '/' . $client_id;

            if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                Storage::delete($filePath . '/' . $filename);
            }

            // save to storage_1/app/public/clients as the new $filename
            $path = $file->storeAs($filePath, $filename);

            User::where('id', $lastInsertedId)->update(['image' => $filename]);
        }

        return redirect('client/client/list')->with('success', 'New Client added successfully');
    }

    public function clientshow($id)
    {
        //
    }

    public function clientEdit($id)
    {
        $userModel = new User();
        try {
            $clientData = $userModel->where('id', $id)
                ->where('dept_id', '=', Auth::user()->dept_id)
                ->firstOrFail();
            $departmentModel = new Department();
            $departmentList = $departmentModel->pluck('name', 'id');
        } catch (\Exception $ex) {
            return redirect('client/client/list')->with('error', 'No Data Found...');
        }
        return view('user.client.clientEdit', compact('clientData', 'id', "departmentList"));
    }

    public function clientUpdate(Request $request, $id)
    {
        $userModel = new User();
        $userArr = $userModel->findOrFail($id);
        $validateArr = [
            'first_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'user_type' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
        ];

        $validateMessage = [
            'first_name.required' => 'Name is required',
            'email.required' => 'Valid email required',
            'phone.required' => 'Phone required',
            'user_type.required' => 'Please Select a user type',
        ];

        if (Auth::user()->user_type == $userArr->user_type) {
            unset($validateArr['user_type']);
            unset($validateArr['user_type.required']);
        };

        $data = $this->validate($request, $validateArr, $validateMessage);

        $formData = $request->all();
        $formData['dob'] = date('Y-m-d', strtotime($formData['dob']));

        $userModel->updateData($formData, $id);


        /*  File Manipulation   */
        $filename = '';
        if ($request->hasFile('image')) {

            $path = "clients";

            // cache the file
            $file = $request->file('image');

            // generate a new filename. getClientOriginalExtension() for the file extension
            $filename = $id . '.' . $file->getClientOriginalExtension();
            $filePath = $path . '/' . $id;

            if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                Storage::delete($filePath . '/' . $filename);
            }

            // save to storage_1/app/public/properties as the new $filename
            $path = $file->storeAs($filePath, $filename);
//            $request->request->add(['p_images' => $filename]);

            User::where('id', $id)->update(['image' => $filename]);
        }

        return redirect('client/client/list')->with('success', 'Client edited successfully');
    }

    public function clientDestroy($id, Request $request)
    {
        $userData = User::findOrFail($id);

        $path = "clients";
        $filePath = $path . '/' . $id;
        $filename = $userData['image'];

        if (isset($id)) {
            DB::beginTransaction();
            try {
                $updateData = [
                    'status' => config('constants.status.Deleted'),
                ];
                $userData->where('id', '=', $id)->update($updateData);
//                $userData->delete();

//                if (!empty($filename)) {
//                    if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
////                Storage::deleteDirectory($filePath);
//                        Storage::delete($filePath . '/' . $filename);
//                    }
//                }
                DB::commit();
                $request->session()->flash('success', 'Data Deleted Successfully..');
                return response()->json(['status' => 'success']);
            } catch (\Exception $ex) {
                DB::rollback();
                $request->session()->flash('errors', 'Data can not Deleted Successfully..');
                return response()->json(['status' => 'error']);
//                return response()->json(['error' => $ex->getMessage()], 500);
            }
        } else {
            $request->session()->flash('errors', 'Data can not Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    public function clientChangePasswordForm()
    {
        return view('user.client.clientChangePassword');
    }

    public function clientChangePassword(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $password_confirmation = $request->password_confirmation;

//        dd($request->email);
        try {

            $this->resetPassword($email, $password, $password_confirmation);
            return redirect('client/client/change_password')->with('success', 'Password updated successfully..');
        } catch (\Exception $exception) {
            $request->session()->flash('error', $exception->getMessage());
            return redirect()->back();
        }
    }


    /*  User Section    */
    public function userEdit($id)
    {
        $userModel = new User();
        try {
            $clientData = $userModel->where('id', $id)
                ->where('dept_id', '=', Auth::user()->dept_id)
                ->firstOrFail();
            $departmentModel = new Department();
            $departmentList = $departmentModel->pluck('name', 'id');
        } catch (\Exception $ex) {
            return redirect('user/client/list')->with('error', 'No Data Found...');
        }
        return view('user.client.userEdit', compact('clientData', 'id', "departmentList"));
    }

    public function userUpdate(Request $request, $id)
    {
        $userModel = new User();
        $userArr = $userModel->findOrFail($id);
        $validateArr = [
            'first_name' => 'required',
            'email' => 'required',
            'phone' => 'required',
//            'image' => 'required|file|image|mimes:jpeg,jpg,png,gif,webp|max:6144'
        ];

        $validateMessage = [
            'first_name.required' => 'Name is required',
            'email.required' => 'Valid email required',
            'phone.required' => 'Phone required',
        ];

        $data = $this->validate($request, $validateArr, $validateMessage);

        $formData = $request->all();
        $formData['dob'] = date('Y-m-d', strtotime($formData['dob']));

        $userModel->updateData($formData, $id);


        /*  File Manipulation   */
        $filename = '';
        if ($request->hasFile('image')) {

            $path = "clients";

            // cache the file
            $file = $request->file('image');

            // generate a new filename. getClientOriginalExtension() for the file extension
            $filename = $id . '.' . $file->getClientOriginalExtension();
            $filePath = $path . '/' . $id;

            if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
//                Storage::deleteDirectory($filePath);
                Storage::delete($filePath . '/' . $filename);
            }

            // save to storage_1/app/public/properties as the new $filename
            $path = $file->storeAs($filePath, $filename);
//            $request->request->add(['p_images' => $filename]);

            User::where('id', $id)->update(['image' => $filename]);
        }

        return redirect('user/')->with('success', 'User profile updated successfully');
    }

    public function userChangePasswordForm()
    {
        return view('user.client.userChangePassword');
    }

    public function userChangePassword(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $password_confirmation = $request->password_confirmation;

//        dd($request->email);
        try {

            $this->resetPassword($email, $password, $password_confirmation);
            return redirect('user/client/change_password')->with('success', 'Password updated successfully..');
        } catch (\Exception $exception) {
            $request->session()->flash('error', $exception->getMessage());
            return redirect()->back();
        }
    }


    /*  Common Function */
    protected function resetPassword($email, $password, $password_confirmation)
    {
        $message = "";
        if ($password !== $password_confirmation) {
            $message = "Password Mismatch";
            throw new \Exception($message);
        }

        $userModel = new User();
        try {
            $user = $userModel->where('email', '=', $email)->findOrFail(Auth::user()->id);
            $user->password = Hash::make($password);
            $user->save();
            return true;
        } catch (\Exception $exception) {
            $message = "Email Not Found...";
            throw new \Exception($message);
        }
    }
}
