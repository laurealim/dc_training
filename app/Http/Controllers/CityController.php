<?php

namespace App\Http\Controllers;

use App\Model\City;
use App\Model\Country;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function adminList()
    {
//        dd($_GET);
//        $request->session()->flash('searchData', $request
//            ->has('searchData') ? $request->get('searchData') : ($request->session()
//            ->has('searchData') ? $request->session()->get('searchData') : ''));
//
//        $request->session()->flash('displayValue', $request
//            ->has('displayValue') ? $request->get('displayValue') : ($request->session()
//            ->has('displayValue') ? $request->session()->get('displayValue') : ''));

        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }


        $cities = new City();

        $cities = $cities->where('cities.name', 'like', '%' . $searchData . '%')
            ->orWhere('countries.name', 'like', '%' . $searchData . '%')
            ->leftJoin('countries', 'cities.country_id', 'countries.id')
            ->leftJoin('users', 'cities.created_by', 'users.id')
            ->select('cities.*', 'countries.id as countryID', 'countries.name as countryName', 'users.first_name')
            ->paginate($displayValue);


        if (request()->ajax()) {
            return view('city.ajax_list', compact('cities'));
        } else {
            return view('city.adminList', compact('cities'));
        }
    }

    function pr($data = array())
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

    public function adminForm()
    {
        $countryData = Country::all();
        return view('city.adminForm', compact('countryData'));
    }

    public function adminStore(Request $request)
    {
        $cityModel = new City();
        $data = $this->validate($request, [
            'name' => 'required',
            'country_id' => 'required',
        ]);

        $cityModel->saveData($request);
        return redirect('admin/city/list')->with('success', 'New City added successfully');
    }

    public function show(City $city)
    {
        //
    }

    public function adminEdit(City $city, $id)
    {
        $cityData = City::where('id', $id)->first();
        $countryData = Country::all();
        return view('city.adminEdit', compact('countryData', 'id', 'cityData'));
    }

    public function adminUpdate(Request $request, City $city, $id)
    {
        $cityModel = new City();
        $data = $this->validate($request, [
            'name' => 'required',
            'country_id' => 'required',
        ]);

        $cityModel->updateData($request);

        return redirect('admin/city/list')->with('success', 'City edited successfully');
    }

    public function adminDestroy(Request $request, City $city)
    {
        $cityInfo = Country::findOrFail($request->id);
        if (isset($request->id)) {
            $cityInfo->delete();
            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            $request->session()->flash('errors', 'Data Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }
}
