<?php

namespace App\Http\Controllers;

use App\Model\AdvanceDue;
use App\Model\Bill;
use App\Model\PendingLedger;
use App\User;
use Illuminate\Http\Request;
use App\Model\Apartment;
use App\Model\ApartmentRent;
use App\Model\Property;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;

class ApartmentController extends Controller
{
    /******************************************************************************/
    /************************    Admin Section   **********************************/
    /******************************************************************************/

    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $clients = new User();
        $users = $clients->select('first_name', 'last_name', 'id')
            ->where('status', '=', 1)
            ->where('user_type', '=', config('constants.userType.Tenant'))
            ->get();

        $apartments = $this->_apartmentLiat($displayValue, $searchData);

        if (request()->ajax()) {
            return view('apartment.ajax_list', compact('apartments', 'users'));
        } else {
            return view('apartment.adminList', compact('apartments', 'users'));
        }
    }

    public function adminForm()
    {
        $propertyObj = new Property();
        $propertyList = array();
        $propertyList = $propertyObj
            ->where('status', '=', config('constants.status.Active'))
            ->pluck('property_name', 'id')->all();

        return view('apartment.adminForm', compact('propertyList'));
    }

    public function adminStore(Request $request)
    {

        $resultData = $this->_apartmentSave($request);

        if ($resultData['msgType'] === 'success') {
            return redirect('admin/apt/list')->with($resultData['msgType'], $resultData['msg']);
        } else {
            return redirect('admin/apt/add')
                ->withErrors($resultData['validator'])
                ->withInput();
//            return redirect('client/property/add')->with($resultData['msgType'], $resultData['msg']);
        }

//        return redirect('admin/apt/list')->with('success', 'New Property added successfully');
    }

    public function show($id)
    {
        //
    }

    public function adminEdit($id, Request $request)
    {

        $propertyObj = new Property();
        $apartments = new Apartment();
        $apt_images = '';

        try {
            $propertyList = $propertyObj->where('status', '=', config('constants.status.Active'))
                ->pluck('property_name', 'id')->all();

            $apartmentData = $apartments->where('apartments.id', $id)
                ->leftJoin('users', 'apartments.created_by', 'users.id')
                ->leftJoin('apartment_rents', 'apartments.id', 'apartment_rents.apt_id')
                ->leftJoin('properties', 'properties.id', 'apartments.property_id')
                ->select('users.first_name', 'properties.property_name', 'apartment_rents.*', 'apartment_rents.id as apt_rent_id', 'apartment_rents.status as apt_rent_status', 'apartments.*')
                ->firstOrFail();
            return view('apartment.adminEdit', compact('apartmentData', 'id', 'propertyList', "propertyList"));
        } catch (\Exception $ex) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
//            return redirect('client/apt/list')->with('errors', 'No Data Found...');
//            return redirect()->back()->withErrors(['msg', 'The Message']);    When has multiple errors and go back to the previous url...
        }
    }

    public function adminUpdate(Request $request, $id)
    {
        $resultData = $this->_apartmentUpdate($request, $id);

        if ($resultData['msgType'] === 'success') {
            return redirect('admin/apt/list')->with($resultData['msgType'], $resultData['msg']);
        } else {
            return redirect('admin/apt/' . $id)
                ->withErrors($resultData['validator'])
                ->withInput();
//            return redirect('client/property/add')->with($resultData['msgType'], $resultData['msg']);
        }
    }

    public function adminDestroy($id, Request $request)
    {
        $apartmentModel = Apartment::findOrFail($id);
        $apartmentRentModel = ApartmentRent::where('apt_id', '=', $id)->firstOrFail();
        if (isset($id)) {
            $apartmentRentModel->delete();
            $apartmentModel->delete();

            $request->session()->flash('success', 'Data Deleted Successfully..');
            return response()->json(['status' => 'success']);
        } else {
            $request->session()->flash('errors', 'Data Cann\'t Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function adminAjaxDelete(Request $request)
    {
        $fileName = $request->fileName;
        $folderId = $request->folderId;
        $imgDir = 'storage/apartments/' . $folderId;
        $filePath = $imgDir . '/' . $fileName;
        $message = array();
        $existingFileName = array();

        if (!empty($fileName) && !empty($folderId)) {
            if (file_exists($imgDir)) {
                if (file_exists($filePath)) {
                    unlink($filePath);
                    $message = array('message' => 'Successfully Deleted.', 'status' => 'success');
                } else {
                    $message = array('message' => 'No File Found to Delete', 'status' => 'error');
                }
                $files = scandir($imgDir);

                unset($files[0]);
                unset($files[1]);

                if (sizeof($files > 0)) {
                    foreach ($files as $key => $fileName) {
                        $existingFileName[] = $fileName;
                    }
                }
            }
        }

        $apt_images = json_encode($existingFileName, true);
        Apartment::where('id', $folderId)->update(['apt_images' => $apt_images]);

        echo json_encode($message);
        die;
    }

    private function checkExistingImg($folderId, $fileNameArr)
    {
        $imgDir = 'storage/apartments/' . $folderId;
        $existingFileName = array();

        if (file_exists($imgDir)) {
            $files = scandir($imgDir);

            unset($files[0]);
            unset($files[1]);

            if (sizeof($files > 0)) {
                foreach ($files as $key => $fileName) {
                    if (!in_array($fileName, $fileNameArr)) {
                        $existingFileName[] = $fileName;
                    }
                }
            }
        }
        return array_merge($fileNameArr, $existingFileName);
    }

    public function adminAssignClient()
    {
        $apartmentModel = new Apartment();
        $aptId = $_POST['id'];
        $clientId = $_POST['clientId'];
        $message = array();

        if (isset($aptId) && isset($clientId)) {
            DB::beginTransaction();
            try {
                $apartmentModel->where('id', $aptId)->update(['is_assigned' => 1, 'client_id' => $clientId]);
                $message = array('message' => 'Successfully Deleted.', 'status' => 'success');
                DB::commit();
            } catch (\Exception $ex) {
                $message = array('message' => 'No File Found to Delete', 'status' => 'error');
                DB::rollback();
            }
        }
        echo json_encode($message);
        die;
    }

    public function adminConfirmAssignClient()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $apartments = new Apartment();
        $clients = new User();

        $approveClients = $apartments->where('apartments.apt_number', 'like', '%' . $searchData . '%')
            ->where([['apartments.status', '=', '1'], ['apartments.is_assigned', '>=', '1']])
            ->leftJoin('users', 'apartments.created_by', 'users.id')
            ->leftJoin('apartment_rents', 'apartments.id', 'apartment_rents.apt_id')
            ->leftJoin('properties', 'properties.id', 'apartments.property_id')
            ->select('users.first_name', 'users.last_name', 'properties.property_name', 'apartment_rents.rent_amount', 'apartment_rents.service_charge', 'apartment_rents.adv_month', 'apartment_rents.adv_rent', 'apartment_rents.id as apt_rent_id', 'apartment_rents.status as apt_rent_status', 'apartments.client_id', 'apartments.id', 'apartments.apt_number', 'apartments.owner', 'apartments.is_assigned')
            ->paginate($displayValue);
//            ->toSql();
//        dd($pendingLedgers);


        $users = $clients->select('first_name', 'last_name', 'id')
            ->where('status', '=', 1)
            ->where('user_type', '=', config('constants.userType.Tenant'))
            ->get();

        if (request()->ajax()) {
            return view('apartment.ajax_confirm_list', compact('approveClients', 'users'));
        } else {
            return view('apartment.adminConfirmList', compact('approveClients', 'users'));
        }
    }

    public function adminApproveClient($aptId, $clientId)
    {
        $apartmentModel = new Apartment();
        $clients = new User();
        $pendingLedgerModel = new PendingLedger();
        $billModel = new Bill();
        $advanceDueModel = new AdvanceDue();

        $ledgerFor = config('constants.ledgerFor.Billing');
        $ledgerType = config('constants.ledgerType.Initial');
        $isInitialData = true;
        $secureAmount = 0;
        $previousAmount = 0;
        $currentAmount = 0;
        $totalBill = 0;
        $lastPendingLedgerId = 0;

        $approveClients = $apartmentModel->where([['apartments.id', '=', $aptId], ['apartments.is_assigned', '>=', '1'], ['apartments.client_id', '>=', $clientId]])
            ->leftJoin('apartment_rents', 'apartments.id', 'apartment_rents.apt_id')
            ->select('apartment_rents.rent_amount', 'apartment_rents.service_charge', 'apartment_rents.adv_month', 'apartment_rents.adv_rent', 'apartment_rents.id as apt_rent_id', 'apartment_rents.status as apt_rent_status', 'apartments.client_id', 'apartments.id', 'apartments.owner', 'apartments.is_assigned')
            ->first();

//        $ledgerDataArr = is_initial_bill($aptId, $clientId, $ledgerFor);
//        if ($ledgerDataArr) {
//            $isInitialData = false;
//            $previousAmount = $ledgerDataArr->current_amount;
//        } else {
//        }
        $secureAmount = (($approveClients->adv_rent * $approveClients->adv_month) * (-1));

        DB::beginTransaction();
        $rent_amount = $approveClients->rent_amount;
        $service_charge = $approveClients->service_charge;
        $adv_month = $approveClients->adv_month;
        $adv_rent = $approveClients->adv_rent;
        $amount = (($rent_amount + $service_charge) * (-1));
        $totalBill = $amount + $secureAmount;
        $currentAmount = $previousAmount + $amount + $secureAmount;

        if ($approveClients) {
            /*  Save Data in Pending Ledger Table Start */
            $pendingLedgerModel->apt_id = $aptId;
            $pendingLedgerModel->client_id = $clientId;
            $pendingLedgerModel->amount = $amount;
            $pendingLedgerModel->secure_amount = $secureAmount;
            $pendingLedgerModel->previous_amount = $previousAmount;
            $pendingLedgerModel->current_amount = $currentAmount;
            $pendingLedgerModel->ledger_for = $ledgerFor;
            $pendingLedgerModel->ledger_type = $ledgerType;
            $pendingLedgerModel->created_by = auth()->user()->id;
            $pendingLedgerModel->status = config('constants.state.Pending');

            try {
                $pendingLedgerModel->save();
                $lastPendingLedgerId = $pendingLedgerModel->id;
            } catch (\Exception $exPledger) {
                dd($exPledger);
                DB::rollback();
                return redirect('admin/apt/confirm_assign_client')->with('error', 'Client Can not Assign Successfully...');
            }
            /*  Save Data in Pending Ledger Table End */

            /*  Save Data in Bill Table Start */

            $billModel->apt_id = $aptId;
            $billModel->client_id = $clientId;
            $billModel->pending_ledger_id = $lastPendingLedgerId;
            $billModel->rent_amount = $rent_amount;
            $billModel->service_charge = $service_charge;
            $billModel->security_bill = $adv_rent;
            $billModel->security_bill_month = $adv_month;
            $billModel->total_bill = $totalBill;
            $billModel->current_amount = $currentAmount;
            $billModel->bill_type = $ledgerType;
            $billModel->created_by = auth()->user()->id;
            $billModel->billing_month = date('n');
            $billModel->billing_year = date('Y');
            $billModel->status = config('constants.state.Pending');

            try {
                $billModel->save();
            } catch (\Exception $exBill) {
                dd($exBill);
                DB::rollback();
                return redirect('admin/apt/confirm_assign_client')->with('error', 'Client Can not Assign Successfully...');
            }
            /*  Save Data in Bill Table End */

            /*  Save Data in Advance-Due Table Starts */
            $advanceDueModel->apt_id = $aptId;
            $advanceDueModel->client_id = $clientId;
            $advanceDueModel->pending_ledger_id = $lastPendingLedgerId;
            $advanceDueModel->total_amount = $amount + $secureAmount;
            $advanceDueModel->previous_amount = $previousAmount;
            $advanceDueModel->current_amount = $currentAmount;
            $advanceDueModel->amount_for = $ledgerFor;
            $advanceDueModel->type = $ledgerType;
            $advanceDueModel->created_by = auth()->user()->id;
            $advanceDueModel->status = config('constants.state.Pending');

            try {
                $advanceDueModel->save();
            } catch (\Exception $exAdDu) {
                dd($exAdDu);
                DB::rollback();
                return redirect('admin/apt/confirm_assign_client')->with('error', 'Client Can not Assign Successfully...');
            }

            /*  Save Data in Advance-Due Table End */


            /*  Update Data in Apartment Table Start */

            try {
                $apartmentData = $apartmentModel->find($aptId);
                $apartmentData->is_assigned = config('constants.is_assigned.Confirmed');
                $apartmentData->save();
                DB::commit();
            } catch (\Exception $exApt) {
                dd($exApt);
                DB::rollback();
                return redirect('admin/apt/confirm_assign_client')->with('error', 'Client Can not Assign Successfully...');
            }
            /*  Update Data in Apartment Table End */
        }
        return redirect('admin/apt/confirm_assign_client')->with('success', 'Client Assigned Successfully...');
    }

    public function adminRejectClient($apt_id, $client_id, Request $request)
    {
        $apartmentModel = new Apartment();
        $aptDataArr = $apartmentModel->where('id', '=', $apt_id)
            ->where('client_id', '=', $client_id)
            ->first();

        if ($aptDataArr) {
            DB::beginTransaction();
            try {
                $aptDataArr->is_assigned = config('constants.is_assigned.Unassigned');
                $aptDataArr->client_id = 0;
                $aptDataArr->save();
                DB::commit();

                $request->session()->flash('success', 'ClientUnassigned Successfully..');
                return response()->json(['status' => 'success']);
            } catch (\Exception $ex) {
                DB::rollback();
                $request->session()->flash('errors', 'Client Cann\'t Unassigned Successfully..');
                return response()->json(['status' => 'error']);
            }
        } else {
            $request->session()->flash('errors', 'Client Cann\'t Unassigned Successfully..');
            return response()->json(['status' => 'error']);
        }
    }



    /******************************************************************************/
    /************************    Client Section   *********************************/
    /******************************************************************************/

    public function clientList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

//        $clients = new User();
//        $users = $clients->select('first_name', 'last_name', 'id')
//            ->where('status', '=', 1)
//            ->where('user_type', '=', config('constants.userType.Tenant'))
//            ->get();

        $users = new User();
        $clientList = $users->where('users.user_type', '=', config('constants.userType.Tenant'))
            ->where('users.status', '=', config('constants.status.Active'))
            ->select('users.id', 'users.first_name', 'users.last_name')
            ->pluck('users.first_name', 'users.id')->all();

        $apartments = $this->_apartmentLiat($displayValue, $searchData);

        if (request()->ajax()) {
            return view('apartment.ajax_list', compact('apartments', 'clientList'));
        } else {
            return view('apartment.clientList', compact('apartments', 'clientList'));
        }
    }

    public function clientForm()
    {
        $propertyObj = new Property();
        $propertyList = $propertyObj->where('status', '=', config('constants.status.Active'))
            ->where('owner', '=', Auth::user()->id)
            ->pluck('property_name', 'id')->all();

        return view('apartment.clientForm', compact('propertyList'));
    }

    public function clientStore(Request $request)
    {
        $resultData = $this->_apartmentSave($request);

        if ($resultData['msgType'] === 'success') {
            return redirect('client/apt/list')->with($resultData['msgType'], $resultData['msg']);
        } else {
            return redirect('client/apt/add')
                ->withErrors($resultData['validator'])
                ->withInput();
//            return redirect('client/property/add')->with($resultData['msgType'], $resultData['msg']);
        }

//        return redirect('admin/apt/list')->with('success', 'New Property added successfully');
    }

    public function clientEdit($id, Request $request)
    {
        try {
            $propertyObj = new Property();
            $apartments = new Apartment();
            $apt_images = '';

            $propertyList = $propertyObj->where('owner', '=', Auth::user()->id)
                ->where('status', '=', config('constants.status.Active'))
                ->pluck('property_name', 'id')->all();

            $apartmentData = $apartments->where('apartments.id', $id)
                ->where('apartments.owner', Auth::user()->id)
                ->leftJoin('users', 'apartments.created_by', 'users.id')
                ->leftJoin('apartment_rents', 'apartments.id', 'apartment_rents.apt_id')
                ->leftJoin('properties', 'properties.id', 'apartments.property_id')
                ->select('users.first_name', 'properties.property_name', 'apartment_rents.*', 'apartment_rents.id as apt_rent_id', 'apartment_rents.status as apt_rent_status', 'apartments.*')
                ->firstOrFail();
            return view('apartment.clientEdit', compact('apartmentData', 'id', 'propertyList'));
        } catch (\Exception $ex) {
            $request->session()->flash('error', 'No Data Found...');
            return redirect()->back();
//            return redirect('client/apt/list')->with('errors', 'No Data Found...');
//            return redirect()->back()->withErrors(['msg', 'The Message']);    When has multiple errors and go back to the previous url...
        }

    }

    public function clientUpdate(Request $request, $id)
    {
        $resultData = $this->_apartmentUpdate($request, $id);

        if ($resultData['msgType'] === 'success') {
            return redirect('client/apt/list')->with($resultData['msgType'], $resultData['msg']);
        } else {
            return redirect('client/apt/' . $id)
                ->withErrors($resultData['validator'])
                ->withInput();
//            return redirect('client/property/add')->with($resultData['msgType'], $resultData['msg']);
        }
//        return redirect('admin/apt/list')->with('success', 'Apartment Updated successfully');
    }

    public function clientDestroy($id, Request $request)
    {
        try {
            DB::beginTransaction();
            $apartmentModel = Apartment::where('owner', '=', Auth::user()->id)
                ->findOrFail($id);
            $apartmentRentModel = ApartmentRent::where('apt_id', '=', $id)
                ->firstOrFail();
            if (isset($id)) {
                $apartmentRentModel->delete();
                $apartmentModel->delete();
                DB::commit();

                $request->session()->flash('success', 'Data Deleted Successfully..');
                return response()->json(['status' => 'success']);
            } else {
                $request->session()->flash('errors', 'Data Can\'t Deleted Successfully..');
                return response()->json(['status' => 'error']);
            }
        } catch (\Exception $ex) {
            DB::rollback();
            $request->session()->flash('error', 'Data Can\'t Deleted Successfully..');
            return response()->json(['status' => 'error']);
        }

    }



    /******************************************************************************/
    /************************    Common Section   *********************************/
    /******************************************************************************/

    private function _apartmentLiat($displayValue, $searchData)
    {
        $apartments = Apartment::query();
        $apartments->where('apartments.apt_number', 'like', '%' . $searchData . '%')
            ->leftJoin('users', 'apartments.owner', 'users.id')
            ->leftJoin('apartment_rents', 'apartments.id', 'apartment_rents.apt_id')
            ->leftJoin('properties', 'properties.id', 'apartments.property_id')
            ->select('users.first_name', 'properties.property_name', 'apartment_rents.*', 'apartment_rents.id as apt_rent_id', 'apartment_rents.status as apt_rent_status', 'apartments.*');

        $apartments->when(Auth::user()->user_type != config('constants.userType.SuperUser'), function ($q) {
            return $q->where('apartments.owner', '=', Auth::user()->id);
        });

        $apartments = $apartments->paginate($displayValue);
        return $apartments;
    }

    private function _apartmentSave($request)
    {
        $apartmentModel = new Apartment();
        $apartmentRentModel = new ApartmentRent();
        $propertyModel = new Property();
        $validator = "";

        try {
            DB::beginTransaction();
            $validationRules = [
                'property_id' => 'required',
                'apt_number' => 'required',
                'apt_size' => 'required|integer',
                'apt_level' => 'required|integer',
                'bed_room' => 'required|integer',
                'toilet' => 'required|integer',
                'veranda' => 'required|integer',
                'rent_amount' => 'required|integer',
                'service_charge' => 'required|integer',
                'adv_month' => 'required|integer',
                'adv_rent' => 'required|integer',
                'images' => 'required',
                'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:6144'
            ];

            $validationMessages = [
                'property_id.required' => 'Please Select a Property.',
                'apt_number.required' => 'Please Set an Apartment Number.',
                'apt_size.required' => 'Please Set Apartment Size.',
                'apt_level.required' => 'Please Set Apartment Level.',
                'bed_room.required' => 'Please Set Total Bedroom numbers.',
                'toilet.required' => 'Please Set Total Toilet numbers.',
                'veranda.required' => 'Please Set Total Veranda numbers.',
                'rent_amount.required' => 'Rent Amount is Required',
                'service_charge.required' => 'Service Charge is required',
                'adv_month.required' => 'Advance Month is required',
                'adv_rent.required' => 'Advance Rent is required',
                'images.required' => 'Photo is required',
            ];

            $resValData = $this->validate($request, $validationRules, $validationMessages);

            $formData = $request->all();

            $formData['created_by'] = Auth::user()->id;
            $formData['owner'] = $propertyModel->getOwnerIdByPropertyId($formData['property_id']);

            $resData = Apartment::Create($formData);
            $lastInsertedId = $resData->id;
//        $lastInsertedId = 1; //$apartmentModel->id;


            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('images')) {
                $apartment_id = $lastInsertedId;
                $path = "apartments";
                $fileNameArr = array();

                foreach ($request->file('images') as $key => $file) {
                    // generate a new filename. getClientOriginalExtension() for the file extension
                    $randName = $this->generateRandomString();
                    $filename = $apartment_id . '_' . $randName . '.' . $file->getClientOriginalExtension();
                    $filePath = $path . '/' . $apartment_id;

//                    if (Storage::disk('public')->exists($filePath . '/' . $filename)) {
////                Storage::deleteDirectory($filePath);
//                        Storage::delete($filePath . '/' . $filename);
//                    }

//                     save to storage/app/public/apartments as the new $filename
                    $imgPath = $file->storeAs($filePath, $filename);
                    $fileNameArr[] = $filename;
                }

                $apt_images = json_encode($fileNameArr, true);
                $formData['apt_images'] = $apt_images;
                Apartment::where('id', $lastInsertedId)->update(['apt_images' => $apt_images]);
            }

            /*  Save Apartment Rent Data    */
            $formData['apt_id'] = $lastInsertedId;
            $formData['updated_by'] = Auth::user()->id;

            ApartmentRent::Create($formData);

            DB::commit();
            $msgType = 'success';
            $msg = 'New Apartment added successfully';

        } catch (ValidationException $vex) {
            DB::rollback();
            $msgType = 'error';
            $msg = 'Validation Error';
            $validator = $vex->validator;
        } catch (\Exception $ex) {
            dd($ex);
            $message = array('message' => 'No File Found to Delete', 'status' => 'error');
            DB::rollback();
            $msgType = 'error';
            $msg = 'New Apartment can\'t added successfully';
        }
        return array('msg' => $msg, 'msgType' => $msgType, 'validator' => $validator);
    }

    private function _apartmentUpdate($request, $id)
    {
        $apartmentModel = new Apartment();
        $apartmentRentModel = new ApartmentRent();
        $validator = "";

        try {
            DB::beginTransaction();
            $formData = $request->all();

            $validationRules = [
                'property_id' => 'required',
                'apt_number' => 'required',
                'apt_size' => 'required|integer',
                'apt_level' => 'required|integer',
                'bed_room' => 'required|integer',
                'toilet' => 'required|integer',
                'veranda' => 'required|integer',
                'rent_amount' => 'required|integer',
                'service_charge' => 'required|integer',
                'adv_month' => 'required|integer',
                'adv_rent' => 'required|integer',
//                'images' => 'required',
//                'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:6144'
            ];

            $validationMessages = [
                'property_id.required' => 'Please Select a Property.',
                'apt_number.required' => 'Please Set an Apartment Number.',
                'apt_size.required' => 'Please Set Apartment Size.',
                'apt_level.required' => 'Please Set Apartment Level.',
                'bed_room.required' => 'Please Set Total Bedroom numbers.',
                'toilet.required' => 'Please Set Total Toilet numbers.',
                'veranda.required' => 'Please Set Total Veranda numbers.',
                'rent_amount.required' => 'Rent Amount is Required',
                'service_charge.required' => 'Service Charge is required',
                'adv_month.required' => 'Advance Month is required',
                'adv_rent.required' => 'Advance Rent is required',
//                'images.required' => 'Image is required',
            ];

            $resValData = $this->validate($request, $validationRules, $validationMessages);


            $apartmentModel->updateData($formData, $id);

            /*  File Manipulation   */
            $filename = '';
            if ($request->hasFile('images')) {
                $apartment_id = $id;
                $path = "apartments";
                $fileNameArr = array();

                foreach ($request->file('images') as $key => $file) {
                    // generate a new filename. getClientOriginalExtension() for the file extension
                    $randName = $this->generateRandomString();
                    $filename = $apartment_id . '_' . $randName . '.' . $file->getClientOriginalExtension();
                    $filePath = $path . '/' . $apartment_id;

                    $imgPath = $file->storeAs($filePath, $filename);
                    $fileNameArr[] = $filename;
                }

                $fileNameArr = $this->checkExistingImg($id, $fileNameArr);
                $apt_images = json_encode($fileNameArr, true);
                Apartment::where('id', $id)->update(['apt_images' => $apt_images]);
            }

            /*  Save Apartment Rent Details Data    */
            //ApartmentRent::where('apt_id', $id)->update($formData);
            $apartmentRentModel->updateData($formData, $id);

            DB::commit();
            $msgType = 'success';
            $msg = 'Apartment edited successfully';

        } catch (ValidationException $vex) {
            DB::rollback();
            $msgType = 'error';
            $msg = 'Validation Error';
            $validator = $vex->validator;
        } catch (\Exception $ex) {
            $message = array('message' => 'No File Found to Delete', 'status' => 'error');
            DB::rollback();
            $msgType = 'error';
            $msg = 'Apartment can\'t edited successfully';
        }
        return array('msg' => $msg, 'msgType' => $msgType, 'validator' => $validator);
    }
}
