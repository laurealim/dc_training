<?php

namespace App\Http\Controllers;

use App\Model\AdvanceDue;
use App\Model\Bill;
use App\Model\Ledger;
use App\Model\Payment;
use App\Model\PendingLedger;
use App\Model\Apartment;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LedgerController extends Controller
{
    public function adminList()
    {
        if (isset($_GET['displayValue'])) {
            $displayValue = $_GET['displayValue'] != '' ? $_GET['displayValue'] : "";
        } else {
            $displayValue = '';
        }

        if (isset($_GET['searchData'])) {
            $searchData = $_GET['searchData'] != '' ? $_GET['searchData'] : "";
        } else {
            $searchData = '';
        }

        $pendingLedgerModel = new PendingLedger();
        $apartments = new Apartment();
        $clients = new User();

        $pendingLedgerLists = $pendingLedgerModel->where([['pending_ledgers.status', '!=', config('constants.state.Rejected')]])//, ['pending_ledgers.approved_by', '=', '0']
            ->leftJoin('apartments', 'pending_ledgers.apt_id', 'apartments.id')
            ->leftJoin('users', 'pending_ledgers.client_id', 'users.id')
            ->select('pending_ledgers.*', 'apartments.apt_number', 'apartments.owner', 'users.first_name')
            ->paginate($displayValue);

        $users = $clients->select('first_name', 'last_name', 'id')
            ->where('status', '=', 1)
            ->where('user_type', '=', config('constants.userType.Tenant'))
            ->get();

        if (request()->ajax()) {
            return view('ledger.ajax_list', compact('pendingLedgerLists', 'users'));
        } else {
            return view('ledger.adminList', compact('pendingLedgerLists', 'users'));
        }
    }

    public function adminForm()
    {
        $apartmentModel = new Apartment();

//        $apartmentList = $apartmentModel->where('apartments.is_assigned','=',2)
//            ->where('apartments.client_id','!=',0)
//            ->leftJoin('apartment_rents', 'apartment_rents.apt_id', 'apartments.id')
//            ->leftJoin('properties', 'properties.id', 'apartments.property_id')
//            ->leftJoin('users', 'apartments.client_id', 'users.id')
//            ->select('apartment_rents.*','properties.property_name', 'apartments.apt_number', 'apartments.owner', 'users.first_name', 'users.last_name')
//            ->get();
        $apartmentList = $apartmentModel->where('apartments.is_assigned','=',2)
            ->where('apartments.client_id','!=',0)
            ->leftJoin('properties', 'properties.id', 'apartments.property_id')
            ->leftJoin('users', 'apartments.client_id', 'users.id')
            ->select('properties.property_name', 'apartments.apt_number', 'apartments.owner', 'users.first_name', 'users.last_name')
            ->get();
//        dd($apartmentList);

//        $propertyObj = new Property();
        $propertyList = array();

//        $propertyListObj = $propertyObj->select('id', 'property_name')->get();


//        foreach ($propertyListObj as $key => $value) {
//            $propertyList[$value->id] = $value->property_name;
//        }

        return view('ledger.adminForm', compact('apartmentList'));
    }

    public function adminApproveLedger($id, $state, Request $request)
    {

        $resultData = pendingLedgerUpdate($id, $state);
        if ($resultData['status'] == 1) {
            $request->session()->flash('success', $resultData['message']);
            return response()->json(['status' => 'success']);
        } elseif ($resultData['status'] == 2) {
            $request->session()->flash('errors', $resultData['message']);
            return response()->json(['status' => 'error']);
        }

    }

    public function adminRejectLedger($id, $state, Request $request)
    {
        $resultData = pendingLedgerUpdate($id, $state);
        if ($resultData['status'] == 1) {
            $request->session()->flash('success', $resultData['message']);
            return response()->json(['status' => 'success']);
        } elseif ($resultData['status'] == 2) {
            $request->session()->flash('errors', $resultData['message']);
            return response()->json(['status' => 'error']);
        }
    }
}
