<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CompetitionController extends Controller
{
    public function adminList()
    {
        $cities = "laureal";
        if (request()->ajax()) {
            return view('competition.ajax_list', compact('cities'));
        } else {
            return view('competition.adminList', compact('cities'));
        }
    }

    public function adminForm()
    {
        return view('competition.adminForm');
    }

    public function adminStore(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function adminEdit($id)
    {
        //
    }

    public function adminUpdate(Request $request, $id)
    {
        //
    }

    public function adminDestroy($id)
    {
        //
    }
}
