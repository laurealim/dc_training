@extends('layouts.user')

@section('title')
    Property List
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('property.clientList') }}">Property List</a>
        </li>

        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Property List</h1>
@stop

@section('content')
    <div class="col-xs-12">
        {{--<h3 class="header smaller lighter blue">jQuery dataTables</h3>--}}

        <div class="clearfix">
            <div class="pull-right tableTools-container"></div>
            <div style="padding-bottom: 10px;">
                <a href="{{ route('property.clientForm') }}">
                    <button class="btn btn-primary">Add New Property</button>
                </a>
            </div>
        </div>
        <div>
            <div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="dataTables_length" id="dynamic-table_length">
                            <label>Display
                                <select id="displayValue" name="dynamic-table_length" aria-controls="dynamic-table"
                                        class="form-control input-sm">
                                    <option value="15">15</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                records</label>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div id="dynamic-table_filter" class="dataTables_filter">
                            <label>Search:
                                <input type="text" class="form-control input-sm" placeholder=""
                                       aria-controls="dynamic-table" name="searchData" id="searchData"
                                       value="">
                            </label>
                        </div>
                    </div>
                </div>
                <div class="posts">
                    @include('property.client_ajax_list')
                </div>
            </div>
        </div>
    </div>
@stop

@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        $(window).on('hashchange', function () {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getPosts(page);
                }
            }
        });

        $(document).ready(function () {
            $(document).on('click', '.pagination a', function (e) {
                getPosts($(this).attr('href').split('page=')[1], $(this).attr('href'));
                e.preventDefault();
            });
        });

        function getPosts(page, url) {
            console.log(url);
//            url: '?page=' + page
            var displayValue = $("#displayValue").val();
            var searchData = $("#searchData").val();
            $.ajax({
                url: '?page=' + page + '&displayValue=' + displayValue + '&searchData=' + searchData,
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    alert('Posts could not be loaded.');
                }
            });
        }
        ;


        //        $(".deletebtn").click(function (ev) {
        $(document).on("click", "a.deletebtn", function (ev) {
            ev.preventDefault();
            let url = $(this).attr("href");
            let id = $(this).attr("id");
            if (confirm("Do you wat to delete?")) {
                $.ajax({
                    type: 'DELETE',
                    url: url,
                    dataType: 'json',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {id: id, "_token": "{{ csrf_token() }}"},

                    success: function (data) {
                        if (data.status == 'success') {
                            window.location.reload();
                        }
                        else if (data.status == 'error') {
                            window.location.reload();
                        }
//                    data.request->session()->flash('status', 'Task was successful!');
//                    setInterval(function() {
//                    }, 5900);
                    },
                    error: function (data) {
                    }
                });
            } else {
                return false;
            }
        });


        $("#displayValue").on('change', function () {
            var displayValue = $(this).val();
            var url = window.location.href;
            dynamicDataViewCount(displayValue, '', url);
        });

        $("#searchData").keyup(function (e) {
            var searchData = $(this).val();
            var displayValue = $("#displayValue").val();
            /*
             48-57 - (0-9)Numbers
             65-90 - (A-Z)
             97-122 - (a-z)
             8 - (backspace)
             32 - (space)
             https://www.w3schools.com/charsets/ref_html_ascii.asp
             */

            var keyCode = e.which;
            if (keyCode == 46 || keyCode == 8 || (keyCode < 48 || keyCode > 57) || (keyCode < 65 || keyCode > 90) || (keyCode < 97 || keyCode > 122)) {
                var url = window.location.href;
                dynamicDataViewCount(displayValue, searchData, url);
            }
        });

        function dynamicDataViewCount(displayValue, searchData, url) {
            if (displayValue == '') {
                displayValue = $("#displayValue").val();
            }
            if (searchData == '') {
                searchData = $("#searchData").val();
            }
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {displayValue: displayValue, searchData: searchData},
                success: function (data) {
                    $('.posts').html(data);
                },
                error: function (err) {
                    console.log(err);
                    alert('Posts could not be loaded.');
                }
            });
        }
    </script>
@stop