<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="4%">
            {{--<label class="pos-rel">--}}
                {{--<input type="checkbox" class="ace"/>--}}
                {{--<span class="lbl"></span>--}}
            {{--</label>--}}
        </th>
        <th width="12%">Property Name</th>
        <th width="12%">Property Address</th>
        <th width="12%">Builders Name</th>
        <th width="18%">Facilities</th>
        <th width="15%">Image</th>
        <th width="6%">Owner</th>
        <th width="6%">Created By</th>
        <th width="5%">Status</th>
        <th class="center" width="10%">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php $pageNo = (($properties->currentPage() - 1) * $properties->perPage()) + 1; ?>
    @foreach ($properties as $property)
        <?php //pr($property);?>
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $property->property_name }}
            </td>
            <td>
                {{ $property->address }}
            </td>
            <td>
                {{ $property->builders }}
            </td>
            <td>
                <span class="facility">Gas : &nbsp;&nbsp;&nbsp;</span>{{ ($property->has_gas == 1) ? "Available" : "Not Available" }}
                <br/>
                <span class="facility">Generator : &nbsp;&nbsp;&nbsp; </span>{{ ($property->has_generator == 1) ? "Available" : "Not Available" }}
                <br/>
                <span class="facility">Lift : &nbsp;&nbsp;&nbsp; </span>{{ ($property->has_lift == 1) ? "Available" : "Not Available" }}
            </td>
            <td class="center">
                <img src="{{ asset('storage/properties/'.$property->id.'/'.$property->p_images) }}" class="msg-photo" alt="Kate's Avatar" width="120px" height="80px"/>
{{--                <img src="{{ asset('images/avatars/avatar2.png') }}" class="msg-photo" alt="Kate's Avatar"/>--}}
            </td>
            <td> {{ $clientList[$property->owner]}} </td>
            <td> {{ $property->first_name}} </td>
            <td>
                <?php if($property->status == 1){ ?>
                <span class='label label-success'>Active</span>
                <?php } else if($property->status == 2){ ?>
                <span class='label label-warning'>Inactive</span>
                <?php } else{?>
                <span class='label label-danger'>Deleted</span>
                <?php }?>
            </td>
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    <a class="green" href="{{ route('property.adminEdit',$property->id) }}">
                        <i class="ace-icon fa fa-pencil bigger-200"></i>
                    </a>

                    &nbsp;&nbsp;| &nbsp;&nbsp;

                    <a class="red deletebtn" href="{{ route('property.adminDelete',$property->id) }}"
                       id={{ $property->id }}>
                        <i class="ace-icon fa fa-trash-o bigger-200"></i>
                    </a>

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    {{--<a class="red deletebtn" href="{{ route('country.adminDelete',$country->id) }}" id={{ $country->id }}>--}}
                    {{--<i class="ace-icon fa fa-trash-o bigger-200"></i>--}}
                    {{--</a>--}}
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($properties->currentPage() - 1) * $properties->perPage()) + 1 }}
            to {{ (($properties->currentPage() - 1) * $properties->perPage()) + $properties->perPage() }} of
            {{ $properties->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $properties->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>

<style type="text/css">
    .facility {
        font-weight: bold;
    }
</style>