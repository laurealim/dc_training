@extends('layouts.admin')

@section('title')
    Admin Dashboard
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="{{ route('admin.dashboard') }}">Dashboard</a>
        </li>
    </ul>
@stop

@section('page_header')
    <h1>Admin Dashboard</h1>
@stop

@section('content')
    {{--    <div class="row">--}}
    <div class="col-xs-12">
        <div class="col-sm-6">
            <div class="widget-box">
                <div class="widget-header widget-header-flat widget-header-small">
                    <h5 class="widget-title">
                        <i class="ace-icon fa fa-signal"></i>
                        Department wise Trainings
                    </h5>
                </div>

                <div class="widget-body">
                    <div class="widget-main">
                        <div id="piechart-placeholder"></div>

                        <div class="hr hr8 hr-double"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="table-header">
                Department wise Training Status
            </div>
            <div class="col-sm-12" id="tran_sts">
                <div class="col-sm-4">&nbsp;</div>
                <div class="col-sm-4 pull-rights center spinner-preview" id="spinner-preview_sts"> Loading</div>
                <div class="col-sm-4">&nbsp;</div>
            </div>
            <div class="tran_sts_tbl"></div>
        </div>
        <div class="clearfix"></div>
        <div class="hr hr8 hr-double"></div>
        <div class="col-sm-6">
            <div class="table-header">
                Training Registration and Participant List
            </div>
            <div class="col-sm-12" id="tran_reg">
                <div class="col-sm-4">&nbsp;</div>
                <div class="col-sm-4 pull-rights center spinner-preview" id="spinner-preview_reg"> Loading</div>
                <div class="col-sm-4">&nbsp;</div>
            </div>
            <div class="tran_reg_tbl"></div>
        </div>
        <div class="col-sm-6">
            <div class="table-header">
                Last Upcoming 5 Trainings
            </div>
            <div class="col-sm-12" id="tran_last">
                <div class="col-sm-4">&nbsp;</div>
                <div class="col-sm-4 pull-rights center spinner-preview" id="spinner-preview_last"> Loading</div>
                <div class="col-sm-4">&nbsp;</div>
            </div>
            <div class="tran_latest_tbl"></div>
        </div>
    </div>
    {{--    </div>--}}
@stop

@section('custom_style')
    <style type="text/css">
        /* some elements used in demo only */
        .spinner-preview {
            /*width: 100px;*/
            height: 100px;
            text-align: center;
        !important;
        }
    </style>
@stop

@section('custom_script')
    <script type="text/javascript">

        $.fn.spin = function (opts) {
            this.each(function () {
                var $this = $(this),
                    data = $this.data();

                if (data.spinner) {
                    data.spinner.stop();
                    delete data.spinner;
                }
                if (opts !== false) {
                    data.spinner = new Spinner($.extend({color: $this.css('color')}, opts)).spin(this);
                }
            });
            return this;
        };

        var opts = {
            corners: 1, left: "auto", length: 5, lines: 11, radius: 15, rotate: 11, speed: 1.2, trail: 43, width: 5
        }
        $('#spinner-preview_sts').spin(opts);
        $('#spinner-preview_reg').spin(opts);
        $('#spinner-preview_last').spin(opts);
        jQuery(function ($) {
            /*  ========================================================================= */
            /*  ============================ Inner Chart Start ========================== */
            /*  ========================================================================= */

            $('.easy-pie-chart.percentage').each(function () {
                var $box = $(this).closest('.infobox');
                var barColor = $(this).data('color') || (!$box.hasClass('infobox-dark') ? $box.css('color') : 'rgba(255,255,255,0.95)');
                var trackColor = barColor == 'rgba(255,255,255,0.95)' ? 'rgba(255,255,255,0.25)' : '#E2E2E2';
                var size = parseInt($(this).data('size')) || 50;
                $(this).easyPieChart({
                    barColor: barColor,
                    trackColor: trackColor,
                    scaleColor: false,
                    lineCap: 'butt',
                    lineWidth: parseInt(size / 10),
                    animate: ace.vars['old_ie'] ? false : 1000,
                    size: size
                });
            })

            /*  ========================================================================= */
            /*  ============================ Inner Chart Start ========================== */
            /*  ========================================================================= */

            /*  ========================================================================= */
            /*  ============================ BAR Chart Start ============================ */
            /*  ========================================================================= */

            /*  <div class="infobox-chart"><span class="sparkline" data-values="196,128,202,177,154,94,100,170,224"></span></div>  */

            $('.sparkline').each(function () {
                var $box = $(this).closest('.infobox');
                var barColor = !$box.hasClass('infobox-dark') ? $box.css('color') : '#FFF';
                $(this).sparkline('html',
                    {
                        tagValuesAttribute: 'data-values',
                        type: 'bar',
                        barColor: barColor,
                        chartRangeMin: $(this).data('min') || 0
                    });
            });

            /*  ========================================================================= */
            /*  ============================ BAR Chart Start ============================ */
            /*  ========================================================================= */


            /*  ========================================================================= */
            /*  ============================ PIE Chart Start ============================ */
            /*  ========================================================================= */
            //flot chart resize plugin, somehow manipulates default browser resize event to optimize it!
            //but sometimes it brings up errors with normal resize event handlers
            $.resize.throttleWindow = false;

            /**
             we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
             so that's not needed actually.
             */

            /*  ========================================================================= */
            /*  ============================ PIE Chart Ends ============================= */
            /*  ========================================================================= */

            /////////////////////////////////////
            $(document).one('ajaxloadstart.page', function (e) {
                $tooltip.remove();
            });


            var d1 = [];
            for (var i = 0; i < Math.PI * 2; i += 0.5) {
                d1.push([i, Math.sin(i)]);
            }

            var d2 = [];
            for (var i = 0; i < Math.PI * 2; i += 0.5) {
                d2.push([i, Math.cos(i)]);
            }

            var d3 = [];
            for (var i = 0; i < Math.PI * 2; i += 0.2) {
                d3.push([i, Math.tan(i)]);
            }


            var sales_charts = $('#sales-charts').css({'width': '100%', 'height': '220px'});
            $.plot("#sales-charts", [
                {label: "Domains", data: d1},
                {label: "Hosting", data: d2},
                {label: "Services", data: d3}
            ], {
                hoverable: true,
                shadowSize: 0,
                series: {
                    lines: {show: true},
                    points: {show: true}
                },
                xaxis: {
                    tickLength: 0
                },
                yaxis: {
                    ticks: 10,
                    min: -2,
                    max: 2,
                    tickDecimals: 3
                },
                grid: {
                    backgroundColor: {colors: ["#fff", "#fff"]},
                    borderWidth: 1,
                    borderColor: '#555'
                }
            });

            //show the dropdowns on top or bottom depending on window height and menu position
            $('#task-tab .dropdown-hover').on('mouseenter', function (e) {
                var offset = $(this).offset();

                var $w = $(window)
                if (offset.top > $w.scrollTop() + $w.innerHeight() - 100)
                    $(this).addClass('dropup');
                else $(this).removeClass('dropup');
            });

        })

        function loadStatusData() {
            var url = "<?php echo route('dashboard.adminGetTrainingStatus'); ?>";
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                data: {status: 1},
                success: function (data) {
                    $("#tran_sts").hide();
                    var table = '<table class="table table-bordered" id="tran_sts_tbl">\n' +
                        '                <thead>\n' +
                        '                <tr>\n' +
                        '                    <th scope="col">Department Name</th>\n' +
                        '                    <th scope="col">Total Training</th>\n' +
                        '                    <th scope="col">Total Complete</th>\n' +
                        '                    <th scope="col">Total Pending</th>\n' +
                        '                </tr>\n' +
                        '                </thead>\n' +
                        '                <tbody>';

                    $.each(data, function (key, value) {
                        table = table + '<tr><td>' + value.name + '</td><td>' + value.total_Training + '</td><td>' + value.total_finished + '</td><td>' + value.total_pending + '</td></tr>';
                    });

                    table = table + '</tbody>\n' +
                        '            </table>';
                    $('.tran_sts_tbl').html(table);
                },
                error: function (err) {
                    alert('Data could not be loaded.');
                }
            });
        }

        function loadRegistrationData() {
            var url = "<?php echo route('dashboard.adminGetRegistration'); ?>";
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {status: 1},
                success: function (data) {
                    $("#tran_reg").hide();
                    var table = '<table class="table table-bordered" id="tran_reg_tbl">\n' +
                        '                <thead>\n' +
                        '                <tr>\n' +
                        '                    <th scope="col">Department Name</th>\n' +
                        '                    <th scope="col">Total Registration</th>\n' +
                        '                    <th scope="col">Total Participant</th>\n' +
                        '                </tr>\n' +
                        '                </thead>\n' +
                        '                <tbody>';

                    $.each(JSON.parse(data), function (key, value) {
                        table = table + '<tr><td>' + value.name + '</td><td>' + value.total_reg + '</td><td>' + value.reg_confirm + '</td></tr>';
                    });

                    table = table + '</tbody>\n' +
                        '            </table>';
                    $('.tran_reg_tbl').html(table);
                },
                error: function (err) {
                    alert('Data could not be loaded.');
                }
            });
        }

        function loadLatestData() {
            var url = "<?php echo route('dashboard.adminGetLastTraining'); ?>";
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {status: 1},
                success: function (data) {
                    $("#tran_last").hide();
                    var table = '<table class="table table-bordered" id="tran_latest_tbl">\n' +
                        '                <thead>\n' +
                        '                <tr>\n' +
                        '                    <th scope="col">Department Name</th>\n' +
                        '                    <th scope="col">Training Name</th>\n' +
                        '                    <th scope="col">Total Day</th>\n' +
                        '                    <th scope="col">Start Date</th>\n' +
                        '                    <th scope="col">Exam Date</th>\n' +
                        '                    <th scope="col">Budget</th>\n' +
                        '                </tr>\n' +
                        '                </thead>\n' +
                        '                <tbody>';

                    $.each(JSON.parse(data), function (key, value) {
                        table = table + '<tr><td>' + value.name + '</td><td>' + value.training_name + '</td><td>' + value.total_day + '</td><td>' + value.start_date + '</td><td>' + value.exam_date + '</td><td>' + value.budget + '</td></tr>';
                    });

                    table = table + '</tbody>\n' +
                        '            </table>';
                    $('.tran_latest_tbl').html(table);
                },
                error: function (err) {
                    alert('Data could not be loaded.');
                }
            });
        }

        function loadPiChart() {
            var url = "<?php echo route('dashboard.adminTrainingPiChart'); ?>";
            $.ajax({
                url: url,
                dataType: 'html',
                type: 'GET',
                data: {status: 1},
                success: function (data) {
                    var placeholder = $('#piechart-placeholder').css({'width': '90%', 'min-height': '150px'});
                    $.plot(placeholder, JSON.parse(data), {
                        series: {
                            pie: {
                                show: true,
                                tilt: 0.8,
                                highlight: {
                                    opacity: 0.25
                                },
                                stroke: {
                                    color: '#fff',
                                    width: 2
                                },
                                startAngle: 2
                            }
                        },
                        legend: {
                            show: true,
                            position: "ne",
                            labelBoxBorderColor: null,
                            margin: [-30, 15]
                        }
                        ,
                        grid: {
                            hoverable: true,
                            clickable: true
                        }
                    })

                    var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
                    var previousPoint = null;

                    placeholder.on('plothover', function (event, pos, item) {
                        if (item) {
                            if (previousPoint != item.seriesIndex) {
                                previousPoint = item.seriesIndex;
                                var tip = item.series['label'] + " : " + item.series['percent'] + '%';
                                $tooltip.show().children(0).text(tip);
                            }
                            $tooltip.css({top: pos.pageY + 10, left: pos.pageX + 10});
                        } else {
                            $tooltip.hide();
                            previousPoint = null;
                        }

                    });
                },
                error: function (err) {
                    console.log(err);
                    alert('Posts could not be loaded.');
                }
            });
        }


        $(window).on('load', function () {
            loadStatusData();
            loadRegistrationData();
            loadLatestData();
            loadPiChart();
        });
    </script>
@stop