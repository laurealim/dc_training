<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="4%">
            {{--<label class="pos-rel">--}}
            {{--<input type="checkbox" class="ace"/>--}}
            {{--<span class="lbl"></span>--}}
            {{--</label>--}}
        </th>
        <th width="8%">Name</th>
        <th width="8%">Department</th>
        <th width="10%">Address</th>
        <th width="4%">Email</th>
        <th width="8%">Phone</th>
        <th width="10%">Image</th>
        <th width="6%">Gender</th>
        <th width="6%">Date of Barth</th>
        <th width="8%">User Type</th>
        <th width="8%">Created By</th>
        <th width="6%">Status</th>
        <th class="center" width="14%">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php $pageNo = (($clientLList->currentPage() - 1) * $clientLList->perPage()) + 1; ?>
    @if(!empty($clientLList))
        @foreach ($clientLList as $client)
            <tr>
                <td class="center">
                    <label class="pos-rel">
                        {{--<input type="checkbox" class="ace"/>--}}
                        <span class="lbl">{{ $pageNo++ }}</span>
                    </label>
                </td>
                <td>
                    {{ $client->first_name }} {{ $client->last_name }}
                </td>
                <td>
                    {{ $departmentList[$client->dept_id] }}
                </td>
                <td>
                    {{ $client->address }}
                </td>
                <td>
                    {{ $client->email }}
                </td>
                <td>
                    {{ $client->phone }}
                </td>
                <td class="center">
                    <img src="{{ asset('storage/clients/'.$client->id.'/'.$client->image) }}" class="msg-photo"
                         alt="Kate's Avatar" width="120px" height="80px"/>
                    {{--                <img src="{{ asset('images/avatars/avatar2.png') }}" class="msg-photo" alt="Kate's Avatar"/>--}}
                </td>
                <td class="center">
                    {{ ucfirst($client->gender) }}
                </td>
                <td>
                    {{ $client->dob }}
                </td>
                <td class="center">
                    <?php if($client->user_type == 1){ ?>
                    <i class="normal-icon ace-icon fa fa-user pink bigger-200"></i>
                    {{--<span>&nbsp;Super Admin</span>--}}
                    <?php } else if($client->user_type == 2){ ?>
                    <i class="normal-icon ace-icon fa fa-user blue bigger-200"></i>
                    {{--<span>&nbsp;Owner</span>--}}
                    <?php } else if($client->user_type == 3){ ?>
                    <i class="normal-icon ace-icon fa fa-user green bigger-200"></i>
                    {{--<span>&nbsp;Client</span>--}}
                    <?php }?>
                </td>
                <td class="center">
                    <?php
                    $userData = \App\User::getDataById($client->created_by);
                    echo $userData['created_by'] != 0 ? $userData['first_name'] : "Admin";
                    ?>
                </td>
                <td class="center">
                    <?php if($client->status == 1){ ?>
                    <span class='label label-success'>Active</span>
                    <?php } else if($client->status == 2){ ?>
                    <span class='label label-warning'>Inactive</span>
                    <?php } else{?>
                    <span class='label label-danger'>Deleted</span>
                    <?php }?>
                </td>
                <td class="center">
                    <div class="hidden-sm hidden-xs action-buttons">
                        {{--<a class="blue" href="{{ route('') }}">--}}
                        {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                        {{--</a>--}}

                        {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                        <a class="green" href="{{ route('client.adminEdit',$client->id) }}" title="Edit">
                            <i class="ace-icon fa fa-pencil bigger-150"></i>
                        </a>

                        &nbsp;| &nbsp;

                        <a class="red deletebtn" href="{{ route('client.adminDelete',$client->id) }}"
                           id="{{ $client->id }}" title="Delete">
                            <i class="ace-icon fa fa-trash-o bigger-150"></i>
                        </a>

                        &nbsp;| &nbsp;

                        <a class="blue resetPass" href="{{ route('client.adminResetPassword',$client->id) }}"
                           id="{{ $client->id }}" title="Reset Password">
                            <i class="ace-icon fa fa-key bigger-150"></i>
                        </a>

                        {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                        {{--<a class="red deletebtn" href="{{ route('country.adminDelete',$country->id) }}" id={{ $country->id }}>--}}
                        {{--<i class="ace-icon fa fa-trash-o bigger-200"></i>--}}
                        {{--</a>--}}
                    </div>

                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($clientLList->currentPage() - 1) * $clientLList->perPage()) + 1 }}
            to {{ (($clientLList->currentPage() - 1) * $clientLList->perPage()) + $clientLList->perPage() }} of
            {{ $clientLList->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $clientLList->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>

<style type="text/css">
    /*.table tbody > tr > td {*/
    /*text-align: center;*/
    /*}*/
    .facility {
        font-weight: bold;
    }
</style>