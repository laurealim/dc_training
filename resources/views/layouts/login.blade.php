<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.head')
    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-9R4R6KREK7"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-9R4R6KREK7');
        </script>
</head>

<body class="login-layout light-login">

    <div class="main-container">
        <div class="main-content">
                <div class="row">
                    @yield('content')
                </div>
            <!-- /.page-content -->
        </div>
    </div>
    <!-- /.main-content -->

    <div class="footer">
        @include('includes.footer')
    </div>
<!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="{{ asset('js/template/jquery-2.1.4.min.js') }}"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if ('ontouchstart' in document.documentElement) document.write("<script src='{{ asset('js/template/jquery.mobile.custom.min.js') }} '>" + "<" + "/script>");
</script>
<script src="{{ asset('js/template/bootstrap.min.js') }}"></script>

<!-- page specific plugin scripts -->

<!-- ace scripts -->
<script src="{{ asset('js/template/ace-elements.min.js') }}"></script>
<script src="{{ asset('js/template/ace.min.js') }}"></script>

@yield('custom_style')
@yield('custom_script')
        <!-- inline scripts related to this page -->
</body>
</html>
