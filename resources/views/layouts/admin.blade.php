<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.head')
    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-9R4R6KREK7"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'G-9R4R6KREK7');
        </script>
</head>

<body class="no-skin">
<div id="navbar" class="navbar navbar-default          ace-save-state">
    @include('includes.navbar')
</div>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <div id="sidebar" class="sidebar responsive ace-save-state">
        @include('includes.navbar_side')
    </div>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                @yield('breadcrumb')
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-navbar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-sidebar" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-breadcrumbs" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state"
                                       id="ace-settings-add-container" autocomplete="off"/>
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div>
                        <!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight"
                                       autocomplete="off"/>
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div>
                        <!-- /.pull-left -->
                    </div>
                    <!-- /.ace-settings-box -->
                </div>
                <!-- /.ace-settings-container -->

                <div class="page-header">
                    @yield('page_header')
                </div>
                <!-- /.page-header -->

                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">�</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    {{--                            <img src="images/{{ Session::get('image') }}">--}}
                @endif
                @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">�</button>
                        {{--<strong>Whoops!</strong> There were some problems during profile update.--}}
                        {{--<br>--}}
                        <ul>
                            <li>{{ $message }}</li>
                        </ul>
                    </div>
                    {{--                            <img src="images/{{ Session::get('image') }}">--}}
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        {{--<strong>Whoops!</strong> There were some problems during profile update.--}}
                        {{--<br>--}}
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="row" id="main">
                    @yield('content')
                </div>
                <!-- /.row -->
            </div>
            <!-- /.page-content -->
        </div>
    </div>
    <!-- /.main-content -->

    <div class="footer">
        @include('includes.footer')
    </div>

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div>
<!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="{{ asset('js/template/jquery-2.1.4.min.js') }}"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="{{ asset('js/template/jquery-1.11.3.min.js') }}"></script>
<![endif]-->
<script type="text/javascript">
    $('.ace-settings-container').hide();
    if ('ontouchstart' in document.documentElement) document.write("<script src='{{ asset('js/template/jquery.mobile.custom.min.js') }} '>" + "<" + "/script>");
</script>
<script src="{{ asset('js/template/bootstrap.min.js') }}"></script>

<!-- page specific plugin scripts -->
<script src="{{ asset('js/template/wizard.min.js') }}"></script>
<script src="{{ asset('js/template/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/template/jquery-additional-methods.min.js') }}"></script>
<script src="{{ asset('js/template/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/template/jquery.dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/template/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('js/template/buttons.flash.min.js') }}"></script>
<script src="{{ asset('js/template/buttons.html5.min.js') }}"></script>
<script src="{{ asset('js/template/buttons.print.min.js') }}"></script>
<script src="{{ asset('js/template/buttons.colVis.min.js') }}"></script>
<script src="{{ asset('js/template/dataTables.select.min.js') }}"></script>

<!--[if lte IE 8]>
<script src="{{ asset('js/template/excanvas.min.js') }}"></script>
<![endif]-->
<script src="{{ asset('js/template/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/template/jquery-ui.custom.min.js') }}"></script>
<script src="{{ asset('js/template/jquery.ui.touch-punch.min.js') }}"></script>
<script src="{{ asset('js/template/chosen.jquery.min.js') }}"></script>
<script src="{{ asset('js/template/jquery.gritter.min.js') }}"></script>
<script src="{{ asset('js/template/bootbox.js') }}"></script>
<script src="{{ asset('js/template/jquery.easypiechart.min.js') }}"></script>
<script src="{{ asset('js/template/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/template/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('js/template/moment.min.js') }}"></script>
<script src="{{ asset('js/template/daterangepicker.min.js') }}"></script>
<script src="{{ asset('js/template/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/template/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('js/template/jquery.knob.min.js') }}"></script>
<script src="{{ asset('js/template/autosize.min.js') }}"></script>
<script src="{{ asset('js/template/jquery.inputlimiter.min.js') }}"></script>
<script src="{{ asset('js/template/jquery.hotkeys.index.min.js') }}"></script>
<script src="{{ asset('js/template/bootstrap-wysiwyg.min.js') }}"></script>
<script src="{{ asset('js/template/select2.min.js') }}"></script>
<script src="{{ asset('js/template/spinbox.min.js') }}"></script>
<script src="{{ asset('js/template/bootstrap-editable.min.js') }}"></script>
<script src="{{ asset('js/template/ace-editable.min.js') }}"></script>
<script src="{{ asset('js/template/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/template/bootstrap-tag.min.js') }}"></script>
<script src="{{ asset('js/template/jquery.sparkline.index.min.js') }}"></script>
<script src="{{ asset('js/template/jquery.flot.min.js') }}"></script>
<script src="{{ asset('js/template/jquery.flot.pie.min.js') }}"></script>
<script src="{{ asset('js/template/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('js/template/spin.js') }}"></script>

<!-- ace scripts -->
<script src="{{ asset('js/template/ace-elements.min.js') }}"></script>
<script src="{{ asset('js/template/ace.min.js') }}"></script>

@yield('custom_style')
@yield('custom_script')

        <!-- inline scripts related to this page -->
</body>
</html>
