<div id="dynamic-table_wrapper" class="dataTables_wrapper form-inline no-footer">
    <table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
        <thead>
        <tr>
            <th class="center" width="6%">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    {{--<span class="lbl"></span>--}}
                    Sr. No.
                </label>
            </th>
            <th width="28%">Department Name</th>
            <th width="28%">Created By</th>
            <th width="28%">Status</th>
            <th class="center" width="10%">Action</th>
        </tr>
        </thead>

        <tbody>

        <?php $pageNo = (($departments->currentPage() - 1) * $departments->perPage()) + 1 ?>

        @foreach ($departments as $department)
            <tr>
                <td class="center">
                    <label class="pos-rel">
                        {{--<input type="checkbox" class="ace"/>--}}
                        <span class="lbl">{{ $pageNo++ }}</span>
                    </label>
                </td>
                <td>
                    {{ $department->name}}
                </td>
                <td>
                    <?php echo config('constants.userType.'.$department->created_by);?>
                </td>
                <td>
                    <?php echo config('constants.status.'.$department->status);?>
                </td>
                <td class="center">
                    <div class="hidden-sm hidden-xs action-buttons">
                        {{--<a class="blue" href="{{ route('') }}">--}}
                        {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                        {{--</a>--}}

                        {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                        <a class="green" href="{{ route('department.adminEdit',$department->id) }}">
                            <i class="ace-icon fa fa-pencil bigger-200"></i>
                        </a>

                        &nbsp;&nbsp;| &nbsp;&nbsp;

                        <a class="red deletebtn" href="{{ route('department.adminDestroy',$department->id) }}" id={{ $department->id }}>
                        <i class="ace-icon fa fa-trash-o bigger-200"></i>
                        </a>
                    </div>

                    {{--<div class="hidden-md hidden-lg">--}}
                    {{--<div class="inline pos-rel">--}}
                    {{--<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown"--}}
                    {{--data-position="auto">--}}
                    {{--<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>--}}
                    {{--</button>--}}
                    {{--<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">--}}
                    {{--<li>--}}
                    {{--<a href="#" class="tooltip-info" data-rel="tooltip" title="View">--}}
                    {{--<span class="blue">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-120"></i>--}}
                    {{--</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                    {{--<a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">--}}
                    {{--<span class="green">--}}
                    {{--<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>--}}
                    {{--</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}

                    {{--<li>--}}
                    {{--<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">--}}
                    {{--<span class="red">--}}
                    {{--<i class="ace-icon fa fa-trash-o bigger-120"></i>--}}
                    {{--</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="row">
        <div class="col-xs-6">
            <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
                Showing {{ (($departments->currentPage() - 1) * $departments->perPage()) + 1 }}
                to {{ (($departments->currentPage() - 1) * $departments->perPage()) + $departments->perPage() }} of
                {{ $departments->total() }} entries
            </div>
        </div>
        <div class="col-xs-6">
            <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
                {{ $departments->links("vendor.pagination.custom") }}
            </div>
        </div>
    </div>
    {{--<div style="width: 100%">--}}
    {{--</div>--}}
    {{--{{ $cities->count() }}--}}
    {{--<br>--}}
    {{--{{ $cities->currentPage() }}--}}
    {{--<br>--}}
    {{--{{ $cities->perPage() }}--}}
    {{--<br>--}}
    {{--<br>--}}
    {{--{{ $cities->total() }}--}}
    {{--<br>--}}
</div>
