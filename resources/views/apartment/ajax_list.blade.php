<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="3%">
            {{--<label class="pos-rel">--}}
            {{--<input type="checkbox" class="ace"/>--}}
            {{--<span class="lbl"></span>--}}
            {{--</label>--}}
        </th>
        <th width="10%">Property Name</th>
        <th width="3%">Apartment Number</th>
        <th width="3%">Apartment Size</th>
        <th width="3%">Apartment Level</th>
        <th width="3%">Total Bedroom</th>
        <th width="3%">Total Toilet</th>
        <th width="3%">Total Veranda</th>
        <th width="10%">Facilities</th>
        <th width="10%">Rent Information (BDT)</th>
        <th width="10%">Utility Bill Information (BDT)</th>
        <th width="15%">Advance Payment (BDT)</th>
        <th width="3%">Owner</th>
        <th width="3%">Created By</th>
        <th width="3%">Status</th>
        <th width="3%">Allocation</th>
        <th class="center" width="12%">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php $pageNo = (($apartments->currentPage() - 1) * $apartments->perPage()) + 1; ?>
    @foreach ($apartments as $apartment)
        <?php //pr($apartment);?>
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $apartment->property_name }}
            </td>
            <td>
                {{ $apartment->apt_number }}
            </td>
            <td>
                {{ $apartment->apt_size }}
            </td>
            <td>
                {{ $apartment->apt_level }}
            </td>
            <td>
                {{ $apartment->bed_room }}
            </td>
            <td>
                {{ $apartment->toilet }}
            </td>
            <td>
                {{ $apartment->veranda }}
            </td>
            <td>
                <span class="facility">Intercom : &nbsp;&nbsp;&nbsp;</span>{{ ($apartment->intercom == 1) ? "Available" : "Not Available" }}
                <br/>
                <span class="facility"> Geyser : &nbsp;&nbsp;&nbsp; </span>{{ ($apartment->geyser == 1) ? "Available" : "Not Available" }}
                <br/>
                <span class="facility">Car Parking : &nbsp;&nbsp;&nbsp; </span>{{ ($apartment->car_parking == 1) ? "Available" : "Not Available" }}
            </td>
            <td>
                <span class="facility">Rent Amount : &nbsp;&nbsp;&nbsp;</span>{{ number_format($apartment->rent_amount, 2, '.', '') }}
                <br/>
                <span class="facility"> Service Charge : &nbsp;&nbsp;&nbsp; </span>{{ number_format($apartment->service_charge, 2, '.', '') }}
            </td>
            <td>
                <span class="facility">Electricity Bill : &nbsp;&nbsp;&nbsp;</span>{{ ($apartment->electricity == 1) ? "Tenant" : "Not Included" }}
                <br/>
                <span class="facility"> Gas Bill : &nbsp;&nbsp;&nbsp; </span>{{ ($apartment->gas == 1) ? "Tenant" : "Not Included" }}
                <br/>
                <span class="facility"> Water Bill : &nbsp;&nbsp;&nbsp; </span>{{ ($apartment->water == 1) ? "Tenant" : "Not Included" }}
            </td>
            <td>
                <span class="facility">Advance Amount per month : &nbsp;&nbsp;&nbsp;</span>{{ number_format($apartment->adv_rent, 2, '.', '') }}
                <br/>
                <span class="facility">Advance Payment for : &nbsp;&nbsp;&nbsp; </span>{{ $apartment->adv_month }}
                months
                <br/>
                <span class="facility">Total Advance Payment : &nbsp;&nbsp;&nbsp; </span>{{ number_format((($apartment->adv_month) * ($apartment->adv_rent)), 2, '.', '')  }}

            </td>
            <td class="center">
                {{ $apartment->first_name}}
            </td>
            <td class="center">
                {{ $apartment->user->first_name}}
            </td>
            <td>
                <?php if($apartment->status == 1){ ?>
                <span class='label label-success'>Active</span>
                <?php } else if($apartment->status == 2){ ?>
                <span class='label label-warning'>Inactive</span>
                <?php } else{?>
                <span class='label label-danger'>Deleted</span>
                <?php }?>
            </td>
            <td>
                <?php if($apartment->is_assigned == 1){ ?>
                <span class='label label-warning'>Allocated</span>
                <?php } elseif($apartment->is_assigned == 2){?>
                <span class='label label-danger'>Confirmed</span>
                <?php }else{?>
                <span class='label label-success'>Free</span>
                <?php }?>
            </td>
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    <a class="green" href="{{ route('apt.adminEdit',$apartment->id) }}">
                        <i class="ace-icon fa fa-pencil bigger-150"></i>
                    </a>

                    &nbsp;| &nbsp;

                    <a class="red deletebtn" href="{{ route('apt.adminDelete',$apartment->id) }}"
                       id={{ $apartment->id }}>
                        <i class="ace-icon fa fa-trash-o bigger-150"></i>
                    </a>

                    @if($apartment->is_assigned < 1)
                        &nbsp;| &nbsp;

                        <a class="red blue assign_client" href="#" title="Assign Client"
                           id={{ $apartment->id }}>
                            <i class="ace-icon fa fa-user-plus bigger-150"></i>
                        </a>
                    @endif

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    {{--<a class="red deletebtn" href="{{ route('country.adminDelete',$country->id) }}" id={{ $country->id }}>--}}
                    {{--<i class="ace-icon fa fa-trash-o bigger-200"></i>--}}
                    {{--</a>--}}
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($apartments->currentPage() - 1) * $apartments->perPage()) + 1 }}
            to {{ (($apartments->currentPage() - 1) * $apartments->perPage()) + $apartments->perPage() }} of
            {{ $apartments->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $apartments->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>

<style type="text/css">
    .facility {
        font-weight: bold;
    }
</style>