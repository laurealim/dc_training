<div class="navbar-container ace-save-state" id="navbar-container">
    <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
        <span class="sr-only">Toggle sidebar</span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>

        <span class="icon-bar"></span>
    </button>

    <div class="navbar-header pull-left">
        <a href="index.html" class="navbar-brand">
            <small>
                <i class="fa fa-leaf"></i>
                Training Management System
                {{--                {{ config('app.name', 'Training Management System') }}--}}
            </small>
        </a>
    </div>

    <div class="navbar-buttons navbar-header pull-right" role="navigation">
        <ul class="nav ace-nav">
            <li class="light-blue dropdown-modal">
                <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                    <img class="nav-user-photo"
                         src="{{ asset('storage/clients/'.Auth::user()->id.'/'.Auth::user()->image) }}"
                         alt="Jason's Photo"/>
                    <span class="user-info">
									<small>Welcome,</small>
                                    {{ Auth::user() ->first_name }}
								</span>

                    <i class="ace-icon fa fa-caret-down"></i>
                </a>

                <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                    @if(Auth::user()->user_type == config('constants.userType.SuperUser'))
                        <li>
                            <a href="{{ route('client.adminEdit',Auth::user()->id) }}">
                                <i class="ace-icon fa fa-user"></i>
                                Profile
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('client.adminChangePasswordForm') }}">
                                <i class="ace-icon fa fa-key"></i>
                                Change Password
                            </a>
                        </li>
                    @elseif(Auth::user()->user_type == config('constants.userType.Owner'))
                        <li>
                            <a href="{{ route('client.clientEdit',Auth::user()->id) }}">
                                <i class="ace-icon fa fa-user"></i>
                                Profile
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('client.clientChangePasswordForm') }}">
                                <i class="ace-icon fa fa-key"></i>
                                Change Password
                            </a>
                        </li>
                    @elseif(Auth::user()->user_type == config('constants.userType.Tenant'))
                        <li>
                            <a href="{{ route('client.userEdit',Auth::user()->id) }}">
                                <i class="ace-icon fa fa-user"></i>
                                Profile
                            </a>
                        </li>

                        <li>
                            <a href="{{ route('client.userChangePasswordForm') }}">
                                <i class="ace-icon fa fa-key"></i>
                                Change Password
                            </a>
                        </li>
                    @endif

                    <li class="divider"></li>

                    <li>
                        <?php $logout_url = "admin.logout"; ?>
                        @if(Auth::user()->user_type == config('constants.userType.SuperUser'))
                            <a href="{{ route('admin.logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <?php echo Auth::user()->first_name?> Logout
                            </a>
                        @elseif(Auth::user()->user_type == config('constants.userType.Owner'))
                            <?php $logout_url = "client.logout"; ?>
                            <a href="{{ route('client.logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <?php echo Auth::user()->first_name?> Logout
                            </a>
                        @elseif(Auth::user()->user_type == config('constants.userType.Tenant'))
                            <?php $logout_url = "user.logout"; ?>
                            <a href="{{ route('user.logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <?php echo Auth::user()->first_name?> Logout
                            </a>
                        @endif

                        <form id="logout-form" action="{{ route($logout_url) }}" method="POST"
                              style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div><!-- /.navbar-container -->