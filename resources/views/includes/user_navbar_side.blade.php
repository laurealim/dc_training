<script type="text/javascript">
    try {
//        ace.settings.loadState('sidebar')
    } catch (e) {
    }
</script>


<ul class="nav nav-list">
    @if(Auth::user()->user_type == config('constants.userType.Owner'))
        <li class="active open">
            <a href="{{ route('client.dashboard') }}">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
        </li>
        <li class="">
            <a href="{{ route('client.clientList') }}">
                <i class="menu-icon fa fa-users"></i>
                <span class="menu-text"> Users </span>
            </a>

            <b class="arrow"></b>
        </li>
        <li class="">
            <a href="{{ route('trade.clientList') }}">
                <i class="menu-icon fa fa-flag"></i>
                <span class="menu-text"> Trades </span>
            </a>

            <b class="arrow"></b>
        </li>
        <li class="">
            <a href="{{ route('training.clientList') }}">
                <i class="menu-icon fa fa-graduation-cap"></i>
                <span class="menu-text"> Training </span>
            </a>

            <b class="arrow"></b>
        </li>
        <li class="">
            <a href="{{ route('registration.clientList') }}">
                <i class="menu-icon fa fa-database"></i>
                <span class="menu-text">Trainee Registration </span>
            </a>

            <b class="arrow"></b>
        </li>
        <li class="">
            <a href="{{ route('participant.clientList') }}">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text"> Participant List </span>
            </a>
            <b class="arrow"></b>
        </li>
        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-line-chart"></i>
                <span class="menu-text"> Report </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li class="">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-pencil-square-o"></i>
                        <span class="menu-text"> Registration Report </span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="">
                            <a href="{{ route('report.clientTrainingWiseRegistration') }}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Registration Report
                            </a>

                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-pencil-square-o"></i>
                        <span class="menu-text"> Training Report </span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="">
                            <a href="{{ route('report.clientTrainingReport') }}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Training Report
                            </a>
                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>
                <li class="">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-pencil-square-o"></i>
                        <span class="menu-text"> Participant Report </span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="">
                            <a href="{{ route('report.clientParticipantReport') }}">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Participant Report
                            </a>
                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>

    @elseif(Auth::user()->user_type == config('constants.userType.Tenant'))
        <li class="active open">
            <a href="{{ route('user.dashboard') }}">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
        </li>

        <li class="">
            <a href="{{ route('registration.userList') }}">
                <i class="menu-icon fa fa-bullhorn"></i>
                <span class="menu-text">Trainee Registration </span>
            </a>

            <b class="arrow"></b>
        </li>
    @endif


    {{--<li class="">--}}
    {{--<a href="{{ route('client.adminList') }}">--}}
    {{--<i class="menu-icon fa fa-bullhorn"></i>--}}
    {{--<span class="menu-text"> Clients </span>--}}
    {{--</a>--}}

    {{--<b class="arrow"></b>--}}
    {{--</li>--}}

    {{--<li class="">--}}
    {{--<a href="#" class="dropdown-toggle">--}}
    {{--<i class="menu-icon fa fa-pencil-square-o"></i>--}}
    {{--<span class="menu-text"> Ledgers </span>--}}
    {{--<b class="arrow fa fa-angle-down"></b>--}}
    {{--</a>--}}
    {{--<b class="arrow"></b>--}}
    {{--<ul class="submenu">--}}
    {{--<li class="">--}}
    {{--<a href="{{ route('ledger.adminList') }}">--}}
    {{--<i class="menu-icon fa fa-caret-right"></i>--}}
    {{--Ledger Lists--}}
    {{--</a>--}}

    {{--<b class="arrow"></b>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</li>--}}

    {{--<li class="">--}}
    {{--<a href="#" class="dropdown-toggle">--}}
    {{--<i class="menu-icon fa fa-money"></i>--}}
    {{--<span class="menu-text"> Bills </span>--}}
    {{--<b class="arrow fa fa-angle-down"></b>--}}
    {{--</a>--}}
    {{--<b class="arrow"></b>--}}
    {{--<ul class="submenu">--}}
    {{--<li class="">--}}
    {{--<a href="{{ route('bill.adminList') }}">--}}
    {{--<i class="menu-icon fa fa-caret-right"></i>--}}
    {{--Bill Lists--}}
    {{--</a>--}}

    {{--<b class="arrow"></b>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</li>--}}

    {{--<li class="">--}}
    {{--<a href="#" class="dropdown-toggle">--}}
    {{--<i class="menu-icon fa fa-credit-card" aria-hidden="true"></i>--}}
    {{--<span class="menu-text"> Payment </span>--}}
    {{--<b class="arrow fa fa-angle-down"></b>--}}
    {{--</a>--}}
    {{--<b class="arrow"></b>--}}
    {{--<ul class="submenu">--}}
    {{--<li class="">--}}
    {{--<a href="{{ route('payment.adminList') }}">--}}
    {{--<i class="menu-icon fa fa-caret-right"></i>--}}
    {{--Payment Lists--}}
    {{--</a>--}}

    {{--<b class="arrow"></b>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</li>--}}

</ul><!-- /.nav-list -->

<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
    <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"
       data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
</div>