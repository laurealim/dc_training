<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="3%">
            Sr. No.
        </th>
        <th width="10%">প্রশিক্ষণার্থীর নাম</th>
        <th width="8%">প্রশিক্ষণের নাম</th>
        <th width="8%">মোবাইল নাম্বার</th>
        <th width="5%">ই-মেইল</th>
        <th width="10%">জন্ম নিবন্ধন / ভোটার আইডি নাম্বার</th>
        <th width="5%">বয়স</th>
        <th width="5%">ছবি</th>
        <th width="8%">বর্তমান ঠিকানা</th>
        <th width="5%">শিক্ষাগত যোগ্যতা</th>
        <th width="5%">বর্তমান পেশা</th>
        <th width="4%">Created By</th>
        <th width="4%">Status</th>
        <th class="center" width="15%">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php //pr($registrationList); ?>
    <?php $pageNo = (($registrationList->currentPage() - 1) * $registrationList->perPage()) + 1;?>
    @foreach ($registrationList as $registration)
        <?php if(array_key_exists($registration->training_id, $trainingList)){ ?>
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $registration->name_bn }}
            </td>
            <td>
                {{--                                {{ $trainingList[$registration->training_id] }}--}}
                {{ $trainingList[$registration->training_id] }}
            </td>
            <td>
                {{ $registration->phone }}
            </td>
            <td>
                {{ $registration->email }}
                {{--                {{ $departmentList[$registration->dept_id] }}--}}
            </td>
            <td>
                {{ $registration->nid }}
            </td>
            <td class="center">
                {{ $registration->age }}
            </td>
            <td class="center">
                <img src="{{ asset('storage/trainee/'.$registration->id.'/'.$registration->image) }}" class="msg-photo"
                     alt="Kate's Avatar" width="120px" height="80px"/>
            </td>
            <td>
                <?php
                $vill = $post = $upo = $dist = '';
                $add1 = $add2 = $add3 = $add4 = '';
                if (!empty($registration->vill_pre)) {
                    $add1 = $registration->vill_pre;
                }
                if (!empty($registration->post_pre)) {
                    $add2 = $addressArr[3][$registration->post_pre];
                }
                if (!empty($registration->up_pre)) {
                    $add3 = $addressArr[2][$registration->up_pre];
                }
                if (!empty($registration->dist_pre)) {
                    $add4 = $addressArr[1][$registration->dist_pre];
                }
                ?>
                {{ $add1 }}, {{ $add2 }}, {{ $add3 }} , {{ $add4 }}
{{--                {{ $registration->vill_pre }}, {{ $addressArr[3][$registration->post_pre] }}, {{ $addressArr[2][$registration->up_pre] }} , {{ $addressArr[1][$registration->dist_pre] }}--}}
            </td>
            <td>
                {{ config('constants.edu_level.arr.'.$registration->edu_lvl) }}
            </td>
            <td>
                {{ $registration->cur_job  }}
            </td>
            <td class="center">
                {{ $registration->user->first_name}}
            </td>
            <td class="center">
                <?php if ($registration->status == config('constants.registration_status.Waiting')) {?>
                <span class='label label-warning'><?php echo config('constants.registration_status.1'); ?></span>
                <?php } else if ($registration->status == config('constants.registration_status.Approved')) {?>
                <span class='label label-info'><?php echo config('constants.registration_status.2'); ?></span>
                <?php } else if ($registration->status == config('constants.registration_status.Confirmed')) {?>
                <span class='label label-success'><?php echo config('constants.registration_status.3'); ?></span>
                <?php } else {?>
                <span class='label label-danger'><?php echo config('constants.registration_status.0'); ?></span>
                <?php }?>
            </td>
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}
                    <?php if($registration->status != config('constants.registration_status.Confirmed')){ ?>

                    <a class="green" href="{{ route('registration.clientEdit',$registration->id) }}" title="Edit">
                        <i class="ace-icon fa fa-pencil bigger-150"></i>
                    </a>

                    &nbsp;| &nbsp;

                    <a class="red deletebtn" href="{{ route('registration.clientDestroy',$registration->id) }}"
                       id="{{ $registration->id }}" title="Delete">
                        <i class="ace-icon fa fa-trash-o bigger-150"></i>
                    </a>

                    &nbsp;| &nbsp;

                    <?php
                    if($registration->status == config('constants.registration_status.Approved')){?>
                    <a class="warning change_reg_status" title="Make Trainee Waiting."
                       href="{{ route('registration.clientRegStatusChange',array($registration->id,1)) }}"
                       id={{ $registration->id }} status="1">
                        <i class="ace-icon fa fa-times orange2 bigger-150"></i>
                    </a>
                    <?php
                    }elseif ($registration->status == config('constants.registration_status.Waiting')) {
                    ?>
                    <a class="blue change_reg_status" title="Approve Trainee."
                       href="{{ route('registration.clientRegStatusChange',array($registration->id,2)) }}"
                       id={{ $registration->id }} status="2">
                        <i class="ace-icon fa fa-check bigger-150"></i>
                    </a>
                    <?php
                    } }
                    ?>
                </div>
            </td>
        </tr>
        <?php
        }
        ?>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($registrationList->currentPage() - 1) * $registrationList->perPage()) + 1 }}
            to {{ (($registrationList->currentPage() - 1) * $registrationList->perPage()) + $registrationList->perPage() }}
            of
            {{ $registrationList->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $registrationList->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>

<style type="text/css">
    /*.table tbody > tr > td {*/
    /*text-align: center;*/
    /*}*/
    .facility {
        font-weight: bold;
    }
</style>
