@extends('layouts.user')

@section('title')
    Edit Registration
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('registration.clientList') }}">Registration List</a>
        </li>

        <li>
            <a href="{{ route('registration.clientEdit',$id) }}">Registration Edit</a>
        </li>
    </ul>
@stop

@section('page_header')
    <h1>Registration Form</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" action="{{ route('registration.clientUpdate', $id) }}" method="POST"
              enctype="multipart/form-data">
            {{ csrf_field() }}

            <h3 class="header smaller lighter blue">
                প্রশিক্ষণের তালিকা
            </h3>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="training_id"> প্রশিক্ষণের নাম *</label>
                <div class="col-sm-9">
                    <select id="training_id" placeholder="" name="training_id" class="col-xs-10 col-sm-5" required>
                        <option>--- বাছাই করুন ---</option>
                        <?php foreach ($trainingList as $id => $name) { ?>
                            <?php echo "<option value='{$id}'" . ($registrationData->training_id == $id ? ('selected="selected"') : '') . ">{$name}</option>"; ?>
                        <?php }?>
                    </select>
                </div>
            </div>
            <div class="space-4"></div>

            <h3 class="header smaller lighter blue">
                সাধারণ তথ্য
            </h3>
            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="name_bn"> শিক্ষার্থীর নাম(বাংলা) *</label>

                <div class="col-sm-8">
                    <input type="text" id="name_bn" placeholder="বাংলায়" name="name_bn"
                           value="{{ $registrationData->name_bn }}" class="col-xs-10 col-sm-12" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('name_bn'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('name_bn') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="name_en"> শিক্ষার্থীর নাম(ইংরেজি)
                    *</label>

                <div class="col-sm-8">
                    <input type="text" id="name_en" placeholder="ইংরেজিতে" name="name_en"
                           value="{{ $registrationData->name_en }}" class="col-xs-10 col-sm-12" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('name_en'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('name_en') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="f_name_bn"> পিতার নাম(বাংলা) *</label>

                <div class="col-sm-8">
                    <input type="text" id="f_name_bn" placeholder="বাংলায়" name="f_name_bn"
                           value="{{ $registrationData->f_name_bn }}" class="col-xs-10 col-sm-12" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('f_name_bn'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('f_name_bn') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group col-sm-6">
                <label class="col-sm-4 control-label no-padding-right" for="m_name_bn"> মাতার নাম(বাংলা) *</label>

                <div class="col-sm-8">
                    <input type="text" id="m_name_bn" placeholder="বাংলায়" name="m_name_bn"
                           value="{{ $registrationData->m_name_bn }}" class="col-xs-10 col-sm-12" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('m_name_bn'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('m_name_bn') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>
            <div class="space-4"></div>
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="header smaller lighter blue">
                        ঠিকানা
                    </h3>
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                বর্তমান ঠিকানা
                            </h4>
                        </div>
                        <div class="card-body" style="display: flow-root">
                            <br>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="dist_pre"> জেলা *</label>

                                <div class="col-sm-8">
                                    <select name='dist_pre' class="col-xs-10 col-sm-12" id="dist_pre">
                                        <option value="">--- বাছাই করুন ---</option>
                                        @foreach($listArr[0] as $id => $name)
                                            <option value="{{ $id }}" {{ $id == $registrationData->dist_pre ? 'selected="selected"' : '' }}>{{ $name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('dist_pre'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('dist_pre') }}</strong>
                                            </span>
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="up_pre"> থানা/উপোজেল
                                    *</label>

                                <div class="col-sm-8">
                                    <select name='up_pre' class="col-xs-8 col-sm-12" id="up_pre">
                                        <option value="">--- বাছাই করুন ---</option>
                                        @foreach($listArr[2] as $id => $name)
                                            <option value="{{ $id }}" {{ $id == $registrationData->up_pre ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('up_pre'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('up_pre') }}</strong>
                                            </span>
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">

                                <label class="col-sm-4 control-label no-padding-right" for="post_pre"> ইউনিয়ন *</label>

                                <div class="col-sm-8">
                                    <select name='post_pre' class="col-xs-10 col-sm-12" id="post_pre">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php if($registrationData->post_pre < 1){?>
                                            <option value=0 selected="selected">পৌরসভা</option>
                                        <?php } else{ ?>
                                            <option value=0 >পৌরসভা</option>
                                        <?php }?>
                                        @foreach($listArr[4] as $id => $name)
                                            <option value="{{ $id }}" {{ $id == $registrationData->post_pre ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('post_pre'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('post_pre') }}</strong>
                                            </span>
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="vill_pre"> গ্রাম/মহল্লা
                                    *</label>

                                <div class="col-sm-8">
                                    <input type="text" id="vill_pre" placeholder="গ্রাম/মহল্লা" name="vill_pre"
                                           class="col-xs-10 col-sm-12" value="{{ $registrationData->vill_pre }}" required/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('vill_pre'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('vill_pre') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space-4"></div>

                    <div><span><input type="checkbox" id="same_add" name="same_add" class="same_add">  স্থায়ী ঠিকানা ও বর্তমান ঠিকানা এক  </span></div>
                    <div class="card">
                        <div class="card-header">
                            <h4>
                                স্থায়ী ঠিকানা
                            </h4>
                        </div>
                        <div class="card-body" style="display: flow-root">
                            <br>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="dist_per"> জেলা *</label>

                                <div class="col-sm-8">
                                    <select name='dist_per' class="col-xs-10 col-sm-12" id="dist_per">
                                        <option value="">--- বাছাই করুন ---</option>
                                        @foreach($listArr[1] as $id => $name)
                                            <option value="{{ $id }}" {{ $id == $registrationData->dist_per ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('dist_per'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('dist_per') }}</strong>
                                            </span>
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="up_per"> থানা/উপোজেল
                                    *</label>

                                <div class="col-sm-8">
                                    <select name='up_per' class="col-xs-8 col-sm-12" id="up_per">
                                        <option value="">--- বাছাই করুন ---</option>
                                        @foreach($listArr[3] as $id => $name)
                                            <option value="{{ $id }}" {{ $id == $registrationData->up_per ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('up_per'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('up_per') }}</strong>
                                            </span>
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">

                                <label class="col-sm-4 control-label no-padding-right" for="post_per"> ইউনিয়ন *</label>

                                <div class="col-sm-8">
                                    <select name='post_per' class="col-xs-10 col-sm-12" id="post_per">
                                        <option value="">--- বাছাই করুন ---</option>
                                        <?php if($registrationData->post_per < 1){?>
                                            <option value=0 selected="selected">পৌরসভা</option>
                                        <?php } else{ ?>
                                        <option value=0 >পৌরসভা</option>
                                        <?php }?>
                                        @foreach($listArr[5] as $id => $name)
                                            <option value="{{ $id }}" {{ $id == $registrationData->post_per ? 'selected="selected"' : '' }} >{{ $name }}</option>
                                        @endforeach
                                    </select>
                                    <span class="help-inline col-xs-12 col-sm-7">
                                        @if ($errors->has('post_per'))
                                            <span class="help-block middle">
                                                <strong>{{ $errors->first('post_per') }}</strong>
                                            </span>
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label class="col-sm-4 control-label no-padding-right" for="vill_per"> গ্রাম/মহল্লা
                                    *</label>

                                <div class="col-sm-8">
                                    <input type="text" id="vill_per" placeholder="গ্রাম/মহল্লা" name="vill_per"
                                           class="col-xs-10 col-sm-12" value="{{ $registrationData->vill_per }}" required/>
                                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('vill_per'))
                                            <span class="help-block middle">
                                <strong>{{ $errors->first('vill_per') }}</strong>
                            </span>
                                        @endif
                                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space-4"></div>
                </div>
            </div>

            <div class="space-4"></div>
            <h3 class="header smaller lighter blue">
                শিক্ষগত যোগ্যতা
            </h3>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="edu_lvl"> সর্বশেষ অর্জন ডিগ্রী
                            *</label>

                        <div class="col-sm-8">
                            <select name='edu_lvl' class="col-xs-10 col-sm-12" id="edu_lvl" required>
                                <option value="">--- বাছাই করুন ---</option>
                                <?php foreach ($eduLevel_list as $id => $name) { ?>
                                    <?php echo "<option value='{$id}'" . ($registrationData->edu_lvl == $id ? ('selected="selected"') : '') . ">{$name}</option>"; ?>
                                <?php }?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('edu_lvl'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('edu_lvl') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>
            <h3 class="header smaller lighter blue">
                যোগাযোগ
            </h3>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="phone"> মোবাইল / টেলিফোন
                            *</label>

                        <div class="col-sm-8">
                            <input type="text" id="phone" placeholder="i.e. 017XXXXXXXX"
                                   name="phone"
                                   value="{{ $registrationData->phone }}" class="col-xs-10 col-sm-12" required/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('phone'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="email"> ই-মেইল</label>

                        <div class="col-sm-8">
                            <input type="text" id="email" placeholder="i.e. demo@gmail.com"
                                   name="email"
                                   value="{{ $registrationData->email }}" class="col-xs-10 col-sm-12"/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('email'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="nid"> জন্মোনিবন্ধন /
                            ভোটার
                            আইডি নাম্বার *</label>

                        <div class="col-sm-8">
                            <input type="text" id="nid" placeholder="i.e. 1234567890" name="nid"
                                   value="{{ $registrationData->nid }}" class="col-xs-10 col-sm-12" required/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('nid'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('nid') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>
            <h3 class="header smaller lighter blue">
                বিবিধ তথ্য
            </h3>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="dob"> জন্ম তারিখ
                            *</label>

                        <div class="col-sm-8">
                            <input class="date-picker form-control" id="dob" type="text" name="dob"
                                   value="{{ date('d-m-Y', strtotime($registrationData->dob)) }}"
                                   data-date-format="dd-mm-yyyy" required/>
                            <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="training_place"> বয়স *</label>

                        <div class="col-sm-8">
                            <input type="number" id="age" placeholder="i.e. 25"
                                   name="age"
                                   value="{{ $registrationData->age }}" class="col-xs-10 col-sm-12" required/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('age'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('age') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="relg"> ধর্ম *</label>

                        <div class="col-sm-8">
                            <select name='relg' class="col-xs-10 col-sm-12" id="relg" required>
                                <option value="">--- বাছাই করুন ---</option>
                                <?php foreach ($relrg_list as $id => $name) { ?>
                                    <?php echo "<option value='{$id}'" . ($registrationData->relg == $id ? ('selected="selected"') : '') . ">{$name}</option>"; ?>
                                <?php }?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('relg'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('relg') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="nation"> জাতীয়তা *</label>

                        <div class="col-sm-8">
                            <input type="text" id="nation" placeholder="i.e. Bangladeshi" name="nation"
                                   value="{{ $registrationData->nation }}" class="col-xs-10 col-sm-12" required/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('nation'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('nation') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="gender"> লিঙ্গ *</label>

                        <div class="col-sm-8">
                            <select name='gender' class="col-xs-10 col-sm-12" id="gender" required>
                                <option value="">--- বাছাই করুন ---</option>
                                <?php foreach ($gender_list as $id => $name) { ?>
                                    <?php echo "<option value='{$id}'" . ($registrationData->gender == $id ? ('selected="selected"') : '') . ">{$name}</option>"; ?>
                                <?php }?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('gender'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('gender') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="married"> বৈবাহিক অবস্থা
                            *</label>

                        <div class="col-sm-8">
                            <select name='married' class="col-xs-10 col-sm-12" id="married" required>
                                <option value="">--- বাছাই করুন ---</option>
                                <?php foreach ($marital_list as $id => $name) { ?>
                                    <?php echo "<option value='{$id}'" . ($registrationData->married == $id ? ('selected="selected"') : '') . ">{$name}</option>"; ?>
                                <?php }?>
                            </select>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('married'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('married') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>

                    <div class="form-group col-sm-10">
                        <div class="space-4"></div>

                        <label class="control-label col-xs-12 col-sm-2 no-padding-right "
                               for="image">ছবি (সর্বোচ্চ ২ MB ) *</label>

                        <div class="col-xs-10 col-sm-10">
                            <input type="file" id="image" class="col-xs-10 col-sm-5" name="image"/>
                            <span class="help-inline col-xs-12 col-sm-7">
                                    @if ($errors->has('image'))
                                    <span class="help-block middle">
                                            <strong>{{ $errors->first('image') }}</strong>
                                        </span>
                                @endif
                                </span>
                        </div>
                    </div>
                    </div>
                    <div class="form-group col-sm-2">
                        <div class="col-xs-10 col-sm-2 col-md-4" style="margin-top:10px">
                            <?php if($registrationData->image != ""){ ?>
                            <img src="{{ asset('storage/trainee/'.$registrationData->id.'/'.$registrationData->image) }}"
                                 class="msg-photo" alt="Kate's Avatar" width="120px" height="80"/>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>
            <h3 class="header smaller lighter blue">
                অভিজ্ঞতা
            </h3>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="cur_job"> বর্তমান
                            পেশা </label>

                        <div class="col-sm-8">
                            <input type="text" id="cur_job" placeholder=""
                                   name="cur_job"
                                   value="{{ $registrationData->cur_job }}" class="col-xs-10 col-sm-12"/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('cur_job'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('cur_job') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label class="col-sm-4 control-label no-padding-right" for="prev_trng_name"> সর্বশেষ
                            প্রশিক্ষণ(যদি
                            থাকে) </label>

                        <div class="col-sm-8">
                            <input type="text" id="prev_trng_name" placeholder=""
                                   name="prev_trng_name"
                                   value="{{ $registrationData->prev_trng_name }}" class="col-xs-10 col-sm-12"/>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('prev_trng_name'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('prev_trng_name') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group col-sm-12">
                        <label class="col-sm-3 control-label no-padding-right" for="prev_exp"> পূর্ব প্রশিক্ষণের
                            বিবরণ
                            (যদি থাকে) </label>

                        <div class="col-sm-9">
                        <textarea id="prev_exp" placeholder="পূর্ব প্রশিক্ষণের বিবরণ" name="prev_exp"
                                  class="col-xs-10 col-sm-12"><?php echo $registrationData->prev_exp ?> </textarea>
                            <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('prev_exp'))
                                    <span class="help-block middle">
                                <strong>{{ $errors->first('prev_exp') }}</strong>
                            </span>
                                @endif
                                {{--<span class="middle">Inline help text</span>--}}
                    </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>
            <div class="space-4"></div>
            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button id="submit" class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">

        /****************************/
        /****************************/
        /*  File Upload Section */
        /****************************/
        /****************************/

        $('#image').ace_file_input({
            style: 'well',
            btn_choose: 'Drop files here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'small'//large | fit
            //,icon_remove:null//set null, to hide remove/reset button
            /**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
            /**,before_remove : function() {
						return true;
					}*/
            ,
            preview_error: function (filename, error_code) {
                //name of the file that failed
                //error_code values
                //1 = 'FILE_LOAD_FAILED',
                //2 = 'IMAGE_LOAD_FAILED',
                //3 = 'THUMBNAIL_FAILED'
                //alert(error_code);
            }

        }).on('change', function () {
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
        });

        var whitelist_ext, whitelist_mime;
        var btn_choose;
        var no_icon;

        var file_input = $('#image');

        file_input
            .ace_file_input('update_settings',
                {
                    'btn_choose': btn_choose,
                    'no_icon': no_icon,
                    'allowExt': whitelist_ext,
                    'allowMime': whitelist_mime
                });

        file_input.ace_file_input('reset_input');

        file_input
            .off('file.error.ace')
            .on('file.error.ace', function (e, info) {
                //console.log(info.file_count);//number of selected files
                //console.log(info.invalid_count);//number of invalid files
                //console.log(info.error_list);//a list of errors in the following format

                //info.error_count['ext']
                //info.error_count['mime']
                //info.error_count['size']

                //info.error_list['ext']  = [list of file names with invalid extension]
                //info.error_list['mime'] = [list of file names with invalid mimetype]
                //info.error_list['size'] = [list of file names with invalid size]


                /**
                 if( !info.dropped ) {
							//perhapse reset file field if files have been selected, and there are invalid files among them
							//when files are dropped, only valid files will be added to our file array
							e.preventDefault();//it will rest input
						}
                 */


                //if files have been selected (not dropped), you can choose to reset input
                //because browser keeps all selected files anyway and this cannot be changed
                //we can only reset file field to become empty again
                //on any case you still should check files with your server side script
                //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
            });


        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/

        //datepicker plugin
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        })

        // Age Calculation Function
        function getAge(dateString) {
            var now = new Date();
            var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

            var yearNow = now.getFullYear();
            var monthNow = now.getMonth();
            var dateNow = now.getDate();

            let curDateFormat = new Date(dateString.substring(6, 10), dateString.substring(3, 5), dateString.substring(0, 2));

            var dobs = new Date(dateString.substring(6, 10), dateString.substring(3, 5), dateString.substring(0, 2) - 1);

            var yearDob = dobs.getFullYear();
            var monthDob = dobs.getMonth();
            var dateDob = dobs.getDate();
            var age = {};
            var ageString = "";
            var yearString = "";
            var monthString = "";
            var dayString = "";


            yearAge = yearNow - yearDob;

            if (monthNow >= monthDob)
                var monthAge = monthNow - monthDob;
            else {
                yearAge--;
                var monthAge = 12 + monthNow - monthDob;
            }

            if (dateNow >= dateDob)
                var dateAge = dateNow - dateDob;
            else {
                monthAge--;
                var dateAge = 31 + dateNow - dateDob;

                if (monthAge < 0) {
                    monthAge = 11;
                    yearAge--;
                }
            }

            age = {
                years: yearAge,
                months: monthAge,
                days: dateAge
            };

            if (age.years > 1) yearString = " years";
            else yearString = " year";
            if (age.months > 1) monthString = " months";
            else monthString = " month";
            if (age.days > 1) dayString = " days";
            else dayString = " day";

            let curYear, curMon, curDay;


            if ((age.years > 0) && (age.months > 0) && (age.days > 0)) {
                ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years == 0) && (age.months == 0) && (age.days > 0)) {
                ageString = "Only " + age.days + dayString + " old!";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years > 0) && (age.months == 0) && (age.days == 0)) {
                ageString = age.years + yearString + " old. Happy Birthday!!";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years > 0) && (age.months > 0) && (age.days == 0)) {
                ageString = age.years + yearString + " and " + age.months + monthString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years == 0) && (age.months > 0) && (age.days > 0)) {
                ageString = age.months + monthString + " and " + age.days + dayString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years > 0) && (age.months == 0) && (age.days > 0)) {
                ageString = age.years + yearString + " and " + age.days + dayString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else if ((age.years == 0) && (age.months > 0) && (age.days == 0)) {
                ageString = age.months + monthString + " old.";
                curYear = age.years;
                curMon = age.months + 1;
                curDay = age.days;
            } else ageString = "Oops! Could not calculate age!";

            return {curYear, curMon, curDay};
        }

        $("#dob").on('change', function () {
            var date = $(this).val();
            var resData = getAge(date);
            console.log(resData);
            $("#age").val(resData.curYear);
        })

        $("#submit").click(function(e) {
            // e.preventDefault();
            var logoimg = document.getElementById("image");
            let size = logoimg.files[0].size;
            // if (size > 13334325) {
            if (size > 3000000) {
                alert("Logo Image Size is exceeding 2 Mb");
                e.preventDefault();
            }
        });



        $('#dist_pre').on('change', function () {
            var districtID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('upazila.clientUpazilaSelectAjaxList') }}";
            if (districtID) {
                $('select[name="up_pre"]').empty();
                $('select[name="post_pre"]').empty();
                upazillaList(districtID, token, url, 'pre');
            } else {
                $('select[name="up_pre"]').empty();
                $('select[name="post_pre"]').empty();
            }
        });

        $('#up_pre').on('change', function () {
            var upazilaID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('union.clientUnionSelectAjaxList') }}";
            if (upazilaID) {
                $('select[name="post_pre"]').empty();
                unionList(upazilaID, token, url, 'pre');
            } else {
                $('select[name="post_pre"]').empty();
            }
        });

        $('#dist_per').on('change', function () {
            var districtID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('upazila.clientUpazilaSelectAjaxList') }}";
            if (districtID) {
                $('select[name="up_per"]').empty();
                $('select[name="post_per"]').empty();
                upazillaList(districtID, token, url, 'per');
            } else {
                $('select[name="up_per"]').empty();
                $('select[name="post_per"]').empty();
            }
        });

        $('#up_per').on('change', function () {
            var upazilaID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('union.clientUnionSelectAjaxList') }}";
            if (upazilaID) {
                $('select[name="post_per"]').empty();
                unionList(upazilaID, token, url, 'per');
            } else {
                $('select[name="post_per"]').empty();
            }
        });

        function upazillaList(districtID, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {districtID: districtID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);

                    if (type == 'pre')
                        $('select[name="up_pre"]').append('<option value="">' + '--- বাছাই করুন ---' + '</option>');
                    else
                        $('select[name="up_per"]').append('<option value="">' + '--- বাছাই করুন ---' + '</option>');

                    $.each(data, function (key, value) {
                        if (type == 'pre')
                            $('select[name="up_pre"]').append('<option value="' + key + '">' + value + '</option>');
                        else
                            $('select[name="up_per"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        function unionList(upazilaID, token, url, type) {
            $.ajax({
                url: url,
                method: 'POST',
                data: {upazilaID: upazilaID, _token: token},
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    if (type == 'pre'){
                        $('select[name="post_pre"]').append('<option value="">' + '--- বাছাই করুন ---' + '</option>');
                        $('select[name="post_pre"]').append('<option value="' + 0 + '">' + 'পৌরসভা' + '</option>');
                    }
                    else{
                        $('select[name="post_per"]').append('<option value="">' + '--- বাছাই করুন ---' + '</option>');
                        $('select[name="post_per"]').append('<option value="' + 0 + '">' + 'পৌরসভা' + '</option>');
                    }
                    $.each(data, function (key, value) {
                        if (type == 'pre')
                            $('select[name="post_pre"]').append('<option value="' + key + '">' + value + '</option>');
                        else
                            $('select[name="post_per"]').append('<option value="' + key + '">' + value + '</option>');
                    });
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Status: " + textStatus);
                    alert("Error: " + errorThrown);
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        };

        $("#same_add").on("change", function () {
            let dist_pre = $("#dist_pre").val();
            let up_pre = $("#up_pre").val();
            let post_pre = $("#post_pre").val();
            if ($("#post_pre").val()) {
                if ($(this).prop("checked") == true) {
                    $("#dist_per").prop("disabled",true);
                    $("#up_per").prop("disabled",true);
                    $("#post_per").prop("disabled",true);
                    $("#vill_per").prop("disabled",true);
                    console.log("Checkbox is checked.")
                } else if ($(this).prop("checked") == false) {
                    $("#dist_per").prop("disabled",false);
                    $("#up_per").prop("disabled",false);
                    $("#post_per").prop("disabled",false);
                    $("#vill_per").prop("disabled",false);
                    console.log("Checkbox is unchecked.")
                }
            } else {
                alert(" দয়া  করে  বর্তমান  ঠিকানা  আগে  বাছাই করুন।");
                $(this).prop("checked", false);
            }
        });
    </script>
@stop