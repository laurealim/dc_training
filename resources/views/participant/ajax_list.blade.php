<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="3%">
            Sr. No.
        </th>
        <th width="10%">প্রশিক্ষণার্থীর নাম</th>
        <th width="10%">প্রশিক্ষণের নাম</th>
        <th width="8%">মোবাইল নাম্বার</th>
        <th width="5%">ই-মেইল</th>
        <th width="10%">জন্ম নিবন্ধন / ভোটার আইডি নাম্বার</th>
        <th width="5%">বয়স</th>
        <th width="10%">বর্তমান ঠিকানা</th>
        <th width="5%">শিক্ষাগত যোগ্যতা</th>
        <th width="5%">বর্তমান পেশা</th>
        <th width="4%">Created By</th>
        <th width="5%">Status</th>
        <th class="center" width="15%">Action</th>
    </tr>
    </thead>

    <tbody>

<?php //pr($registrationList); ?>
<input type="hidden" id="uri" value="<?php echo $uri; ?>" />
    <?php $pageNo = (($registrationList->currentPage() - 1) * $registrationList->perPage()) + 1;?>
    @foreach ($registrationList as $registration)
        <!--        --><?php //pr($registration);?>
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $registration->name_bn }}
            </td>
            <td>
                {{--                {{ $trainingList[$registration->training_id] }}--}}
                {{ $trainingList[$registration->training_id] }}
            </td>
            <td>
                {{ $registration->phone }}
            </td>
            <td>
                {{ $registration->email }}
                {{--                {{ $departmentList[$registration->dept_id] }}--}}
            </td>
            <td>
                {{ $registration->nid }}
            </td>
            <td class="center">
                {{ $registration->age }}
            </td>
            <td>
                {{ $registration->vill_pre }}, {{ $registration->post_pre }}, {{ $registration->up_pre }}
                , {{ $registration->dist_pre }}
            </td>
            <td>
                {{ config('constants.edu_level.arr.'.$registration->edu_lvl) }}
            </td>
            <td>
                {{ $registration->cur_job  }}
            </td>
            <td class="center">
                {{ $registration->user->first_name}}
            </td>
            <td class="center">
                <?php if ($registration->status == config('constants.registration_status.Waiting')) {?>
                <span class='label label-warning'><?php echo config('constants.registration_status.1'); ?></span>
                <?php } else if ($registration->status == config('constants.registration_status.Approved')) {?>
                <span class='label label-info'><?php echo config('constants.registration_status.2'); ?></span>
                <?php } else if ($registration->status == config('constants.registration_status.Confirmed')) {?>
                <span class='label label-success'><?php echo config('constants.registration_status.3'); ?></span>
                <?php } else {?>
                <span class='label label-danger'><?php echo config('constants.registration_status.0'); ?></span>
                <?php }?>
            </td>
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}
                    <?php
                    if($registration->status == config('constants.registration_status.Approved')){?>
                    <a class="warning change_reg_status" title="Make Trainee Waiting."
                       href="{{ route('registration.adminRegStatusChange',array($registration->id,1)) }}" id={{ $registration->id }} status="1">
                        <i class="ace-icon fa fa-times orange2 bigger-150"></i>
                    </a>
                    <?php
                    }elseif ($registration->status == config('constants.registration_status.Waiting')) {
                    ?>
                    <a class="blue change_reg_status" title="Approve Trainee."
                       href="{{ route('registration.adminRegStatusChange',array($registration->id,2)) }}" id={{ $registration->id }} status="2">
                        <i class="ace-icon fa fa-check bigger-150"></i>
                    </a>
                    <?php
                    }
                    ?>
                    <?php if($registration->status != config('constants.registration_status.Confirmed')){ ?>
                    &nbsp; |
                    <a class="green confirmbtn" href="{{ route('participant.adminRegConfirm',$registration->id) }}" id={{ $registration->id }} title="Confirm">
                        <i class="ace-icon fa fa-check-square-o bigger-150"></i>
                    </a>
                    <?php }?>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($registrationList->currentPage() - 1) * $registrationList->perPage()) + 1 }}
            to {{ (($registrationList->currentPage() - 1) * $registrationList->perPage()) + $registrationList->perPage() }}
            of
            {{ $registrationList->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $registrationList->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>

<style type="text/css">
    /*.table tbody > tr > td {*/
    /*text-align: center;*/
    /*}*/
    .facility {
        font-weight: bold;
    }
</style>