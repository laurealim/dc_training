@extends('layouts.admin')

@section('title')
    Add Union
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('upazila.adminList') }}">Union List</a>
        </li>

        <li>
            <a href="{{ route('upazila.adminForm') }}">Union Add</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Add Union</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" action="{{ route('union.adminStore') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="division_id"> Division Name *</label>

                <div class="col-sm-9">
                    <select name='division_id' class="col-xs-10 col-sm-5" id="division_id">
                        <option value="">-- Please Select One --</option>
                        @foreach($divisionList as $id => $name)
                            <option value="{{ $id }}" }>{{ $name }}</option>
                        @endforeach
                    </select>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('division_id'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('divisions_id') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="district_id"> District Name *</label>

                <div class="col-sm-9">
                    <select name='district_id' class="col-xs-10 col-sm-5" id="district_id">
                        <option value="">-- Please Select One --</option>
{{--                        @foreach($districtList as $id => $name)--}}
{{--                            <option value="{{ $id }}" }>{{ $name }}</option>--}}
{{--                        @endforeach--}}
                    </select>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('district_id'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('district_id') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="upazilla_id"> Upazila Name *</label>

                <div class="col-sm-9">
                    <select name='upazilla_id' class="col-xs-10 col-sm-5" id="upazilla_id">
                        <option value="">-- Please Select One --</option>
{{--                        @foreach($upazilaList as $id => $name)--}}
{{--                            <option value="{{ $id }}" }>{{ $name }}</option>--}}
{{--                        @endforeach--}}
                    </select>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('upazilla_id'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('upazilla_id') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="name"> Union Name *</label>

                <div class="col-sm-9">
                    <input type="text" id="name" placeholder="Upazila Name" name="name"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="bn_name"> Union Name (Bangla)
                    *</label>

                <div class="col-sm-9">
                    <input type="text" id="bn_name" placeholder="Upazila Name(Bangla)" name="bn_name" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('bn_name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('bn_name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="url"> Union URL</label>

                <div class="col-sm-9">
                    <input type="text" id="url" placeholder="Upazila URL" name="url" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('url'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('url') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            {{--<div class="form-group">--}}
            {{--<label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Status </label>--}}

            {{--<div class="col-sm-9">--}}
            {{--<select name='status' class="col-xs-10 col-sm-5" id="form-field-select-1">--}}
            {{--<option value="1">Active</option>--}}
            {{--<option value="0">Inactive</option>--}}
            {{--</select>--}}
            {{--</div>--}}
            {{--</div>--}}

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        $('#division_id').on('change', function () {
            var divisionID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('district.adminDistrictSelectAjaxList') }}";
            if (divisionID) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {divisionID: divisionID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="district_id"]').empty();
                        $('select[name="upazilla_id"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="district_id"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            } else {
                $('select[name="district_id"]').empty();
            }
        });

        $('#district_id').on('change', function () {
            var districtID = $(this).val();
            var token = $("input[name='_token']").val();
            var url = "{{ route('upazila.adminUpazilaSelectAjaxList') }}";
            if (districtID) {
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: {districtID: districtID, _token: token},
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('select[name="upazilla_id"]').empty();
                        $.each(data, function (key, value) {
                            $('select[name="upazilla_id"]').append('<option value="' + key + '">' + value + '</option>');
                        });
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("Status: " + textStatus);
                        alert("Error: " + errorThrown);
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                });
            } else {
                $('select[name="upazilla_id"]').empty();
            }
        });
    </script>
@stop