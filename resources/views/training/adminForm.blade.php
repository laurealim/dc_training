@extends('layouts.admin')

@section('title')
    Add Training
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('training.adminList') }}">Training List</a>
        </li>

        <li>
            <a href="{{ route('training.adminForm') }}">Training Add</a>
        </li>
    </ul>
@stop

@section('page_header')
    <h1>Training Form</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <h3 class="header smaller lighter blue">
            সাধারণ তথ্য
        </h3>

        <form class="form-horizontal" role="form" action="{{ route('training.adminStore') }}" method="POST"
              enctype="multipart/form-data">
            {{ csrf_field() }}

            <input type="hidden" name="trade_code" id="trade_code" value="<?php echo strtotime(now()); ?>">
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="training_type"> ট্রেনিং এর ধরণ *</label>

                <div class="col-sm-9">
                    <select id="training_type" placeholder="" name="training_type" class="col-xs-10 col-sm-5" required>
                        <option value="1">নতুন ট্রেনিং</option>
                        <option value="2">পূর্ববর্তী ট্রেনিং</option>
                    </select>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group trainingList">
                <label class="col-sm-3 control-label no-padding-right" for="trainingList"> প্রশিক্ষণের নাম *</label>

                <div class="col-sm-9">
                    <select id="trainingList" placeholder="" name="trainingList" class="col-xs-10 col-sm-5">
                        <?php foreach ($trainingList as $id => $name) {?>
                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                        <?php }?>
                    </select>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group text_name">
                <label class="col-sm-3 control-label no-padding-right" for="name"> প্রশিক্ষণের নাম *</label>

                <div class="col-sm-9">
                    <input type="text" id="name" placeholder="প্রশিক্ষণের নাম" name="name"
                           class="col-xs-10 col-sm-5" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="training_place"> প্রশিক্ষণের স্থান *</label>

                <div class="col-sm-9">
                    <input type="text" id="training_place" placeholder="প্রশিক্ষণের স্থান" name="training_place"
                           class="col-xs-10 col-sm-5" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('training_place'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('training_place') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="dept_id"> প্রতিষ্ঠানের নাম *</label>

                <div class="col-sm-9">
                    <select id="dept_id" placeholder="" name="dept_id" class="col-xs-10 col-sm-5" required>
                        <option value="">--- বাছাই করুন ---</option>
                        <?php foreach ($deptList as $id => $name) {?>
                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                        <?php }?>
                    </select>
                <!-- <input type="email" id="dept_id" placeholder="" name="dept_id"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('dept_id'))
                    <span class="help-block middle">
                        <strong>{{ $errors->first('dept_id') }}</strong>
                            </span>
                        @endif
                {{--<span class="middle">Inline help text</span>--}}
                        </span> -->
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="trade_name"> ট্রেডের নাম *</label>

                <div class="col-sm-9">
                    <select id="trade_name" placeholder="" name="trade_name" class="col-xs-10 col-sm-5" required>
                        <option value="">--- বাছাই করুন ---</option>
                        <?php foreach ($tradeList as $id => $name) {?>
                            <?php echo "<option value='{$id}'>{$name}</option>"; ?>
                        <?php }?>
                    </select>
{{--                    <input type="text" id="trade_name" placeholder="ট্রেডের নাম" name="trade_name" class="col-xs-10 col-sm-5"/>--}}
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('trade_name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('trade_name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="total_day"> প্রশিক্ষণের সময়কাল (দিন)
                    *</label>

                <div class="col-sm-9">
                    <input type="number" min="1" id="total_day" placeholder="প্রশিক্ষণের সময়কাল (দিন)" name="total_day" class="col-xs-10 col-sm-5" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('total_day'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('total_day') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="code">প্রতিষ্ঠানের কোড নং</label>

                <div class="col-sm-9">
                    <input type="text" id="code" placeholder="প্রতিষ্ঠানের কোড নং" name="code"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('code'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('code') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="proposal_org">প্রশিক্ষণ অনুমোদনকারী
                    সংস্থা</label>

                <div class="col-sm-9">
                    <input type="text" id="proposal_org" placeholder="প্রশিক্ষণ অনুমোদনকারী সংস্থা" name="proposal_org"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('proposal_org'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('proposal_org') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="financial_org">প্রশিক্ষণ অর্থায়নকারী                    সংস্থা</label>

                <div class="col-sm-9">
                    <input type="text" id="financial_org" placeholder="প্রশিক্ষণ অর্থায়নকারী সংস্থা"
                           name="financial_org"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('financial_org'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('financial_org') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="budget">অর্থায়ন মূল্য *</label>

                <div class="col-sm-9">
                    <input type="number" min="1" id="budget" placeholder="অর্থায়ন মূল্য" name="budget"
                           class="col-xs-10 col-sm-5" required/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('budget'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('budget') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="start_date">প্রশিক্ষণ শুরুর দিন *</label>

                <div class="col-sm-9">
                    <div class="input-group col-xs-10 col-sm-5 ">
                        <input class="date-picker form-control" id="start_date" type="text" name="start_date"
                               data-date-format="dd-mm-yyyy" required/>
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="end_date">প্রশিক্ষণ শেষের দিন *</label>

                <div class="col-sm-9">
                    <div class="input-group col-xs-10 col-sm-5 ">
                        <input class="date-picker form-control" id="end_date" type="text" name="end_date"
                               data-date-format="dd-mm-yyyy" required/>
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="exam_date">পরীক্ষার দিন </label>

                <div class="col-sm-9">
                    <div class="input-group col-xs-10 col-sm-5 ">
                        <input class="date-picker form-control" id="exam_date" type="text" name="exam_date"
                               data-date-format="dd-mm-yyyy" />
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> বর্তমান অবস্থা *</label>

                <div class="col-sm-9">
                    <select name='status' class="col-xs-10 col-sm-5" id="form-field-select-1" required>
                        <option value="">--- বাছাই করুন ---</option>
                        <option value="1">অনিষ্পাদিত</option>
                        <option value="2">সম্পন্ন</option>
                        <option value="0">বাতিল</option>
                    </select>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')
    <script type="text/javascript">
        $('.trainingList').hide();
        $('.trainingList').children().attr('disabled',true);
        $('#trainingList').removeAttr('required');

        /****************************/
        /****************************/
        /*  File Upload Section */
        /****************************/
        /****************************/

        $('#image').ace_file_input({
            style: 'well',
            btn_choose: 'Drop files here or click to choose',
            btn_change: null,
            no_icon: 'ace-icon fa fa-cloud-upload',
            droppable: true,
            thumbnail: 'small'//large | fit
            //,icon_remove:null//set null, to hide remove/reset button
            /**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
            /**,before_remove : function() {
						return true;
					}*/
            ,
            preview_error: function (filename, error_code) {
                //name of the file that failed
                //error_code values
                //1 = 'FILE_LOAD_FAILED',
                //2 = 'IMAGE_LOAD_FAILED',
                //3 = 'THUMBNAIL_FAILED'
                //alert(error_code);
            }

        }).on('change', function () {
            //console.log($(this).data('ace_input_files'));
            //console.log($(this).data('ace_input_method'));
        });

        var whitelist_ext, whitelist_mime;
        var btn_choose;
        var no_icon;

        var file_input = $('#image');

        file_input
            .ace_file_input('update_settings',
                {
                    'btn_choose': btn_choose,
                    'no_icon': no_icon,
                    'allowExt': whitelist_ext,
                    'allowMime': whitelist_mime
                });

        file_input.ace_file_input('reset_input');

        file_input
            .off('file.error.ace')
            .on('file.error.ace', function (e, info) {
                //console.log(info.file_count);//number of selected files
                //console.log(info.invalid_count);//number of invalid files
                //console.log(info.error_list);//a list of errors in the following format

                //info.error_count['ext']
                //info.error_count['mime']
                //info.error_count['size']

                //info.error_list['ext']  = [list of file names with invalid extension]
                //info.error_list['mime'] = [list of file names with invalid mimetype]
                //info.error_list['size'] = [list of file names with invalid size]


                /**
                 if( !info.dropped ) {
							//perhapse reset file field if files have been selected, and there are invalid files among them
							//when files are dropped, only valid files will be added to our file array
							e.preventDefault();//it will rest input
						}
                 */


                //if files have been selected (not dropped), you can choose to reset input
                //because browser keeps all selected files anyway and this cannot be changed
                //we can only reset file field to become empty again
                //on any case you still should check files with your server side script
                //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
            });


        /****************************/
        /****************************/
        /*  End */
        /****************************/
        /****************************/

        //datepicker plugin
        $('.date-picker').datepicker({
            autoclose: true,
            todayHighlight: true
        })

        /*      Training Type   */
        $('#training_type').on('change',function () {
            var training_type = $(this).val();
            if(training_type == 2){
                var trainign_name = '<?php echo $trainingList; ?>';
                console.log(trainign_name);
                $('.trainingList').show();
                $('.trainingList').children().attr('disabled',false);
                $('#trainingList').attr('required');

                // $('.text_name').hide();
                // $('.text_name').children().attr('disabled',true);

                previousDataSet($('#trainingList').val());
            }else{
                $('.trainingList').hide();
                $('.trainingList').children().attr('disabled',true);
                $('#trainingList').removeAttr('required');

                var t_cd = "<?php echo strtotime(now()); ?>";
                $('#trade_code').val(t_cd);

                // $('.text_name').show();
                // $('.text_name').children().attr('disabled',false);

                clearField();
            }
        });

        /*      Training Name   */
        $('#trainingList').on('change',function () {
            var training_trade_value = $(this).val();
            previousDataSet(training_trade_value)
        });

        function previousDataSet(training_trade_value){
            var training_trade = '<?php echo $trainingTradeList; ?>';
            $.each(JSON.parse(training_trade), function (index, values) {
                if(values.id == training_trade_value){
                    $('#training_place').val(values.training_place);
                    $('#trade_name').val(values.trade_name);
                    $('#code').val(values.code);
                    $('#name').val(values.name);
                    $('#trade_code').val(values.trade_code);
                    console.log(values.id);
                }
            })
        }

        function clearField() {
            $('#training_place').val("");
            $('#trade_name').val("");
            $('#code').val("");
        }
    </script>
@stop