<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="5%">
            <label class="pos-rel">
                {{--<input type="checkbox" class="ace"/>--}}
                {{--<span class="lbl"></span>--}}
            </label>
        </th>
        <th width="20%">Organization Name</th>
        <th width="15%">Organization Type</th>
        <th width="30%">Address</th>
        <th width="10%">Contact No.</th>
        <th width="5%">Created By</th>
        <th width="5%">Status</th>
        <th class="center" width="10%">Action</th>
    </tr>
    </thead>

    <tbody>

    <?php $pageNo = (($organizations->currentPage() - 1) * $organizations->perPage()) + 1 ?>
    @foreach ($organizations as $organization)
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $organization->name }}
            </td>
            <td>
                {{ $organization->type }}
            </td>
            <td>
                {{ $organization->address }}
            </td>
            <td>
                {{ $organization->phone }}
            </td>
            <td> {{ $organization->first_name}} </td>
            <td>
                <?php if($organization->status == 1){ ?>
                <span class='label label-success'>Active</span>
                <?php } else{ ?>
                <span class='label label-danger'>Inactive</span>
                <?php } ?>
            </td>
            <td class="center">
                <div class="hidden-sm hidden-xs action-buttons">
                    {{--<a class="blue" href="{{ route('') }}">--}}
                    {{--<i class="ace-icon fa fa-search-plus bigger-200"></i>--}}
                    {{--</a>--}}

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    <a class="green" href="{{ route('organization.adminEdit',$organization->id) }}">
                        <i class="ace-icon fa fa-pencil bigger-200"></i>
                    </a>

                    &nbsp;&nbsp;| &nbsp;&nbsp;

                    <a class="red deletebtn" href="{{ route('organization.adminDelete',$organization->id) }}" id={{ $organization->id }}>
                        <i class="ace-icon fa fa-trash-o bigger-200"></i>
                    </a>

                    {{--&nbsp;&nbsp;| &nbsp;&nbsp;--}}

                    {{--<a class="red deletebtn" href="{{ route('country.adminDelete',$country->id) }}" id={{ $country->id }}>--}}
                    {{--<i class="ace-icon fa fa-trash-o bigger-200"></i>--}}
                    {{--</a>--}}
                </div>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="row">
    <div class="col-xs-6">
        <div class="dataTables_info" id="dynamic-table_info" role="status" aria-live="polite">
            Showing {{ (($organizations->currentPage() - 1) * $organizations->perPage()) + 1 }}
            to {{ (($organizations->currentPage() - 1) * $organizations->perPage()) + $organizations->perPage() }} of
            {{ $organizations->total() }} entries
        </div>
    </div>
    <div class="col-xs-6">
        <div class="dataTables_paginate paging_simple_numbers" id="dynamic-table_paginate">
            {{ $organizations->links("vendor.pagination.custom") }}
        </div>
    </div>
</div>
