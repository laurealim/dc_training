@extends('layouts.admin')

@section('title')
    Add Organization
@stop

@section('breadcrumb')
    <ul class="breadcrumb">
        <li>
            <i class="ace-icon fa fa-bars home-icon"></i>
            <a href="{{ route('organization.adminList') }}">Organization List</a>
        </li>

        <li>
            <a href="{{ route('organization.adminForm') }}">Organization Add</a>
        </li>
        {{--<li class="active">User Profile</li>--}}
    </ul>
@stop

@section('page_header')
    <h1>Add Organization</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <form class="form-horizontal" role="form" action="{{ route('organization.adminStore') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Organization Name *</label>

                <div class="col-sm-9">
                    <input type="text" id="form-field-1" placeholder="Organization Name" name="name"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('name'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Organization Type </label>

                <div class="col-sm-9">
                    <select name='type' class="col-xs-10 col-sm-5" id="form-field-select-1">
                        <option value="">--- Please Select One ---</option>
                        @foreach(\App\Model\Organization::$organizationType as $key =>$type)
                            <option value="{{ $key }}">{{ $type }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="space-4"></div>
            <div class="form-group">

                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Organization Address
                    *</label>

                <div class="col-sm-9">
                    <textarea id="form-field-1" placeholder="Organization Address" name="address"
                              class="col-xs-10 col-sm-5"> </textarea>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('address'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Organization Phone *</label>

                <div class="col-sm-9">
                    <input type="text" id="form-field-1" placeholder="Organization Phone" name="phone"
                           class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
                        @if ($errors->has('phone'))
                            <span class="help-block middle">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                        {{--<span class="middle">Inline help text</span>--}}
                    </span>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" for="form-field-1-1"> Status </label>

                <div class="col-sm-9">
                    <select name='status' class="col-xs-10 col-sm-5" id="form-field-select-1">
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </div>
            </div>

            <div class="space-4"></div>

            <div class="clearfix form-actions">
                <div class="col-md-offset-3 col-md-9">
                    <button class="btn btn-info" type="submit">
                        <i class="ace-icon fa fa-check bigger-110"></i>
                        Submit
                    </button>

                    &nbsp; &nbsp; &nbsp;
                    <button class="btn" type="reset">
                        <i class="ace-icon fa fa-undo bigger-110"></i>
                        Reset
                    </button>
                </div>
            </div>
        </form>
    </div>
@stop


@section('custom_style')

@stop

@section('custom_script')

@stop