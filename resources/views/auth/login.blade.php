@extends('layouts.login')

@section('title')
    User Login
@stop
@section('content')
    <div class="space-12"></div>
    <div class="space-12"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 content_box">
                <div class="center col-md-12 col-sm-12">
                    <h1>
                        <span class="red">Login Portal</span>
                    </h1>
                    <h4 class="blue" id="id-company-text">Please Login with correct portal</h4>
                </div>
                <div class="space-12"></div>

                {{--<div class="panel panel-default center" style="">--}}
                <div class="center col-md-12 col-sm-12">
                    <div class="col-md-3 col-sm-8 col-md-offset-1 col-sm-offset-2">
                        <figure>
                            <a href="{{ route('admin.login') }}" class='box photo center panel-body'>

                                <img src="{{ asset('images/users/admin.png') }}"
                                     class="img_photo" alt="Alex's Avatar"/>

                                <p style="font-size:20px;"> Admin </p>

                            </a>
                        </figure>
                    </div>
                    <div class="col-md-3 col-sm-8 col-md-offset-1 col-sm-offset-2">
                        <figure>
                            <a href="{{ route('client.login') }}" class='box photo center panel-body'>
                                <img src="{{ asset('images/users/owner.png') }}"
                                     class="img_photo" alt="Alex's Avatar"/>

                                <p style="font-size:20px;"> Office Head </p>
                            </a>
                        </figure>
                    </div>
                    <div class="col-md-3 col-sm-8 col-md-offset-1 col-sm-offset-2">
                        <figure>
                            <a href="{{ route('user.login') }}" class='box photo center panel-body'>
                                <img src="{{ asset('images/users/users.png') }}"
                                     class="img_photo" alt="Alex's Avatar"/>

                                <p style="font-size:20px;"> User </p>
                            </a>
                        </figure>
                    </div>
                </div>

                {{--</div>--}}
            </div>
        </div>
    </div>
@stop

@section('custom_style')
    <style type="text/css">
        .container {
            display: flex;
            justify-content: center;
        }

        .panel-body {
            /*border-radius: 200px;*/
            /*border: 1px solid silver;*/
            /*flex-basis: 110px;*/
            /*background-color: #B7D3E5;*/
            /*border: 1px solid silver;*/
            /*margin: 5px;*/
            /*padding: 0;*/
            /*margin-top: 25%;*/
        }

        .panel-body .photo .img_photo {
            /*border-radius: 200px;*/
            /*border: 1px solid silver;*/
            /*display: block;*/
            /*margin-left: auto;*/
            /*margin-right: auto;*/
            /*width: 90%;*/
            /*height: 90%;*/
            /*padding: 5px;*/
        }

        figure {
            position: relative;
            overflow: hidden;
            margin: 0;
            height: 100%;
            width: 100%;
            filter: grayscale(.9);
            border-radius: 100px;
            border: 1px solid silver;
        }

        figure img {
            padding: 10px;
            width: 100%;
            height: 100%;
        }

        figure::before {
            position: absolute;
            top: 50%;
            left: 50%;
            z-index: 2;
            display: block;
            content: '';
            width: 0;
            height: 0;
            background: rgba(0, 0, 0, .2);
            border-radius: 100%;
            transform: translate(-50%, -50%);
            opacity: 0;
        }

        figure:hover {
            animation: bloom ease-in-out .40s forwards;
        }

        figure:hover::before {
            animation: circle .40s;
        }

        @keyframes bloom {
            0% {
                filter: grayscale(.8);
            }
            40% {
                filter: grayscale(.5);
            }
            100% {
                filter: grayscale(0);
            }
        }

        @keyframes circle {
            0% {
                opacity: .5;
                background: rgba(213, 156, 34, .2);

            }
            40% {
                opacity: 1;
                background: rgba(213, 34, 160, .2);
            }
            100% {
                width: 200%;
                height: 200%;
                opacity: 0;
            }
        }

    </style>
@stop

@section('custom_script')
    <script type="text/javascript">
        var hasEmail = '<?php echo $errors->has('email') ?>';
        var loginError = '<?php echo $errors->has('login_error') ?>';
        {{--var hasEmail = '<? echo $errors->has('email') ?>';--}}
        jQuery(function ($) {

            $(document).on('click', '.toolbar a[data-target]', function (e) {
                e.preventDefault();
                var target = $(this).data('target');
                $('.widget-box.visible').removeClass('visible');//hide others
                $(target).addClass('visible');//show target
            });
        });


        //you don't need this, just used for changing background
        jQuery(function ($) {
            $('#btn-login-dark').on('click', function (e) {
                $('body').attr('class', 'login-layout');
                $('#id-text2').attr('class', 'white');
                $('#id-company-text').attr('class', 'blue');

                e.preventDefault();
            });
            $('#btn-login-light').on('click', function (e) {
                $('body').attr('class', 'login-layout light-login');
                $('#id-text2').attr('class', 'grey');
                $('#id-company-text').attr('class', 'blue');

                e.preventDefault();
            });
            $('#btn-login-blur').on('click', function (e) {
                $('body').attr('class', 'login-layout blur-login');
                $('#id-text2').attr('class', 'white');
                $('#id-company-text').attr('class', 'light-blue');

                e.preventDefault();
            });

        });

        //        $('#login_button_id').on('click', function(){
        //            alert("click me");
        //            $('#loginForm').submit();
        //        });

        if (loginError) {
//            var target = $('#forgot-box').data('target');
            $('.widget-box.visible').removeClass('visible');//hide others
            $('#forgot-box').find('.help-block').remove();
            $('#login-box').addClass('visible');//show target
        }
        else if (hasEmail) {
//            var target = $('#forgot-box').data('target');
            $('.widget-box.visible').removeClass('visible');//hide others
            $('#login-box').find('.help-block').remove();
            $('#forgot-box').addClass('visible');//show target
        }


    </script>
@stop