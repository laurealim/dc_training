<table id="dynamic-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 0px;">
    <thead>
    <tr>
        <th class="center" width="3%">
            Sr. No.
        </th>
        <th width="8%">প্রশিক্ষণার্থীর নাম</th>
        <th width="8%">প্রশিক্ষণের নাম</th>
        <th width="8%">ট্রেডের নাম</th>
        <th width="8%">মোবাইল নাম্বার</th>
        <th width="5%">ই-মেইল</th>
        <th width="8%">জন্ম নিবন্ধন / ভোটার আইডি নাম্বার</th>
        <th width="5%">বয়স</th>
        <th width="8%">বর্তমান ঠিকানা</th>
        <th width="5%">শিক্ষাগত যোগ্যতা</th>
        <th width="5%">বর্তমান পেশা</th>
        <th width="4%">Created By</th>
        <th width="5%">Status</th>
    </tr>
    </thead>

    <tbody>

    <?php //pr($registrationList); ?>
    <input type="hidden" id="training_id" value="<?php echo $training_id; ?>"/>
    <input type="hidden" id="trade_name" value="<?php echo $trade_name; ?>"/>
    <input type="hidden" id="reg_status" value="<?php echo $reg_status; ?>"/>
    <?php $pageNo = 1;?>
<!--    --><?php //$pageNo = (($registrationList->currentPage() - 1) * $registrationList->perPage()) + 1;?>
    @foreach ($registrationList as $registration)
<?php //pr($registrationList);?>
        <tr>
            <td class="center">
                <label class="pos-rel">
                    {{--<input type="checkbox" class="ace"/>--}}
                    <span class="lbl">{{ $pageNo++ }}</span>
                </label>
            </td>
            <td>
                {{ $registration->name_bn }}
            </td>
            <td>
                {{--                {{ $trainingList[$registration->training_id] }}--}}
                {{ $trainingList[$registration->training_id] }}
            </td>
            <td>
                {{ $tradeList[$registration->trade_name] }}
            </td>
            <td>
                {{ $registration->phone }}
            </td>
            <td>
                {{ $registration->email }}
                {{--                {{ $departmentList[$registration->dept_id] webtradeList}}--}}
            </td>
            <td>
                {{ $registration->nid }}
            </td>
            <td class="center">
                {{ $registration->age }}
            </td>
            <td>
                {{ $registration->vill_pre }}, {{ $registration->post_pre }}, {{ $registration->up_pre }}
                , {{ $registration->dist_pre }}
            </td>
            <td>
                {{ config('constants.edu_level.arr.'.$registration->edu_lvl) }}
            </td>
            <td>
                {{ $registration->cur_job  }}
            </td>
            <td class="center">
                {{ $registration->user->first_name}}
            </td>
            <td class="center">
                <?php if ($registration->status == config('constants.registration_status.Waiting')) {?>
                <span class='label label-warning'><?php echo config('constants.registration_status.1'); ?></span>
                <?php } else if ($registration->status == config('constants.registration_status.Approved')) {?>
                <span class='label label-info'><?php echo config('constants.registration_status.2'); ?></span>
                <?php } else if ($registration->status == config('constants.registration_status.Confirmed')) {?>
                <span class='label label-success'><?php echo config('constants.registration_status.3'); ?></span>
                <?php } else {?>
                <span class='label label-danger'><?php echo config('constants.registration_status.0'); ?></span>
                <?php }?>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>


<style type="text/css">
    /*.table tbody > tr > td {*/
    /*text-align: center;*/
    /*}*/
    .facility {
        font-weight: bold;
    }
</style>