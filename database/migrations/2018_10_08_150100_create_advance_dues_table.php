<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvanceDuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advance_dues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('apt_id');
            $table->integer('client_id');
            $table->integer('pending_ledger_id');
            $table->integer('total_amount');
            $table->integer('previous_amount')->default(0)->comment("0 will be only when parent bill bill_type will be initial(1)");
            $table->integer('current_amount')->comment("+(ve) value define Extra, -(ve) value define Due ");
            $table->tinyInteger('amount_for')->comment('1 = Billing, 2 = Payment');
            $table->tinyInteger('type')->comment('1 = Initial, 2 = Regular, 3 = Adjustment, 4 = Advance');
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->integer('approved_by')->default(0);
            $table->tinyInteger('status')->comment('1 = Pending, 2 = Approved, 3 = Rejected');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advance_dues');
    }
}
