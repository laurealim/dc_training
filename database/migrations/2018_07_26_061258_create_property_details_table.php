<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');
            $table->string('builders');
            $table->integer('total_unit');
            $table->integer('total_floor');
            $table->integer('unit_floor');
            $table->tinyInteger('has_gas')->default(1)->comment('1 = Yes, 0 = No');
            $table->tinyInteger('has_lift')->default(1)->comment('1 = Yes, 0 = No');
            $table->tinyInteger('has_generator')->default(1)->comment('1 = Yes, 0 = No');
            $table->text('p_description')->nullable();
            $table->text('p_images')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_details');
    }
}
