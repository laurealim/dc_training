<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentUpdateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent_update_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('apt_rent_id');
            $table->string('rent_amount');
            $table->date('applied_from');
            $table->date('applied_to')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1 = Running, 0 = Expired');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rent_update_histories');
    }
}
