<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('training_place');
            $table->integer('total_day');
            $table->integer('dept_id');
            $table->string('trade_name')->nullable();
            $table->integer('trade_code');
            $table->string('code')->nullable();
            $table->string('proposal_org')->nullable();
            $table->string('financial_org')->nullable();
            $table->double('budget',8,2)->nullable();
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->dateTime('exam_date')->nullable();
            $table->integer('created_by')->default(0);
            $table->integer('is_deleted')->default(0);
            $table->tinyInteger('status')->default(1)->comment('1 = Pending, 2 = Complete, 0 = Postponed');
            $table->timestamps();
            $table->collation = 'utf8_general_ci';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainings');
    }
}
