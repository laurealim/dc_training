<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendingLedgersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_ledgers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('apt_id');
            $table->integer('client_id');
            $table->integer('amount')->comment("if comes form the bill, value will be -(ve). Otherwise +(ve).");
            $table->integer('secure_amount')->default(0)->comment("if it is initial and has the security amount.");
            $table->integer('previous_amount')->default(0)->comment("0 will be only when parent bill_type will be initial(1).");
            $table->integer('current_amount')->comment("+(ve) value define Extra, -(ve) value define Due.");
            $table->tinyInteger('ledger_for')->comment('1 = Billing, 2 = Payment');
            $table->tinyInteger('ledger_type')->comment('1 = Initial, 2 = Regular, 3 = Adjustment, 4 = Advance');
            $table->integer('created_by')->default(0);
            $table->integer('updated_by')->default(0);
            $table->integer('approved_by')->default(0);
            $table->tinyInteger('status')->default(1)->comment('1 = Pending, 2 = Approved, 3 = Rejected');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pending_ledgers');
    }
}
