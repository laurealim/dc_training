<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id');
            $table->string('apt_number');
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->integer('owner');
            $table->integer('apt_size');
            $table->integer('apt_level');
            $table->integer('bed_room');
            $table->integer('toilet');
            $table->integer('veranda');
            $table->tinyInteger('intercom')->default(1)->comment('1 = Available, 2 = Not Available, 0 = Deleted');
            $table->tinyInteger('geyser')->default(1)->comment('1 = Available, 2 = Not Available, 0 = Deleted');
            $table->tinyInteger('car_parking')->default(1)->comment('1 = Available, 2 = Not Available, 0 = Deleted');
            $table->text('apt_images')->nullable();
            $table->text('apt_desc')->nullable();
            $table->tinyInteger('status')->default(1)->comment('1 = Active, 2 = Inactive, 0 = Deleted');
            $table->tinyInteger('is_assigned')->default(0)->comment('0 = Unassigned, 1 = Assigned, 2 = Confirmed');
            $table->integer('client_id')->default(0)->comment('0 = None');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartments');
    }
}
