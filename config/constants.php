<?php
return [
    'status' => [
        0 => 'Deleted',
        1 => 'Active',
        2 => 'Inactive',
        'Deleted' => 0,
        'Active' => 1,
        'Inactive' => 2,
    ],

    'userType' => [
        1 => 'Super User',
        2 => 'Owner',
        3 => 'Tenant',
        'SuperUser' => 1,
        'Owner' => 2,
        'Tenant' => 3,
    ],

    'ledgerFor' => [
        1 => 'Billing',
        2 => 'Payment',
        'Billing' => 1,
        'Payment' => 2,
    ],

    'ledgerType' => [
        1 => 'Initial',
        2 => 'Regular',
        3 => 'Adjustment',
        4 => 'Advance',
        'Initial' => 1,
        'Regular' => 2,
        'Adjustment' => 3,
        'Advance' => 4,
    ],

    'state' => [
        1 => 'Pending',
        2 => 'Approved',
        3 => 'Rejected',
        'Pending' => 1,
        'Approved' => 2,
        'Rejected' => 3,
    ],

    'is_assigned' => [
        0 => 'Unassigned',
        1 => 'Assigned',
        2 => 'Confirmed',
        'Unassigned' => 0,
        'Assigned' => 1,
        'Confirmed' => 2,
    ],

    'training_status' => [
        1 => 'অপেক্ষমাণ',
        2 => 'সম্পন্ন',
        0 => 'বাতিল',
        'Pending' => 1,
        'Complete' => 2,
        'Postponed' => 0,
    ],

    'is_deleted' => [
        1 => 'Deleted',
        0 => 'Active',
        'Deleted' => 1,
        'Active' => 0,
    ],

    'edu_level' => [
        'arr' => [
            1 => 'পি,এস,সি',
            2 => 'জে,এস,সি',
            3 => 'এস,এস,সি / সমমানের',
            4 => 'এইচ,এস,সি / সমমানের',
            5 => 'ব্যাচেলর / সমমানের',
            6 => 'মাস্টার্স / সমমানের',
            7 => 'ডক্টরেট',
            8 => 'অন্যান্য'
        ],
        'PSC' => 1,
        'JSC' => 2,
        'SSC' => 3,
        'HSC' => 4,
        'Diploma' => 5,
        'Bsc' => 6,
        'Msc' => 7,
        'Fajil' => 8,
        'Kamil' => 9,
        'Others' => 10,
    ],

    'edu_board' => [
        'arr' => [
            1 => 'ঢাকা',
            2 => 'রাজশাহী',
            3 => 'কুমিল্লা',
            4 => 'যশোর',
            5 => 'চিটাগাং',
            6 => 'বরিশাল',
            7 => 'সিলেট',
            8 => 'দিলাজপুর',
            9 => 'মাদ্রাসা',
        ],
        'Dhaka' => 1,
        'Rajshahi' => 2,
        'Comilla' => 3,
        'Jessore' => 4,
        'Chittagong' => 5,
        'Barisal' => 6,
        'Sylhet' => 7,
        'Dinajpur' => 8,
        'Madrasah' => 9,
    ],

    'religious' => [
        'arr' => [
            1 => 'ইসলাম',
            2 => 'হিন্দু',
            3 => 'খ্রিস্টান',
            4 => 'বৌদ্ধ',
            5 => 'অন্যান্ন',
        ],
        'Islam' => 1,
        'Hindu' => 2,
        'Christian' => 3,
        'Buddhist' => 4,
        'Others' => 5,
    ],

    'gender' => [
        'arr' => [
            1 => 'পুরুষ',
            2 => 'মহিলা',
            3 => 'অন্যান্ন',
        ],
        'Male' => 1,
        'Female' => 2,
        'Others' => 3,
    ],

    'marital_status' => [
        'arr' => [
            1 => 'অবিবাহিত',
            2 => 'বিবাহিত',
            3 => 'ডিভোর্স',
            4 => 'বিধবা/বিপত্নীক',
        ],
        'Single' => 1,
        'Married' => 2,
        'Divorced' => 3,
        'Widowed' => 4,
    ],

    'registration_status' => [
        1 => 'Waiting',
        2 => 'Approved',
        3 => 'Confirmed',
        'Waiting' => 1,
        'Approved' => 2,
        'Confirmed' => 3,
    ],


];
?>